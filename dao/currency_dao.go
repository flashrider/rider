package dao

import (
	"github.com/tarantool/go-tarantool"
	"math"
)

type CurrencyType uint8

const (
	CURRENCY_TYPE_FIAT       CurrencyType = 1
	CURRENCY_TYPE_CRYPTO     CurrencyType = 2
	CURRENCY_TYPE_TOKEN      CurrencyType = 3
	CURRENCY_TYPE_ELECTRONIC CurrencyType = 4
	CURRENCY_TYPE_CFD        CurrencyType = 5 //oil/corn...
)

//ownerIndex
type CurrencyDao struct {
}

func NewCurrencyDao() *CurrencyDao {
	return &CurrencyDao{}
}

//insert
func (self *CurrencyDao) Insert(currency *CurrencyEntity) (uint8, error) {
	resp, err := client.Insert(SPACE_NAME_CURRENCY, []interface{}{nil, currency.Code, currency.Type,
		currency.Delisted, currency.UpdateTime, currency.IssuerCurrId})
	if err != nil {
		return 0, err
	}

	return ToUint8(resp), nil
}

//get by id
func (self *CurrencyDao) GetById(id uint8) (*CurrencyEntity, error) {
	resp, err := client.Select(SPACE_NAME_CURRENCY, PRIMARY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{id})
	if err != nil {
		return nil, err
	}
	return self.unpack(resp.Tuples())
}

//get all
func (self *CurrencyDao) GetAll() ([]*CurrencyEntity, error) {
	resp, err := client.Select(SPACE_NAME_CURRENCY, PRIMARY_INDEX, 0, math.MaxUint8,
		tarantool.IterAll, []interface{}{})
	if err != nil {
		return nil, err
	}
	return self.unpackList(resp.Tuples())
}

//single object
func (self *CurrencyDao) unpack(respData [][]interface{}) (*CurrencyEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

//list
func (self *CurrencyDao) unpackList(respData [][]interface{}) ([]*CurrencyEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *CurrencyEntity = nil
	respArr := make([]*CurrencyEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity
		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}

func (dao *CurrencyDao) MapFromInterface(data []interface{}) *CurrencyEntity {
	entity := &CurrencyEntity{}
	entity.Id = uint16(data[0].(uint64))
	entity.Code = data[1].(string)
	entity.Type = CurrencyType(data[2].(uint64))
	entity.Delisted = data[3].(bool)
	entity.UpdateTime = data[4].(uint64)
	entity.IssuerCurrId = uint16(data[5].(uint64))
	return entity
}
