package dao

import (
	"fmt"
	"gitlab.com/flashrider/rider/util"
	"testing"
)

func TestInsertCurrency(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	curDao := NewCurrencyDao()
	currency := &CurrencyEntity{}
	currency.Code = "EUR"
	currency.Type = CURRENCY_TYPE_FIAT

	id, err := curDao.Insert(currency)
	if err != nil {
		panic(err)
	}

	fmt.Println("id:", id)
	//assert.Equal(t, uint64(0), 1)
}

func TestInsertCurrencyList(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	curDao := NewCurrencyDao()

	fiatArr := []string{"EUR", "USD", "GBP", "CNY", "RUB"}
	cryptoArr := []string{"BTC", "LTC", "DASH", "BCH", "XLM", "XRP", "ETH", "EOS", "BSV", "MIOTA", "XMR", "ZEC"}
	now := util.NowMk()
	for _, fiat := range fiatArr {
		id, err := curDao.Insert(&CurrencyEntity{0, fiat, CURRENCY_TYPE_FIAT, false, now, 0})
		if err != nil {
			panic(err)
		}
		fmt.Println(fiat, " id:", id)
	}

	for _, fiat := range cryptoArr {
		id, err := curDao.Insert(&CurrencyEntity{0, fiat, CURRENCY_TYPE_CRYPTO, false, now, 0})
		if err != nil {
			panic(err)
		}
		fmt.Println(fiat, " id:", id)
	}
}

func TestGetCurrency(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	curDao := NewCurrencyDao()
	currEntity, err := curDao.GetById(1)
	if err != nil {
		panic(err)
	}
	fmt.Println(currEntity)
}

func TestGetAll(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	curDao := NewCurrencyDao()
	entityList, err := curDao.GetAll()
	if err != nil {
		panic(err)
	}
	fmt.Println(entityList[0])
}
