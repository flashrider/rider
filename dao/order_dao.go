package dao

import (
	"github.com/tarantool/go-tarantool"
	"math"
)

type OrderKind uint8
type OrderType uint8
type OrderDirection uint8
type OrderPriceTrigger uint8
type OrderState uint8
type OrderDuration uint8

const (
	ORDER_STATE_INDEX                = "STATE_INDEX"
	ORDER_ACCOUNT_ID_AND_STATE_INDEX = "ACCOUNT_ID_AND_STATE"

	//Kind
	ORDER_KIND_ENTRY OrderKind = 1
	ORDER_KIND_TP    OrderKind = 2 //executed as limit ask order
	ORDER_KIND_SL    OrderKind = 3

	//Type
	ORDER_TYPE_MKT   OrderType = 1
	ORDER_TYPE_LIMIT OrderType = 2
	ORDER_TYPE_STOP  OrderType = 3 //execute as market
	ORDER_TYPE_MIT   OrderType = 4 //execute as market + mandatory max splipp.

	//Direction
	ORDER_DIRECTION_SELL OrderDirection = 1
	ORDER_DIRECTION_BUY  OrderDirection = 0

	//Trigger
	ORDER_PRICE_TRIGGER_ASK_L OrderPriceTrigger = 1 // <= ask
	ORDER_PRICE_TRIGGER_ASK_G OrderPriceTrigger = 2 // >= ask
	ORDER_PRICE_TRIGGER_BID_L OrderPriceTrigger = 3 // <= bid
	ORDER_PRICE_TRIGGER_BID_G OrderPriceTrigger = 4 // >= bid

	//State
	ORDER_STATE_IFD                 OrderState = 1 //IFD state for TP and SL
	ORDER_STATE_PLACED              OrderState = 2 //at order book
	ORDER_STATE_PARTIALLY_FILLED    OrderState = 3 //opened (current price touched)
	ORDER_STATE_FILLED              OrderState = 4 //filled (closed)
	ORDER_STATE_CANCELED            OrderState = 5 //canceled by user
	ORDER_STATE_EXPIRED             OrderState = 6 //expired by time
	ORDER_STATE_CANCELLED_BY_ADMIN  OrderState = 7 //canceled by admin
	ORDER_STATE_CANCELLED_IFD_GROUP OrderState = 8 //if TP order executed SL should be canceled and reverse.

	//Duration
	ORDER_DURATION_DAY    OrderDuration = 1 // 24h
	ORDER_DURATION_GTC    OrderDuration = 2 // Good-Till-Canceled
	ORDER_DURATION_GTD    OrderDuration = 3 // Good-Till-Date See EXP_TIME field
	ORDER_DURATION_IOC    OrderDuration = 4 // Immediate or Cancel Order
	ORDER_DURATION_FOK    OrderDuration = 5 // Fill-Or-Kill
	ORDER_DURATION_AON    OrderDuration = 6 // All-or-None, till end of session
	ORDER_DURATION_MINUTE OrderDuration = 7 // order alive X minutes
)

//ownerIndex
type OrderDao struct {
}

func NewOpenOrderDao() *OrderDao {
	return &OrderDao{}
}

func (self *OrderDao) getTblName(instrName string) string {
	return instrName + "_" + SPACE_NAME_ORDER
}

//get by id
func (dao *OrderDao) GetById(instrumentName string, id uint64) (*OrderEntity, error) {
	resp, err := client.Select(dao.getTblName(instrumentName), PRIMARY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{id})
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

//insert order
func (dao *OrderDao) Insert(instrumentName string, order *OrderEntity) (uint64, error) {
	resp, err := client.Insert(
		dao.getTblName(instrumentName),
		[]interface{}{
			order.Id,
			order.PositionId,
			order.AccountId,
			order.Kind,
			order.Type,
			order.Direction,
			order.AmountRest,
			order.Price,
			order.PriceTrigger,
			order.Amount,
			order.State,
			order.EntryOrderId,
			order.ExpTime,
			order.CreatedTime,
			order.UpdateTime,
			order.Duration,
			order.CorrelationId,
		},
	)
	if err != nil {
		return 0, err
	}

	return ToUint64(resp), nil
}

func (dao *OrderDao) GetOrders(instrumentName string, orderState OrderState) ([]*OrderEntity, error) {
	resp, err := client.Select(dao.getTblName(instrumentName), ORDER_STATE_INDEX, 0, math.MaxUint16, tarantool.IterEq,
		[]interface{}{orderState})
	if err != nil {
		return nil, err
	}
	return dao.unpackList(resp.Tuples())
}

//for example orderState ORDER_STATE_FILLED, return ORDER_STATE_IFD, ORDER_STATE_PLACED, ORDER_STATE_PARTIALLY_FILLED
func (dao *OrderDao) GetOrdersLessState(instrumentName string, orderState OrderState) ([]*OrderEntity, error) {
	resp, err := client.Select(dao.getTblName(instrumentName), ORDER_STATE_INDEX, 0, math.MaxUint16, tarantool.IterLe,
		[]interface{}{orderState})
	if err != nil {
		return nil, err
	}
	return dao.unpackList(resp.Tuples())
}

//return open and pending orders from all instruments or nil
func (dao *OrderDao) GetAccountOpenOrder(accountId uint32) ([][]interface{}, error) {
	resp, err := client.Call("orderDAO.getPendingOpenOrderByAccount", []interface{}{accountId})

	if err != nil {
		return nil, err
	}

	return NilIfEmpty(resp.Tuples()), nil
}

func (dao *OrderDao) GetCount(instrumentName string) (uint64, error) {
	return TupleCount(dao.getTblName(instrumentName), PRIMARY_INDEX, []interface{}{})
}

//single object
func (self *OrderDao) unpack(respData [][]interface{}) (*OrderEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

//list
func (self *OrderDao) unpackList(respData [][]interface{}) ([]*OrderEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *OrderEntity = nil
	respArr := make([]*OrderEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}

func (dao *OrderDao) MapFromInterface(data []interface{}) *OrderEntity {
	entity := &OrderEntity{}
	entity.Id = uint64(data[0].(uint64))
	entity.PositionId = data[1].(uint64)
	entity.AccountId = uint32(data[2].(uint64))
	entity.Kind = OrderKind(data[3].(uint64))
	entity.Type = OrderType(data[4].(uint64))
	entity.Direction = OrderDirection(data[5].(uint64))
	entity.AmountRest = data[6].(uint64)
	entity.Price = data[7].(uint64)
	entity.PriceTrigger = OrderPriceTrigger(data[8].(uint64))
	entity.Amount = data[9].(uint64)
	entity.State = OrderState(data[10].(uint64))
	entity.EntryOrderId = data[11].(uint64)
	entity.ExpTime = data[12].(uint64)
	entity.CreatedTime = data[13].(uint64)
	entity.UpdateTime = data[14].(uint64)
	entity.Duration = OrderDuration(data[15].(uint64))
	entity.CorrelationId = data[16].(uint64)
	return entity
}
