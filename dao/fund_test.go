package dao

import (
	"fmt"
	"testing"
)

func TestInsertFund(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	fundDao := NewFundDao()
	fund := &FundEntity{}
	fund.AccountId = 1
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 1
	fund.Balance = 50000000000
	id, err := fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 1
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 2
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 1
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 6
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	//account 2
	fund = &FundEntity{}
	fund.AccountId = 2
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 1
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 2
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 2
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 2
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 6
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	//account 2
	fund = &FundEntity{}
	fund.AccountId = 3
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 1
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 3
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 2
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)

	fund = &FundEntity{}
	fund.AccountId = 3
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = 6
	fund.Balance = 50000000000
	id, err = fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("id:", id)
}

func TestInsertFundBulk(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	fundDao := NewFundDao()
	currrencyDao := NewCurrencyDao()
	list, _ := currrencyDao.GetAll()
	for _, curr := range list {
		createFundTest(fundDao, 1, curr.Id)
		createFundTest(fundDao, 2, curr.Id)
	}
}

func createFundTest(fundDao *FundDao, accountId uint32, currencyId uint16) {
	fund := &FundEntity{}
	fund.AccountId = accountId
	fund.State = FUNDS_STATE_ACTIVE
	fund.CurrId = currencyId
	fund.Balance = 50000000000
	id, err := fundDao.Insert(fund)
	if err != nil {
		panic(err)
	}
	fmt.Println("accId: ", accountId, "fundId: ", id)

}

func TestGetCompactFund(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	fundDao := NewFundDao()
	accArrr, fundArr, currArr, balanceArr, inOpenArrArr, stateArr, err := fundDao.GetAllCompact()
	if err != nil {
		panic(err)
	}
	fmt.Println(accArrr)
	fmt.Println(fundArr)
	fmt.Println(currArr)
	fmt.Println(balanceArr)
	fmt.Println(inOpenArrArr)
	fmt.Println(stateArr)
}

func TestGetByAccountId(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	fundDao := NewFundDao()
	fundArr, err := fundDao.GetByAccountId(uint32(1))
	if err != nil {
		panic(err)
	}
	fmt.Println(fundArr)
}
