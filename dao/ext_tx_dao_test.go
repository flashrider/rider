package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExtTradeCRD(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewExtTxDao()
	newExtTx := &ExtTxEntity{}
	newExtTx.AccountId = uint32(42)
	newExtTx.CurrencyId = uint16(1)
	newExtTx.Type = TX_TYPE_CASHIN
	newExtTx.Amount = uint64(3007)
	newExtTx.Timestamp = uint64(1234567890)

	id, err := dao.Insert(newExtTx)
	if err != nil {
		panic(err)
	}

	extTx, err := dao.GetById(id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, uint32(42), extTx.AccountId, "AccountId")
	assert.Equal(t, uint16(1), extTx.CurrencyId, "CurrencyId")
	assert.Equal(t, TX_TYPE_CASHIN, extTx.Type, "Type")
	assert.Equal(t, uint64(3007), extTx.Amount, "Amount")
	assert.Equal(t, uint64(1234567890), extTx.Timestamp, "Timestamp")

	res, err := dao.Delete(id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	extTx, err = dao.GetById(id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, extTx, "Deleted ExtTx should not exist")
}
