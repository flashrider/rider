package dao

import (
	"github.com/tarantool/go-tarantool"
	"math"
)

type FundState uint8

const (
	FUNDS_STATE_ACTIVE   FundState = 1
	FUNDS_STATE_DISABLED FundState = 2
)

//ownerIndex
type FundDao struct {
}

func NewFundDao() *FundDao {
	return &FundDao{}
}

//insert
func (self *FundDao) Insert(fund *FundEntity) (uint8, error) {
	resp, err := client.Insert(SPACE_NAME_FUND, []interface{}{nil, fund.AccountId, fund.CurrId, fund.Balance,
		fund.Reserved, fund.TotalVolumeAmount, fund.State, fund.TotalCommission,
		fund.TotalCashIn, fund.TotalCashOut, fund.ResetStatTime})
	if err != nil {
		return 0, err
	}

	return ToUint8(resp), nil
}

//get by id
func (self *FundDao) GetById(id uint8) (*FundEntity, error) {
	resp, err := client.Select(SPACE_NAME_FUND, PRIMARY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{id})
	if err != nil {
		return nil, err
	}
	return self.unpack(resp.Tuples())
}

func (dao *FundDao) GetByAccountId(accountId uint32) ([][]interface{}, error) {
	resp, err := client.Select(
		SPACE_NAME_FUND, ACCOUNT_ID_INDEX, 0, math.MaxUint16, tarantool.IterEq,
		[]interface{}{accountId},
	)
	if err != nil {
		return nil, err
	}
	return NilIfEmpty(resp.Tuples()), nil
}

/*
	return
 	accArr, fundMap, currArr, balanceArr
*/
func (self *FundDao) GetAllCompact() ([]interface{}, []interface{}, []interface{}, []interface{}, []interface{},
	[]interface{}, error) {
	resp, err := client.Call("fundDAO.getFundIdArr", []interface{}{})
	if err != nil {
		return nil, nil, nil, nil, nil, nil, err
	}
	tplArr := resp.Tuples()
	return tplArr[0], tplArr[1], tplArr[2], tplArr[3], tplArr[4], tplArr[5], nil
}

//get all limit max uint16
func (self *FundDao) GetAll() ([]*FundEntity, error) {
	resp, err := client.Select(SPACE_NAME_FUND, PRIMARY_INDEX, 0, math.MaxUint16,
		tarantool.IterAll, []interface{}{})
	if err != nil {
		return nil, err
	}
	return self.unpackList(resp.Tuples())
}

//single object
func (self *FundDao) unpack(respData [][]interface{}) (*FundEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

//list
func (self *FundDao) unpackList(respData [][]interface{}) ([]*FundEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *FundEntity = nil
	respArr := make([]*FundEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity
		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}

func (dao *FundDao) MapFromInterface(data []interface{}) *FundEntity {
	entity := &FundEntity{}
	entity.Id = data[0].(uint64)
	entity.AccountId = uint32(data[1].(uint64))
	entity.CurrId = uint16(data[2].(uint64))
	entity.Balance = data[3].(uint64)
	entity.Reserved = data[4].(uint64)
	entity.TotalVolumeAmount = data[5].(uint64)
	entity.State = FundState(data[6].(uint64))
	entity.TotalCommission = data[7].(uint64)
	entity.TotalCashIn = data[8].(uint64)
	entity.TotalCashOut = data[9].(uint64)
	entity.ResetStatTime = data[10].(uint64)
	return entity
}
