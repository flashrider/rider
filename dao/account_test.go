package dao

import (
	"fmt"
	"testing"
)

func TestInsertAccount(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	accDao := NewAccountDao()
	acc := &AccountEntity{}
	acc.State = ACCOUNT_STATE_ACTIVE
	acc.OtpKey = "TEST1"
	acc.UsedKeyTime = 0
	acc.CreatedTime = 0

	id, err := accDao.Insert(acc)
	if err != nil {
		panic(err)
	}

	fmt.Println("id:", id)

	acc = &AccountEntity{}
	acc.State = ACCOUNT_STATE_ACTIVE
	acc.OtpKey = "TEST2"
	acc.UsedKeyTime = 0
	acc.CreatedTime = 0

	id, err = accDao.Insert(acc)
	if err != nil {
		panic(err)
	}

	fmt.Println("id:", id)

	acc = &AccountEntity{}
	acc.State = ACCOUNT_STATE_ACTIVE
	acc.OtpKey = "TEST3"
	acc.UsedKeyTime = 0
	acc.CreatedTime = 0

	id, err = accDao.Insert(acc)
	if err != nil {
		panic(err)
	}

	fmt.Println("id:", id)
	//assert.Equal(t, uint64(0), 1)
}

func TestGetCountAccount(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()
	accDao := NewAccountDao()
	count, err := accDao.GetCount()
	if err != nil {
		panic(err)
	}
	fmt.Println("count:", count)
}
