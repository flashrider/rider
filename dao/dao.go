package dao

import (
	"errors"
	"github.com/tarantool/go-tarantool"
	"math"
	"time"
)

const (
	SPACE_NAME_ACCOUNT         = "ACCOUNT"
	SPACE_NAME_CURRENCY        = "CURRENCY"
	SPACE_NAME_EXT_TX          = "EXT_TX"
	SPACE_NAME_FUND            = "FUND"
	SPACE_NAME_INSTRUMENT      = "INSTRUMENT"
	SPACE_NAME_ORDER           = "ORDER"
	SPACE_NAME_ORDER_BOOK      = "ORDER_BOOK"
	SPACE_NAME_TRADE           = "TRADE"
	SPACE_NAME_POSITION        = "POSITION"
	SPACE_NAME_SESSION_HISTORY = "SESSION_HISTORY"
	SPACE_NAME_OHLC_ASK        = "OHLC_ASK"
	SPACE_NAME_OHLC_BID        = "OHLC_BID"

	PRIMARY_INDEX    = "PRIMARY"
	ACCOUNT_ID_INDEX = "ACCOUNT_ID_INDEX"
)

var (
	client   *tarantool.Connection = nil
	emptyErr                       = errors.New("empty")
)

//ArtyomA
//init Tarantool DB client
//Tarantool is an open-source NoSQL database management system and Lua application server.
/*
	run Tarantool: tarantool glp_db.lua
	Connect to server(terminal) : tarantoolctl connect myuser:mypass@127.0.0.1:3302

	Popular commands:
 	select all : box.space.anti_tampering:select()
	select by primay key : box.space.internal_tx:select{1}
	box.space.internal_tx.index.txIdIndex:select({'AAAAAA'},{iterator=box.index.EQ})
	box.space.anti_tampering.index.primary:select({"CEADA5D0EDF57F11ED402E9E25863637FEF088C54176CAFA4EB0CEB7D9FE6A00"},{iterator=box.index.EQ})
	box.space.wallet:select({},{iterator=box.index.All, offset=0, limit=100})
	drop index : box.space.space55.index.primary:drop()
	drop space : box.space.space55:drop()
	su user : box.session.su('admin')
	show all indexes and primary keys box.space._index:select{}
	show all spaces : box.space._space:select({})
	drop sequence: box.sequence.extTxrowIdSeq:drop()
	get space description:  box.space._space:select(530)
*/

func NewTarantoolClient(server, user, pass string, maxConn uint32) {
	if client == nil {
		if server != "" {
			opts := tarantool.Opts{
				Timeout:       4 * time.Second,
				Reconnect:     1 * time.Second,
				MaxReconnects: math.MaxUint64,
				User:          user,
				Pass:          pass,
				Concurrency:   maxConn,
			}

			clientLocal, err := tarantool.Connect(server, opts)
			if err != nil {
				// TODO Fix correctly or remove when everyone moves to Linux
				// Try twice as test cases failing 50% of the time when running tests on Windows
				clientLocal, err = tarantool.Connect(server, opts)
				if err != nil {
					panic(err)
				}
			}
			client = clientLocal
		} else {
			panic(errors.New("server field is empty !"))
		}
	} else {
		panic(errors.New("client already defined !"))
	}
}

//Close Tarantool connection pool
func Close() error {
	if client != nil {
		err := client.Close()
		client = nil
		return err
	}
	return nil
}

/****
Utility methods
*/
//return record count by index + query
func TupleCount(spaceName string, index string, val ...interface{}) (uint64, error) {
	resp, err := client.Call("box.space."+spaceName+".index."+index+":count", val)
	if err != nil {
		return 0, err
	}
	return ToUint64(resp), nil
}

//return all tuples count
func TupleAllCount(spaceName string) (uint64, error) {
	resp, err := client.Call("box.space."+spaceName+":count", []interface{}{})
	if err != nil {
		return 0, err
	}
	return ToUint64(resp), nil
}

//return true if value present
func HasTuple(spaceName string, index string, val ...interface{}) (bool, error) {
	resp, err := client.Call("box.space."+spaceName+".index."+index+":count", val)
	if err != nil {
		return false, err
	}
	if ToUint64(resp) > 0 {
		return true, nil
	}
	return false, nil
}

func GetMaxAtIndex(spaceName string, indexName string) (*tarantool.Response, error) {
	resp, err := client.Call("box.space."+spaceName+".index."+indexName+":max", []interface{}{})
	if err != nil {
		return nil, err
	}
	return resp, nil
}

// if empty assign nil
func NilIfEmpty(tupleArr [][]interface{}) [][]interface{} {
	if len(tupleArr) == 0 || len(tupleArr[0]) == 0 {
		tupleArr = nil
	}
	return tupleArr
}

//Tarantool response to uint64
func ToUint64(resp *tarantool.Response) uint64 {
	return resp.Tuples()[0][0].(uint64)
}

//Tarantool response to uint8
func ToUint8(resp *tarantool.Response) uint8 {
	return uint8(resp.Tuples()[0][0].(uint64))
}

//Tarantool response to uint8
func ToUint16(resp *tarantool.Response) uint16 {
	return uint16(resp.Tuples()[0][0].(uint64))
}

//Tarantool response to bool
func ToBool(resp *tarantool.Response) bool {
	return resp.Tuples()[0][0].(bool)
}
