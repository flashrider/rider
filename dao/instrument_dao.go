package dao

import (
	"github.com/tarantool/go-tarantool"
	"math"
)

type InstrumentMarketType uint8
type InstrumentSessionState uint8

const (
	INSTRUMENT_MARKET_FX_TYPE      InstrumentMarketType = 1
	INSTRUMENT_MARKET_CRYPTO_TYPE  InstrumentMarketType = 2
	INSTRUMENT_MARKET_FUTURES_TYPE InstrumentMarketType = 3
	INSTRUMENT_MARKET_CFD_TYPE     InstrumentMarketType = 4

	INSTRUMENT_SESSION_OPEN_STATE    InstrumentSessionState = 1
	INSTRUMENT_SESSION_HALTED_STATE  InstrumentSessionState = 2
	INSTRUMENT_SESSION_EXPIRED_STATE InstrumentSessionState = 3
)

//ownerIndex
type InstrumentDao struct {
}

func NewInstrumentDao() *InstrumentDao {
	return &InstrumentDao{}
}

//get by id
func (self *InstrumentDao) GetById(id uint8) (*InstrumentEntity, error) {
	resp, err := client.Select(SPACE_NAME_INSTRUMENT, PRIMARY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{id})
	if err != nil {
		return nil, err
	}
	return self.unpack(resp.Tuples())
}

//insert order
func (self *InstrumentDao) Insert(instrument *InstrumentEntity) (uint16, error) {
	resp, err := client.Insert(
		SPACE_NAME_INSTRUMENT,
		[]interface{}{
			nil,
			instrument.Symbol,
			instrument.BaseCurrCodeId,
			instrument.QuoteCurrCodeId,
			instrument.SessionState,
			instrument.SessionOpenTime,
			instrument.SessionEndTime,
			instrument.MarketType,
			instrument.Decimal,
			instrument.FeeAccountId,
			instrument.UpdateTime,
			instrument.Version,
			instrument.MinAmount,
			instrument.OrderBookMsgSeqNum,
		},
	)
	if err != nil {
		return 0, err
	}

	return ToUint16(resp), nil
}

//get all
func (self *InstrumentDao) GetAll() ([]*InstrumentEntity, error) {
	resp, err := client.Select(SPACE_NAME_INSTRUMENT, PRIMARY_INDEX, 0, math.MaxUint16,
		tarantool.IterAll, []interface{}{})
	if err != nil {
		return nil, err
	}
	return self.unpackList(resp.Tuples())
}

//single object
func (self *InstrumentDao) unpack(respData [][]interface{}) (*InstrumentEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

//list
func (self *InstrumentDao) unpackList(respData [][]interface{}) ([]*InstrumentEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *InstrumentEntity = nil
	respArr := make([]*InstrumentEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}

func (dao *InstrumentDao) MapFromInterface(data []interface{}) *InstrumentEntity {
	entity := &InstrumentEntity{}
	entity.Id = uint16(data[0].(uint64))
	entity.Symbol = data[1].(string)
	entity.BaseCurrCodeId = uint16(data[2].(uint64))
	entity.QuoteCurrCodeId = uint16(data[3].(uint64))
	entity.SessionState = InstrumentSessionState(data[4].(uint64))
	entity.SessionOpenTime = data[5].(uint64)
	entity.SessionEndTime = data[6].(uint64)
	entity.MarketType = InstrumentMarketType(data[7].(uint64))
	entity.Decimal = uint8(data[8].(uint64))
	entity.FeeAccountId = uint32(data[9].(uint64))
	entity.UpdateTime = data[10].(uint64)
	entity.Version = uint32(data[11].(uint64))
	entity.MinAmount = uint32(data[12].(uint64))
	entity.OrderBookMsgSeqNum = data[13].(uint64)
	return entity
}
