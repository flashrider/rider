package dao

import "github.com/tarantool/go-tarantool"

const (
	POSITION_STATE_OPEN   PositionState = 1
	POSITION_STATE_CLOSED PositionState = 2
)

type PositionDao struct {
}

func NewPositionDao() *PositionDao {
	return &PositionDao{}
}

func (dao *PositionDao) getTblName(instrumentName string) string {
	return instrumentName + "_" + SPACE_NAME_POSITION
}

func (dao *PositionDao) Insert(instrumentName string, entity *PositionEntity) (uint64, error) {
	resp, err := client.Insert(
		dao.getTblName(instrumentName),
		[]interface{}{
			entity.Id,
			entity.Direction,
			entity.CloseOrderId,
			entity.AccountId,
			entity.OpenTime,
			entity.SettleDate,
			entity.Amount,
			entity.OpenPrice,
			entity.ClosePrice,
			entity.PL,
			entity.Commission,
			entity.State,
		},
	)
	if err != nil {
		return 0, err
	}
	return ToUint64(resp), nil
}

func (dao *PositionDao) GetById(symbol string, id uint64) (*PositionEntity, error) {
	resp, err := client.Select(
		dao.getTblName(symbol), PRIMARY_INDEX, 0, 1, tarantool.IterEq, []interface{}{id},
	)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (dao *PositionDao) Delete(symbol string, id uint64) (*tarantool.Response, error) {
	resp, err := client.Delete(dao.getTblName(symbol), PRIMARY_INDEX, []interface{}{id})
	if err != nil {
		return &tarantool.Response{}, err
	}
	return resp, nil
}

func (dao *PositionDao) GetAccountOpenPosition(accountId uint32) ([][]interface{}, error) {
	resp, err := client.Call("positionDAO.getOpenPositionByAccount", []interface{}{accountId})
	if err != nil {
		return nil, err
	}
	return NilIfEmpty(resp.Tuples()), nil
}

func (self *PositionDao) unpack(respData [][]interface{}) (*PositionEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (self *PositionDao) unpackList(respData [][]interface{}) ([]*PositionEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *PositionEntity = nil
	respArr := make([]*PositionEntity, len(respData))
	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}
	return respArr, nil
}

func (dao *PositionDao) MapFromInterface(data []interface{}) *PositionEntity {
	entity := &PositionEntity{}
	entity.Id = data[0].(uint64)
	entity.Direction = OrderDirection(uint8(data[1].(uint64)))
	entity.CloseOrderId = data[2].(uint64)
	entity.AccountId = uint32(data[3].(uint64))
	entity.OpenTime = data[4].(uint64)
	entity.SettleDate = data[5].(uint64)
	entity.Amount = data[6].(uint64)
	entity.OpenPrice = data[7].(uint64)
	entity.ClosePrice = data[8].(uint64)
	entity.PL = ToInt64(data[9])
	entity.Commission = data[10].(uint64)
	entity.State = PositionState(uint8(data[11].(uint64)))
	return entity
}

func ToInt64(data interface{}) int64 {
	switch data.(type) {
	case uint64:
		return int64(data.(uint64))
	case int64:
		return data.(int64)
	default:
		return 0
	}
}
