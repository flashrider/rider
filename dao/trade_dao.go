package dao

import (
	"github.com/tarantool/go-tarantool"
)

type TradeDao struct {
}

func NewTradeDao() *TradeDao {
	return &TradeDao{}
}

func (self *TradeDao) getTblName(instrumentName string) string {
	return instrumentName + "_" + SPACE_NAME_TRADE
}

func (dao *TradeDao) Insert(instrumentName string, trade *TradeEntity) (uint64, error) {
	resp, err := client.Insert(
		dao.getTblName(instrumentName),
		[]interface{}{
			nil,
			trade.Direction,
			trade.AskOrderId,
			trade.BidOrderId,
			trade.Price,
			trade.Quantity,
			trade.SettleDate,
		},
	)
	if err != nil {
		return 0, err
	}

	return ToUint64(resp), nil
}

func (dao *TradeDao) GetById(instrumentName string, id uint64) (*TradeEntity, error) {
	resp, err := client.Select(
		dao.getTblName(instrumentName), PRIMARY_INDEX, 0, 1, tarantool.IterEq, []interface{}{id},
	)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (dao *TradeDao) Delete(instrumentName string, id uint64) (*tarantool.Response, error) {
	resp, err := client.Delete(dao.getTblName(instrumentName), PRIMARY_INDEX, []interface{}{id})
	if err != nil {
		return &tarantool.Response{}, err
	}
	return resp, nil
}

func (dao *TradeDao) GetLastByPrimaryKey(instrumentName string) (*TradeEntity, error) {
	resp, err := GetMaxAtIndex(dao.getTblName(instrumentName), PRIMARY_INDEX)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (self *TradeDao) unpack(respData [][]interface{}) (*TradeEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (self *TradeDao) unpackList(respData [][]interface{}) ([]*TradeEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *TradeEntity = nil
	respArr := make([]*TradeEntity, len(respData))
	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}
	return respArr, nil
}

func (dao *TradeDao) MapFromInterface(data []interface{}) *TradeEntity {
	entity := &TradeEntity{}
	entity.Id = data[0].(uint64)
	entity.Direction = OrderDirection(uint8(data[1].(uint64)))
	entity.AskOrderId = data[2].(uint64)
	entity.BidOrderId = data[3].(uint64)
	entity.Price = data[4].(uint64)
	entity.Quantity = data[5].(uint64)
	entity.SettleDate = data[6].(uint64)
	return entity
}
