package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/tarantool/go-tarantool"
	"math"
	"math/rand"
	"testing"
	"time"
)

func TestOrderCRD(t *testing.T) {
	rand.Seed(time.Now().UnixNano())

	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewOpenOrderDao()
	newOrder := &OrderEntity{}
	newOrder.Id = rand.Uint64()
	newOrder.PositionId = uint64(2)
	newOrder.AccountId = uint32(1)
	newOrder.Kind = ORDER_KIND_ENTRY
	newOrder.Type = ORDER_TYPE_LIMIT
	newOrder.Direction = ORDER_DIRECTION_SELL
	newOrder.AmountRest = uint64(100)
	newOrder.Price = uint64(500)
	newOrder.PriceTrigger = ORDER_PRICE_TRIGGER_ASK_G
	newOrder.Amount = 250
	newOrder.State = ORDER_STATE_PARTIALLY_FILLED
	newOrder.EntryOrderId = uint64(3)
	newOrder.ExpTime = uint64(time.Now().UnixNano())
	newOrder.CreatedTime = uint64(time.Now().UnixNano())
	newOrder.UpdateTime = uint64(time.Now().UnixNano())
	newOrder.Duration = ORDER_DURATION_DAY
	newOrder.CorrelationId = rand.Uint64()

	id, err := dao.Insert(DEFAULT_INSTRUMENT, newOrder)
	if err != nil {
		panic(err)
	}

	order, err := dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, uint32(1), order.AccountId, "AccountId")
}

func TestGetOpenAndPendingOrders(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	orderDao := NewOpenOrderDao()
	result, err := orderDao.GetAccountOpenOrder(1)
	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}

func TestIpc(t *testing.T) {
	opts := tarantool.Opts{
		Timeout:       4 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: math.MaxUint64,
		User:          LOCAL_USER,
		Pass:          LOCAL_PASSWORD,
		Concurrency:   CONNECTIONS,
	}
	conn, err := tarantool.Connect("/home/tjoma/dev/tarantool_sandbox/db.sock", opts)
	if err != nil {
		panic(err)
	}

	fmt.Println(conn.ConnectedNow())

}
