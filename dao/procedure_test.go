package dao

import (
	"fmt"
	"strings"
	"testing"
)

const (
	DEFAULT_INSTRUMENT = "BTCUSD"
)

func TestCreateInstrument(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	baseId := uint8(6)
	quoteId := uint8(2)

	instrumentId, err := CreateInstrument(DEFAULT_INSTRUMENT, baseId, quoteId, INSTRUMENT_SESSION_OPEN_STATE,
		0, 0, 5, INSTRUMENT_MARKET_CRYPTO_TYPE, 3, 1000)

	if err != nil {
		panic(err)
	}
	fmt.Println(instrumentId)

	baseId = uint8(6)
	quoteId = uint8(1)
	instrumentId, err = CreateInstrument("BTCEUR", baseId, quoteId, INSTRUMENT_SESSION_OPEN_STATE,
		0, 0, 5, INSTRUMENT_MARKET_CRYPTO_TYPE, 3, 1000)
	if err != nil {
		panic(err)
	}
	fmt.Println(instrumentId)
}

/* cycle dependency
func TestWriteSnapshot(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	snapshot := &service.MemSnapshot{}
	fund := &FundCacheModel{}
	fund.Id = 1
	snapshot.UpdateFund(fund)
	order := &OrderEntity{}
	order.Id = 10000000000
	snapshot.NewOrder(order)
	ok, err := WriteSnapshot(snapshot)
	if err != nil {
		panic(err)
	}

	fmt.Println(ok)
}
*/
func TestCreateInstrumentBulk(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	instruments := buildNewInstruments()

	size := min(len(instruments), 60)
	for i := 0; i < size; i++ {
		instrument := instruments[i]
		instrumentId, err := CreateInstrument(
			instrument.Symbol,
			uint8(instrument.BaseCurrCodeId),
			uint8(instrument.QuoteCurrCodeId),
			instrument.SessionState,
			instrument.SessionOpenTime,
			instrument.SessionEndTime,
			instrument.Decimal,
			instrument.MarketType,
			uint64(instrument.FeeAccountId),
			uint64(instrument.MinAmount),
		)
		fmt.Println(err, instrumentId)
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func buildNewInstruments() []*InstrumentEntity {
	curDao := NewCurrencyDao()
	allCurrencies, err := curDao.GetAll()
	if err != nil {
		panic(err)
	}
	instrumentDao := NewInstrumentDao()
	allInstruments, err := instrumentDao.GetAll()
	fiatCurrencies := filterByCurrencyType(allCurrencies, CURRENCY_TYPE_FIAT)
	cryptoCurrencies := filterByCurrencyType(allCurrencies, CURRENCY_TYPE_CRYPTO)
	instruments := make([]*InstrumentEntity, 0)
	for _, cryptoCurrency := range cryptoCurrencies {
		for _, fiatCurrency := range fiatCurrencies {
			instrumentName := buildInstrumentName(cryptoCurrency, fiatCurrency)
			if findInstrumentByName(allInstruments, instrumentName) == nil {
				entity := buildInstrument(instrumentName, cryptoCurrency, fiatCurrency)
				instruments = append(instruments, entity)
			}
		}
	}
	return instruments
}

func buildInstrument(instrumentName string, cryptoCurrency *CurrencyEntity, fiatCurrency *CurrencyEntity) *InstrumentEntity {
	entity := &InstrumentEntity{}
	entity.Symbol = instrumentName
	entity.BaseCurrCodeId = cryptoCurrency.Id
	entity.QuoteCurrCodeId = fiatCurrency.Id
	entity.SessionState = INSTRUMENT_SESSION_OPEN_STATE
	entity.MarketType = INSTRUMENT_MARKET_CRYPTO_TYPE
	entity.SessionOpenTime = 0
	entity.SessionEndTime = 0
	entity.Decimal = 5
	entity.FeeAccountId = 3
	entity.MinAmount = 1000
	return entity
}

func buildInstrumentName(cryptoCurrency *CurrencyEntity, fiatCurrency *CurrencyEntity) string {
	var builder strings.Builder
	builder.WriteString(cryptoCurrency.Code)
	builder.WriteString(fiatCurrency.Code)
	instrumentName := builder.String()
	return instrumentName
}

func filterByCurrencyType(allCurrencies []*CurrencyEntity, currencyType CurrencyType) []*CurrencyEntity {
	currencies := make([]*CurrencyEntity, 0)
	for _, currency := range allCurrencies {
		if currency.Type == currencyType {
			currencies = append(currencies, currency)
		}
	}
	return currencies
}

func findInstrumentByName(instruments []*InstrumentEntity, name string) *InstrumentEntity {
	for _, instrument := range instruments {
		if instrument.Symbol == name {
			return instrument
		}
	}
	return nil
}
