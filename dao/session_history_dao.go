package dao

import "github.com/tarantool/go-tarantool"

type SessionHistoryDao struct {
}

func NewSessionHistoryDao() *SessionHistoryDao {
	return &SessionHistoryDao{}
}

func (dao *SessionHistoryDao) Insert(history *SessionHistoryEntity) (uint64, error) {
	resp, err := client.Insert(
		SPACE_NAME_SESSION_HISTORY,
		[]interface{}{
			nil,
			history.Instrument,
			history.BaseCurrCodeId,
			history.QuoteCurrCodeId,
			history.SessionState,
			history.SessionOpenTime,
			history.SessionEndTime,
			history.MarketType,
			history.Time,
			history.Decimal,
			history.FeeAccountId,
			history.UpdateTime,
			history.Version,
			history.MinAmount,
			history.OrderBookMsgSeqNum,
		},
	)
	if err != nil {
		return 0, err
	}
	return ToUint64(resp), nil
}

func (dao *SessionHistoryDao) GetById(id uint64) (*SessionHistoryEntity, error) {
	resp, err := client.Select(
		SPACE_NAME_SESSION_HISTORY, PRIMARY_INDEX, 0, 1, tarantool.IterEq, []interface{}{id},
	)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (dao *SessionHistoryDao) Delete(id uint64) (*tarantool.Response, error) {
	resp, err := client.Delete(SPACE_NAME_SESSION_HISTORY, PRIMARY_INDEX, []interface{}{id})
	if err != nil {
		return &tarantool.Response{}, err
	}
	return resp, nil
}

func (dao *SessionHistoryDao) unpack(respData [][]interface{}) (*SessionHistoryEntity, error) {
	entityList, err := dao.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (dao *SessionHistoryDao) unpackList(respData [][]interface{}) ([]*SessionHistoryEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *SessionHistoryEntity = nil
	respArr := make([]*SessionHistoryEntity, len(respData))
	for i, data := range respData {
		if len(data) > 0 {
			entity = dao.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}
	return respArr, nil
}

func (dao *SessionHistoryDao) MapFromInterface(data []interface{}) *SessionHistoryEntity {
	entity := &SessionHistoryEntity{}
	entity.Id = data[0].(uint64)
	entity.Instrument = data[1].(string)
	entity.BaseCurrCodeId = uint16(data[2].(uint64))
	entity.QuoteCurrCodeId = uint16(data[3].(uint64))
	entity.SessionState = InstrumentSessionState(data[4].(uint64))
	entity.SessionOpenTime = data[5].(uint64)
	entity.SessionEndTime = data[6].(uint64)
	entity.MarketType = InstrumentMarketType(data[7].(uint64))
	entity.Time = data[8].(uint64)
	entity.Decimal = uint8(data[9].(uint64))
	entity.FeeAccountId = uint32(data[10].(uint64))
	entity.UpdateTime = data[11].(uint64)
	entity.Version = uint32(data[12].(uint64))
	entity.MinAmount = uint32(data[13].(uint64))
	entity.OrderBookMsgSeqNum = data[14].(uint64)
	return entity
}
