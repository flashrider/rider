package dao

import "github.com/tarantool/go-tarantool"

type ResolutionType uint8

const (
	//OHLC_RESOLUTION_TICK  	ResolutionType = 1
	OHLC_RESOLUTION_2S  ResolutionType = 2
	OHLC_RESOLUTION_10S ResolutionType = 3
	OHLC_RESOLUTION_M   ResolutionType = 4
	OHLC_RESOLUTION_5M  ResolutionType = 5
	OHLC_RESOLUTION_15M ResolutionType = 6
	OHLC_RESOLUTION_H   ResolutionType = 7
	OHLC_RESOLUTION_4H  ResolutionType = 8
	OHLC_RESOLUTION_D   ResolutionType = 9
	OHLC_RESOLUTION_W   ResolutionType = 10
)

var (
	RESOLUTION_LIST []ResolutionType = []ResolutionType{OHLC_RESOLUTION_2S, OHLC_RESOLUTION_10S, OHLC_RESOLUTION_M,
		OHLC_RESOLUTION_5M, OHLC_RESOLUTION_15M, OHLC_RESOLUTION_H, OHLC_RESOLUTION_4H, OHLC_RESOLUTION_D}
)

type OhlcDao struct {
	SpaceName string
}

func NewOhlcDao(spaceName string) *OhlcDao {
	return &OhlcDao{SpaceName: spaceName}
}

func (dao *OhlcDao) getTblName(instrumentName string) string {
	return instrumentName + "_" + dao.SpaceName
}

func (dao *OhlcDao) Insert(instrumentName string, ohlc *OhlcEntity) (uint64, error) {
	resp, err := client.Insert(
		dao.getTblName(instrumentName),
		[]interface{}{
			nil,
			ohlc.Datetime,
			ohlc.Open,
			ohlc.High,
			ohlc.Low,
			ohlc.Close,
			ohlc.Volume,
			ohlc.Resolution,
		},
	)
	if err != nil {
		return 0, err
	}

	return ToUint64(resp), nil
}

func (dao *OhlcDao) GetById(instrumentName string, id uint64) (*OhlcEntity, error) {
	resp, err := client.Select(
		dao.getTblName(instrumentName), PRIMARY_INDEX, 0, 1, tarantool.IterEq, []interface{}{id},
	)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (dao *OhlcDao) GetLast(instrumentName string, resolution ResolutionType) (*OhlcEntity, error) {
	//TODO
	return nil, nil
}

func (dao *OhlcDao) Delete(instrumentName string, id uint64) (*tarantool.Response, error) {
	resp, err := client.Delete(dao.getTblName(instrumentName), PRIMARY_INDEX, []interface{}{id})
	if err != nil {
		return &tarantool.Response{}, err
	}
	return resp, nil
}

func (dao *OhlcDao) unpack(respData [][]interface{}) (*OhlcEntity, error) {
	entityList, err := dao.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (dao *OhlcDao) unpackList(respData [][]interface{}) ([]*OhlcEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *OhlcEntity = nil
	respArr := make([]*OhlcEntity, len(respData))
	for i, data := range respData {
		if len(data) > 0 {
			entity = dao.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}
	return respArr, nil
}

func (dao *OhlcDao) MapFromInterface(data []interface{}) *OhlcEntity {
	entity := &OhlcEntity{}
	entity.Id = data[0].(uint64)
	entity.Datetime = data[1].(uint64)
	entity.Open = data[2].(uint64)
	entity.High = data[3].(uint64)
	entity.Low = data[4].(uint64)
	entity.Close = data[5].(uint64)
	entity.Volume = data[6].(uint64)
	entity.Resolution = ResolutionType(uint8(data[7].(uint64)))
	return entity
}
