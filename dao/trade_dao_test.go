package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTradeCRD(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewTradeDao()
	newTrade := &TradeEntity{}
	newTrade.Direction = ORDER_DIRECTION_BUY
	newTrade.AskOrderId = uint64(42)
	newTrade.BidOrderId = uint64(3007)
	newTrade.Price = uint64(100)
	newTrade.Quantity = uint64(3)
	newTrade.SettleDate = uint64(7890123)

	id, err := dao.Insert(DEFAULT_INSTRUMENT, newTrade)
	if err != nil {
		panic(err)
	}

	trade, err := dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, ORDER_DIRECTION_BUY, trade.Direction, "Direction")
	assert.Equal(t, uint64(42), trade.AskOrderId, "AskOrderId")
	assert.Equal(t, uint64(3007), trade.BidOrderId, "BidOrderId")
	assert.Equal(t, uint64(100), trade.Price, "Price")
	assert.Equal(t, uint64(3), trade.Quantity, "Quantity")
	assert.Equal(t, uint64(7890123), trade.SettleDate, "SettleDate")

	lastTrade := &TradeEntity{}
	lastTradeId, err := dao.Insert(DEFAULT_INSTRUMENT, lastTrade)
	if err != nil {
		panic(err)
	}

	lastTrade, err = dao.GetLastByPrimaryKey(DEFAULT_INSTRUMENT)

	if err != nil {
		panic(err)
	}

	assert.Equal(t, lastTradeId, lastTrade.Id, "LastId")

	res, err := dao.Delete(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	trade, err = dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, trade, "Deleted Trade should not exist")
}
