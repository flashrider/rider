package dao

/*
	Create new instrument record
	- create order,trade tbl
	return instrument id(uint16) and error obj. if error return 0 and error obj
*/
func CreateInstrument(instrumentName string, bCurrId, qCurrId uint8, sessionState InstrumentSessionState,
	sessionOpenTime, sessionEndTime uint64, decimals uint8, marketType InstrumentMarketType,
	feeAccount, minAmount uint64) (uint16, error) {
	resp, err := client.Call("createInstrument", []interface{}{instrumentName, bCurrId, qCurrId, sessionState,
		sessionOpenTime, sessionEndTime, marketType, decimals, feeAccount, minAmount})

	if err != nil {
		return 0, err
	}

	return ToUint16(resp), nil
}

func WriteSnapshot(memSnapshot interface{}) (bool, error) {
	//TODO replace to async
	resp, err := client.Call("writeSnapshot", []interface{}{memSnapshot})

	if err != nil {
		return false, err
	}

	return ToBool(resp), nil
}
