package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOhlcAskCRD(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewOhlcAskDao()
	newOhlcAsk := &OhlcEntity{}
	newOhlcAsk.Datetime = uint64(1)
	newOhlcAsk.Open = uint64(2)
	newOhlcAsk.High = uint64(3)
	newOhlcAsk.Low = uint64(4)
	newOhlcAsk.Close = uint64(5)
	newOhlcAsk.Volume = uint64(6)
	newOhlcAsk.Resolution = OHLC_RESOLUTION_2S

	id, err := dao.Insert(DEFAULT_INSTRUMENT, newOhlcAsk)
	if err != nil {
		panic(err)
	}

	ohlcAsk, err := dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, uint64(1), ohlcAsk.Datetime, "Datetime")
	assert.Equal(t, uint64(2), ohlcAsk.Open, "Open")
	assert.Equal(t, uint64(3), ohlcAsk.High, "High")
	assert.Equal(t, uint64(4), ohlcAsk.Low, "Low")
	assert.Equal(t, uint64(5), ohlcAsk.Close, "Close")
	assert.Equal(t, uint64(6), ohlcAsk.Volume, "Volume")
	assert.Equal(t, OHLC_RESOLUTION_2S, ohlcAsk.Resolution, "Resolution")

	res, err := dao.Delete(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	ohlcAsk, err = dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, ohlcAsk, "Deleted OhlcAsk should not exist")
}
