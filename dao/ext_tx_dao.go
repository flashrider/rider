package dao

import "github.com/tarantool/go-tarantool"

type TxType uint8

const (
	TX_TYPE_CASHIN  TxType = 1
	TX_TYPE_CASHOUT TxType = 2
)

type ExtTxDao struct {
}

func NewExtTxDao() *ExtTxDao {
	return &ExtTxDao{}
}

func (dao *ExtTxDao) Insert(extTx *ExtTxEntity) (uint64, error) {
	resp, err := client.Insert(
		SPACE_NAME_EXT_TX,
		[]interface{}{
			nil,
			extTx.AccountId,
			extTx.CurrencyId,
			extTx.Type,
			extTx.Amount,
			extTx.Timestamp,
		},
	)
	if err != nil {
		return 0, err
	}
	return ToUint64(resp), nil
}

func (dao *ExtTxDao) GetById(id uint64) (*ExtTxEntity, error) {
	resp, err := client.Select(
		SPACE_NAME_EXT_TX, PRIMARY_INDEX, 0, 1, tarantool.IterEq, []interface{}{id},
	)
	if err != nil {
		return nil, err
	}
	return dao.unpack(resp.Tuples())
}

func (dao *ExtTxDao) Delete(id uint64) (*tarantool.Response, error) {
	resp, err := client.Delete(SPACE_NAME_EXT_TX, PRIMARY_INDEX, []interface{}{id})
	if err != nil {
		return &tarantool.Response{}, err
	}
	return resp, nil
}

func (self *ExtTxDao) unpack(respData [][]interface{}) (*ExtTxEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (self *ExtTxDao) unpackList(respData [][]interface{}) ([]*ExtTxEntity, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *ExtTxEntity = nil
	respArr := make([]*ExtTxEntity, len(respData))
	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}
	return respArr, nil
}

func (dao *ExtTxDao) MapFromInterface(data []interface{}) *ExtTxEntity {
	entity := &ExtTxEntity{}
	entity.Id = data[0].(uint64)
	entity.AccountId = uint32(data[1].(uint64))
	entity.CurrencyId = uint16(data[2].(uint64))
	entity.Type = TxType(uint8(data[3].(uint64)))
	entity.Amount = data[4].(uint64)
	entity.Timestamp = data[5].(uint64)
	return entity
}
