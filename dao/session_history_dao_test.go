package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSessionHistoryCRD(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewSessionHistoryDao()
	newSessionHistory := &SessionHistoryEntity{}
	newSessionHistory.Instrument = DEFAULT_INSTRUMENT
	newSessionHistory.BaseCurrCodeId = uint16(1)
	newSessionHistory.QuoteCurrCodeId = uint16(2)
	newSessionHistory.SessionState = INSTRUMENT_SESSION_OPEN_STATE
	newSessionHistory.SessionOpenTime = uint64(3)
	newSessionHistory.SessionEndTime = uint64(4)
	newSessionHistory.MarketType = INSTRUMENT_MARKET_CRYPTO_TYPE
	newSessionHistory.Time = uint64(5)
	newSessionHistory.Decimal = uint8(6)
	newSessionHistory.FeeAccountId = uint32(7)
	newSessionHistory.UpdateTime = uint64(8)
	newSessionHistory.Version = uint32(9)
	newSessionHistory.MinAmount = uint32(10)
	newSessionHistory.OrderBookMsgSeqNum = uint64(11)

	id, err := dao.Insert(newSessionHistory)
	if err != nil {
		panic(err)
	}

	sessionHistory, err := dao.GetById(id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, DEFAULT_INSTRUMENT, sessionHistory.Instrument, "Symbol")
	assert.Equal(t, uint16(1), sessionHistory.BaseCurrCodeId, "BaseCurrCodeId")
	assert.Equal(t, uint16(2), sessionHistory.QuoteCurrCodeId, "QuoteCurrCodeId")
	assert.Equal(t, INSTRUMENT_SESSION_OPEN_STATE, sessionHistory.SessionState, "SessionState")
	assert.Equal(t, uint64(3), sessionHistory.SessionOpenTime, "SessionOpenTime")
	assert.Equal(t, uint64(4), sessionHistory.SessionEndTime, "SessionEndTime")
	assert.Equal(t, INSTRUMENT_MARKET_CRYPTO_TYPE, sessionHistory.MarketType, "MarketType")
	assert.Equal(t, uint64(5), sessionHistory.Time, "Time")
	assert.Equal(t, uint8(6), sessionHistory.Decimal, "Decimal")
	assert.Equal(t, uint32(7), sessionHistory.FeeAccountId, "FeeAccountId")
	assert.Equal(t, uint64(8), sessionHistory.UpdateTime, "UpdateTime")
	assert.Equal(t, uint32(9), sessionHistory.Version, "Version")
	assert.Equal(t, uint32(10), sessionHistory.MinAmount, "MinAmount")
	assert.Equal(t, uint64(11), sessionHistory.OrderBookMsgSeqNum, "OrderBookMsgSeqNum")

	res, err := dao.Delete(id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	sessionHistory, err = dao.GetById(id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, sessionHistory, "Deleted SessionHistory should not exist")
}
