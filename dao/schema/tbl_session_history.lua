---
--- instruments
---

local exports = {}

if not box.space.SESSION_HISTORY then
    log.info('start create space: SESSION_HISTORY')
    local tbl = box.schema.space.create('SESSION_HISTORY', {
        engine = 'vinyl',
        format = {
            { name = 'ID',                  type = 'unsigned' },    --1 instrument id
            { name = 'SYMBOL',              type = 'string' },      --2
            { name = 'BASE_CURR_CODE_ID',   type = 'unsigned' },    --3 
            { name = 'QUOTE_CURR_CODE_ID',  type = 'unsigned' },    --4 
            { name = 'SESSION_STATE',       type = 'unsigned' },    --5
            { name = 'SESSION_OPEN_TIME',   type = 'unsigned' },    --6
            { name = 'SESSION_END_TIME',    type = 'unsigned' },    --7
            { name = 'MARKET_TYPE',         type = 'unsigned' },    --8 market type s/f
            { name = 'TIME',                type = 'unsigned' },    --11 time of the last trade
            { name = 'DECIMALS',            type = 'unsigned' },    --12 decimals cound for price and amount
            { name = 'FEE_ACCOUNT_ID',      type = 'unsigned' },    --13 fee accomulation account
            { name = 'UPDATE_TIME',         type = 'unsigned' },    --14 instrument update time
            { name = 'VERSION',             type = 'unsigned' },    --15 instrument version increment
            { name = 'MIN_AMOUNT',          type = 'unsigned' },    --16 minimum amount, for material 1kg = 0.001 = 100 if 5 decimals
            { name = 'ORDER_BOOK_MSG_SEQ_NUM',type = 'unsigned' },  --17 order book changes (message seq number)
        },
    })

    box.schema.sequence.create('SESSION_HISTORY_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', { sequence = 'SESSION_HISTORY_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('INSTRUMENT_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 2, 'string'},
    })

    log.info('end create')
end



return exports

