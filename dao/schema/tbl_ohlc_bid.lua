---
--- ohlc_bid
---

local exports = {}

exports.getTblName = function(symbol)
    return symbol .. '_OHLC_BID'
end

exports.createTbl = function(symbol)
    local tblName = ohlcBidDAO.getTblName(symbol)

    log.info('start create space: ' .. tblName)
    local tbl = box.schema.space.create(tblName, {
        engine = 'vinyl',
        format = {
            { name = 'ID',          type = 'unsigned' },    --1
            { name = 'DATETIME',    type = 'unsigned' },    --2
            { name = 'OPEN',        type = 'unsigned' },    --3
            { name = 'HIGH',        type = 'unsigned' },    --4
            { name = 'LOW',         type = 'unsigned' },    --5
            { name = 'CLOSE',       type = 'unsigned' },    --6
            { name = 'VOLUME',      type = 'unsigned' },    --7
            { name = 'RESOLUTION',  type = 'unsigned' },    --8
        },
    })

    local seqName = tblName .. '_PRIMARY_SEQ'
    box.schema.sequence.create(seqName, {})
    tbl:create_index('PRIMARY', { sequence = seqName, parts = { 1, 'unsigned' } })

    log.info('end create')
end

-- print stat about instrument ohlc_bid
exports.stat = function(symbol)
    local tblName = ohlcBidDAO.getTblName(symbol)
    local tblSize = box.space[tblName]:bsize()
    local avgSingleTupleSize = tblSize / box.space[tblName]:len()

    return tblSize, avgSingleTupleSize, box.space[tblName].index.PRIMARY:bsize()
end

exports.insert = function(tblName, e)
    box.space[tblName]:insert{e.Id, e.Datetime, e.Open, e.High, e.Low, e.Close, e.Volume, e.Resolution}
end

return exports
