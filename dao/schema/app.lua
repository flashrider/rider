log = require('log')
clock = require('clock')
-- global variables

accountDAO      = nil
currencyDAO     = nil
extTxDAO        = nil
fundDAO         = nil
instrumentDAO   = nil
orderDAO        = nil
tradeDAO        = nil
positionDAO     = nil
ohlcAskDAO      = nil
ohlcBidDAO      = nil

-- start application
local function start()
    box.once("init", function()
        box.schema.user.create('truser', { password = 'testPass' })
        box.schema.user.grant('truser', 'execute,read,write,create,alter', 'universe')
    end)
    --
    -- Tables. If you add a new table or service, add free resources to stop() func
    --
    accountDAO      = require("tbl_account")
    currencyDAO     = require("tbl_currency")
    extTxDAO        = require("tbl_ext_tx")
    fundDAO         = require("tbl_fund")
    instrumentDAO   = require("tbl_instrument")
    orderDAO        = require("tbl_order")
    tradeDAO        = require("tbl_trade")
    positionDAO     = require("tbl_position")
    sesHistoryDAO   = require("tbl_session_history")
    ohlcAskDAO      = require("tbl_ohlc_ask")
    ohlcBidDAO      = require("tbl_ohlc_bid")
end

-- stop application
local function stop()
    --
    -- unload services
    --
    package.loaded['tbl_account']           = nil
    package.loaded['tbl_currency']          = nil
    package.loaded['tbl_ext_tx']            = nil
    package.loaded['tbl_fund_tx']           = nil
    package.loaded['tbl_instrument']        = nil
    package.loaded['tbl_order']             = nil
    package.loaded['tbl_trade']             = nil
    package.loaded['tbl_position']          = nil
    package.loaded['tbl_session_history']   = nil
    package.loaded['tbl_ohlc_ask']          = nil
    package.loaded['tbl_ohlc_bid']            = nil
end

-- create instruemnt procedure
function createInstrument(symbol, bCurrCode, qCurrCode, sessionStatus, sessionOpenTime, sessionEndTime, decimals,
     marketId, feeAccount, minAmount)
    --validate currencies
    local bCurrency = currencyDAO.getCurrency(bCurrCode)
    if bCurrency == nil then
        log.error("currency not found: " .. tostring(bCurrCode))
        return false
    end  

    local qCurrency  = currencyDAO.getCurrency(qCurrCode)
    if qCurrency == nil then
        log.error("currency not found: " .. tostring(qCurrCode))
        return false
    end  
    
    symbol = string.upper(symbol)
    log.info('start create instrument: ' .. symbol)
    log.info('start insert instrument: ' .. symbol)
    local instrument = instrumentDAO.insertRecord(symbol, bCurrCode, qCurrCode, sessionStatus, sessionOpenTime,
        sessionEndTime, decimals, marketId, feeAccount, minAmount)
    log.info('end')
    orderDAO.createTbl(symbol)
    tradeDAO.createTbl(symbol)
    positionDAO.createTbl(symbol)
    ohlcAskDAO.createTbl(symbol)
    ohlcBidDAO.createTbl(symbol)
    log.info('end create instrument: ' .. symbol)

    return instrument.ID
end

-- write a snapshot
function writeSnapshot(snapshot)
    -- log.info(snapshot)
    log.info("%s %d", snapshot.Symbol, tonumber(snapshot.Id))
    local now = clock.time64() / 1000;
    box.begin()
   
    -- new orders
    local orderTblName = orderDAO.getTblName(snapshot.Symbol)
    for _, obj in pairs(snapshot.NewOrderList) do
        orderDAO.insert(orderTblName, obj)
    end    
    -- update orders
    for _, obj in pairs(snapshot.UpdateOrderList) do
        orderDAO.update(orderTblName, obj, now)
    end
    -- update funds
    for _, obj in pairs(snapshot.UpdateFundList) do
        fundDAO.update(obj)
    end
    -- new trades
    local tradeTblName = tradeDAO.getTblName(snapshot.Symbol)
    for _, obj in pairs(snapshot.TradeList) do
        tradeDAO.insert(tradeTblName, obj)
    end
    -- new positions
    local positionTblName = positionDAO.getTblName(snapshot.Symbol)
    for _, obj in pairs(snapshot.NewPositionList) do
        positionDAO.insert(positionTblName, obj)
    end
    -- update positions
    for _, obj in pairs(snapshot.UpdatePositionList) do
        positionDAO.update(positionTblName, obj)
    end

    box.commit()
    return true
end

-- reset data
-- to run from console: resetData(50000000000)
function resetData(fundBalance)
    log.info('start reset data')
    for _, instr in box.space.INSTRUMENT:pairs() do
        orderTblName = orderDAO.getTblName(instr.SYMBOL)
        log.info('deleting all orders in: ' .. orderTblName)
        box.space[orderTblName]:truncate()
        positionTblName = positionDAO.getTblName(instr.SYMBOL)
        log.info('deleting all positions in: ' .. positionTblName)
        box.space[positionTblName]:truncate()
    end
    log.info('updating funds')
    for _, fund in box.space.FUND:pairs() do
        box.space.FUND:update(fund.ID, {{'=', 4, fundBalance}, {'=', 5, 0}, {'=', 6, 0}})
    end
    box.commit()
    log.info('end reset data')
    return true
end

return {
    start = start,
    stop  = stop
}

