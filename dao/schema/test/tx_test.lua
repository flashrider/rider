log = require('log')
clock = require('clock')

function testCreateTx (instrumentName)
    for i = 1, 20000000 do
        txDAO.insertRecord(instrumentName, i, i, i, i, i, i, i)
    end
end

--
-- 10instruments * 20mil of records = 200; table size:1119828507; primary index size: 23849446
-- box.space.BTC_LTC_tx:select(11000000) - 33699 nano sec (HDD 7200RPM sata2)
-- on HDD 100mil of txs size 5.8G
--
function testCreateTxForInstrument()
    local instrumentArr = {'BTC_LTC', 'BTC_USD', 'BTC_ETH', 'BTC_EOS', 'BTC_XLM', 'BTC_BCH', 'BTC_USDT', 'BTC_XEM', 
        'BTC_DASH', 'BTC_EUR'}

    for  i,val in pairs(instrumentArr) do
        createInstrument(val)
        log.info("start write " .. val)
        testCreateTx(val)
        log.info("end write")
    end 
    
end    

function testTxElTimeSelect()
    local now = clock.time64()
    box.space.BTC_LTC_tx:select(11000000)
    return clock.time64() - now
end


function testTxByTime(instrumentName, from, to)
    local now = clock.time64()
    if from > to then
        from = to
    end
    if instrumentName == '' then
        return "invalid instrumentName argument"
    end    
    local tblName = 'BTC_LTC_TX'

    local rows = {}
    local i = 1
    for _, t in box.space[tblName].index.timeIndex:pairs({from}, {iterator = box.index.GE}) do
        if (t[8] <= to) then
            rows[i] = t
            i = i + 1
        else
            return rows, clock.time64() - now
        end
    end

    return rows, clock.time64() - now
end