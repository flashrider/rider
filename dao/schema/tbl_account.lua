local exports = {}

if not box.space.ACCOUNT then
    log.info('start create space ACCOUNT')
    local tbl = box.schema.space.create('ACCOUNT', {
        engine = 'vinyl',
        format = {
            { name = 'ID',           type = 'unsigned' },   --1 instrument id
            { name = 'STATE',        type = 'unsigned' },   --2 see account status
            { name = 'OTP_KEY',      type = 'string' },     --3 one time password key. Key set client sys, 
                                                            --we check this key from user WS connection, when est. conn.            
            { name = 'USED_KEY_TIME',type = 'unsigned' },    --4
            { name = 'CREATED_TIME', type = 'unsigned' },    --5 
        },
    })

    box.schema.sequence.create('ACCOUNT_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', { sequence = 'ACCOUNT_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('OTP_KEY_INDEX', {
        type = 'tree',
        unique = true,
        parts = { 3, 'string', is_nullable = true},
    })

    log.info('end create')
end

exports.stat = function()
    local tblSize = box.space.ACCOUNT:bsize()
    local avgSingleTupleSize = tblSize / box.space.ACCOUNT:len()
    
    return tblSize, avgSingleTupleSize, box.space.ACCOUNT.index.PRIMARY:bsize()
end    

return exports