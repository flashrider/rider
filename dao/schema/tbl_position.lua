---
--- Trades (txs)
---
POSITION_STATE_OPEN = 1

local exports = {}

exports.getTblName = function(symbol)
    return symbol .. '_POSITION'
end

exports.createTbl = function(symbol)
    local tblName = positionDAO.getTblName(symbol)

    log.info('start create space: ' .. tblName)
    local tbl = box.schema.space.create(tblName, {
        engine = 'vinyl',
        format = {
            { name = 'ID',              type = 'unsigned' },    --1 poisition id equals order entry Id
            { name = 'DIRECTION',       type = 'unsigned' },    --2 see order direction ask/bid. ASK=SHORT/BID=LONG
            { name = 'CLOSE_ORDER_ID',  type = 'unsigned' },    --3 close order id (TP/SL) empty if cancel TP/SL or MKT
            { name = 'ACCOUNT_ID',      type = 'unsigned' },    --4 account id
            { name = 'OPEN_TIME',       type = 'unsigned' },    --5 open entry order date
            { name = 'SETTLEDATE',      type = 'unsigned' },    --6 settlement date (matching time)
            { name = 'AMOUNT',          type = 'unsigned' },    --7 base currency amount
            { name = 'OPEN_PRICE',      type = 'unsigned' },    --8 open price
            { name = 'CLOSE_PRICE',     type = 'unsigned' },    --9 close price when filled TP/SL or canceled booth
            { name = 'PL',              type = 'number'   },    --10 amount * openPrice - amount * closePrice
            { name = 'COMMISSION',      type = 'unsigned' },    --11 commission (entryOrder) in quote currency
            { name = 'STATE',           type = 'unsigned' },    --12 position state
        },
    })

    tbl:create_index('PRIMARY', { 
        type = 'tree',
        unique = true,
        parts = { 1, 'unsigned' } 
    })

    tbl:create_index('ACCOUNT_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 4, 'unsigned' },
    })

    -- we should retrive all user open positions
    tbl:create_index('ACCOUNT_ID_AND_CLOSE_ORDER_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 4, 'unsigned', 3, 'unsigned' },
    })

    tbl:create_index('ACCOUNT_ID_AND_STATE', {
        type = 'tree',
        unique = false,
        parts = { 4, 'unsigned', 12, 'unsigned' },
    })

    log.info('end create')

end


-- print stat about instrument txs
exports.stat = function(symbol)
    local tblName = positionDAO.getTblName(symbol)
    local tblSize = box.space[tblName]:bsize()
    local avgSingleTupleSize = tblSize / box.space[tblName]:len() 
    
    return tblSize, avgSingleTupleSize, box.space[tblName].index.PRIMARY:bsize()
end    

exports.getOpenPositionByAccount = function(accountId)
    local rows = {}
    local i = 1
    local tblName = ""
    for _, instr in box.space.INSTRUMENT:pairs() do
        tblName = positionDAO.getTblName(instr.SYMBOL)
        for _, position in box.space[tblName].index.ACCOUNT_ID_INDEX:pairs({accountId}, {iterator = box.index.EQ}) do
            if position.STATE == POSITION_STATE_OPEN then
                -- add last
                rows[i] = position:transform(13, 13, instr.ID)
                i = i + 1
            end
        end
    end
    return rows
end

exports.update = function(tblName, e, updateTime)
    if e.State == POSITION_STATE_OPEN then
        box.space[tblName]:update(e.Id, {{'=', 3, e.CloseOrderId}, {'=', 6, e.SettleDate},
            {'=', 10, e.PL}, {'=', 11, e.Commission}, {'=', 12, e.State}})
    else
        box.space[tblName]:update(e.Id, {{'=', 3, e.CloseOrderId}, {'=', 6, updateTime},
            {'=', 9, e.ClosePrice}, {'=', 10, e.PL}, {'=', 11, e.Commission}, {'=', 12, e.State}})
    end

end

exports.insert = function(tblName, e)
    box.space[tblName]:insert{e.Id, e.Direction, e.CloseOrderId, e.AccountId, e.OpenTime, e.SettleDate,
     e.Amount, e.OpenPrice, e.ClosePrice, e.PL, e.Commission, e.State}
end

return exports

