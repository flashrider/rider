---
--- instruments
---
clock = require('clock')
local exports = {}

if not box.space.INSTRUMENT then
    log.info('start create space: INSTRUMENT')
    local tbl = box.schema.space.create('INSTRUMENT', {
        engine = 'memtx',
        format = {
            { name = 'ID',                  type = 'unsigned' },    --1 instrument id
            { name = 'SYMBOL',              type = 'string' },      --2
            { name = 'BASE_CURR_CODE_ID',   type = 'unsigned' },    --3 
            { name = 'QUOTE_CURR_CODE_ID',  type = 'unsigned' },    --4 
            { name = 'SESSION_STATE',       type = 'unsigned' },    --5 
            { name = 'SESSION_OPEN_TIME',   type = 'unsigned' },    --6 
            { name = 'SESSION_END_TIME',    type = 'unsigned' },    --7 
            { name = 'MARKET_TYPE',         type = 'unsigned' },    --8 market type s/f
            { name = 'DECIMALS',            type = 'unsigned' },    --9 decimals cound for price and amount
            { name = 'FEE_ACCOUNT_ID',      type = 'unsigned' },    --10 fee accomulation account
            { name = 'UPDATE_TIME',         type = 'unsigned' },    --11 instrument update time
            { name = 'VERSION',             type = 'unsigned' },    --12 instrument version increment
            { name = 'MIN_AMOUNT',          type = 'unsigned' },    --13 minimum amount, for material 1kg = 0.001 = 100 if 5 decimals
            { name = 'ORDER_BOOK_MSG_SEQ_NUM',type = 'unsigned' },  --14 order book changes (message seq number)
        },
    })

    box.schema.sequence.create('INSTRUMENT_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', {type = 'tree', sequence = 'INSTRUMENT_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('INSTRUMENT_INDEX', {
        type = 'tree',
        unique = true,
        parts = { 2, 'string'},
    })

    log.info('end create')
end
   

exports.insertRecord = function(symbol, bCurrId, qCurrId, sessionState,
    sessionOpenTime, sessionEndTime, marketType, decimals, feeAccount, minAmount)
    return box.space.INSTRUMENT:auto_increment{symbol, bCurrId, qCurrId, sessionState,
    sessionOpenTime, sessionEndTime, marketType, decimals, feeAccount, clock.time64() / 1000, 1, minAmount, 0}
end

return exports

