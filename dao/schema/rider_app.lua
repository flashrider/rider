#!/usr/bin/env tarantool
--
-- storage support hot code reload
--
log = require('log')

function script_path()
    local str = debug.getinfo(2, "S").source:sub(2)
    return str:match("(.*/)")
end

if script_path() ~= nil then
    package.path = script_path() .. "?.lua;" .. package.path
end


box.cfg({
    listen         = 3302,
    -- listen          = '/home/tjoma/dev/tarantool_sandbox/db.sock',
    -- Log level, 0 = fatal, 1 = error, 2 = crit, 3 = warn, 4 = info, 5 = debug
    log_level       = 5,
    -- DEFAULT SETTINGS
    -- The maximum number of in-memory bytes that vinyl uses.
    -- 128 * 1024 * 1024
    vinyl_memory    = 512 * 1024 * 1024,
    -- The maximal cache size for the vinyl storage engine, in bytes.
    -- 128
    vinyl_cache     = 512,
    -- The maximum number of read threads that vinyl can use for some
    -- concurrent operations, such as I/O and compression.
    vinyl_read_threads = 2,
    -- default 3600
    checkpoint_interval = 300,
    -- default 500000
    rows_per_wal = 250000
})

-- ATTENTION: unload it all properly!
local app = package.loaded['app']
if app ~= nil then
    log.info("start stop")
    -- stop the old application version
    app.stop()
    -- unload the application
    package.loaded['app'] = nil
    log.info("end stop")
end

-- rotate a log
log.rotate()
-- load the application
log.info('require app')
app = require('app')

-- start the application
app.start()