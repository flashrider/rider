---
--- Trades (txs)
---

local exports = {}

exports.getTblName = function(symbol)
    return symbol .. '_TRADE'
end

exports.createTbl = function(symbol)
    local tblName = tradeDAO.getTblName(symbol)

    log.info('start create space: ' .. tblName)
    local tbl = box.schema.space.create(tblName, {
        engine = 'vinyl',
        format = {
            { name = 'ID',          type = 'unsigned' },    --1 tx id
            { name = 'DIRECTION',   type = 'unsigned' },    --2 see order direction ask/bid
            { name = 'A_ORDER_ID',  type = 'unsigned' },    --3 ask order id
            { name = 'B_ORDER_ID',  type = 'unsigned' },    --4 bid order id
            { name = 'PRICE',       type = 'unsigned' },    --5 price
            { name = 'QUANTITY',    type = 'unsigned' },    --6 quantity
            { name = 'SETTLEDATE',  type = 'unsigned' },    --7 settlement date (matching time)
        },
    })

    local seqName = tblName .. '_PRIMARY_SEQ'
    box.schema.sequence.create(seqName, {})
    tbl:create_index('PRIMARY', { sequence = seqName, parts = { 1, 'unsigned' } })

    tbl:create_index('A_ORDER_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 3, 'unsigned' },
    })

    tbl:create_index('B_ORDER_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 4, 'unsigned' },
    })

    tbl:create_index('SETTLEDATE_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 7, 'unsigned' },
    })

    log.info('end create')

end


-- print stat about instrument txs
exports.stat = function(symbol)
    local tblName = tradeDAO.getTblName(symbol)
    local tblSize = box.space[tblName]:bsize()
    local avgSingleTupleSize = tblSize / box.space[tblName]:len() 
    
    return tblSize, avgSingleTupleSize, box.space[tblName].index.PRIMARY:bsize()
end    

exports.insert = function(tblName, e)
    box.space[tblName]:auto_increment{e.Direction, e.AskOrderId, e.BidOrderId, e.Price, e.Quantity, e.SettleDate}
end

return exports

