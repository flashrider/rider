---
--- External txs (cash-in/cash-out)
---

local exports = {}

if not box.space.EXT_TX then
    log.info('start create space: EXT_TX')
    local tbl = box.schema.space.create('EXT_TX', {
        engine = 'vinyl',
        format = {
            { name = 'ID',          type = 'unsigned' },    --1 ext tx id
            { name = 'ACCOUNT_ID',  type = 'unsigned' },    --2 account id
            { name = 'CURR_CODE',   type = 'unsigned' },    --3 currency code
            { name = 'TX_TYPE',     type = 'unsigned' },    --4 cashin/cashout
            { name = 'AMOUNT',      type = 'unsigned' },    --5 amount
            { name = 'TIMESTAMP',   type = 'unsigned' },    --6 time
        },
    })

    box.schema.sequence.create('EXT_TX_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', { sequence = 'EXT_TX_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('ACCOUNT_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 2, 'unsigned' },
    })

    tbl:create_index('ACCOUNT_ID_AND_TIMESTAMP_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 2, 'unsigned', 6, 'unsigned' },
    })

    log.info('end create')

end


-- print stat about instrument txs
exports.stat = function()
    local tblSize = box.space.EXT_TX:bsize()
    local avgSingleTupleSize = tblSize / box.space.EXT_TX:len() 
    
    return tblSize, avgSingleTupleSize, box.space.EXT_TX.index.PRIMARY:bsize()
end    

exports.insertRecord = function()
    return box.space.EXT_TX:auto_increment{aOrderId, aMount, bOrderId, bAmount, timestamp}
end



return exports

