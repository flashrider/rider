---
--- fund
---

log = require('log')

FUNDS_STATE_ACTIVE    = 1
FUNDS_STATE_DISABLED  = 2

local exports = {}

-- if already exists
if not box.space.FUND then
    log.info('start create space: FUND')
    local tbl = box.schema.space.create('FUND', {
        engine = 'vinyl',
        format = {  
            { name = 'ID',                  type = 'unsigned' },    --1 order id
            { name = 'ACCOUNT_ID',          type = 'unsigned' },    --2 account id
            { name = 'CURR_ID',             type = 'unsigned' },    --3 currency id
            { name = 'BALANCE',             type = 'unsigned' },    --4 balnce
            { name = 'RESERVED',            type = 'unsigned' },    --5 in open orders
            { name = 'TOTAL_VOLUME_AMOUNT', type = 'unsigned' },    --6 total volume amount
            { name = 'STATE',               type = 'unsigned' },    --7 see funds status
            { name = 'TOTAL_COMMISSION',    type = 'unsigned' },    --8 total commision payed
            { name = 'TOTAL_CASH_IN',       type = 'unsigned' },    --9 total cash in amount
            { name = 'TOTAL_CASH_OUT',      type = 'unsigned' },    --10 total cash out amount
            { name = 'RESET_STAT_TIME',     type = 'unsigned' },    --11 when last time reset stat data (TOTALs)
        },
    })

    box.schema.sequence.create('FUND_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', { sequence = 'FUND_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('ACCOUNT_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 2, 'unsigned' },
    })

    log.info('end create')
end

-- print stat about instrument txs
exports.stat = function()
    local tblSize = box.space.FUND:bsize()
    local avgSingleTupleSize = tblSize / box.space.FUND:len() 
    return tblSize, avgSingleTupleSize, box.space.FUND.index.PRIMARY:bsize()
end    

exports.getFundIdArr = function()
    local accArr  = {}
    local fundArr = {}
    local currArr = {}
    local balanceArr = {}
    local inOpenArrArr = {}
    local stateArr = {}
    local i = 1
    for _, t in box.space.FUND:pairs() do
        accArr[i] = t.ACCOUNT_ID
        fundArr[i] = t.ID
        currArr[i] = t.CURR_ID
        balanceArr[i] = t.BALANCE
        inOpenArrArr[i] = t.RESERVED
        stateArr[i] = t.STATE
        i = i + 1
    end

    return accArr, fundArr, currArr, balanceArr, inOpenArrArr, stateArr
end

exports.update = function(e)
    box.space.FUND:update(e.Id, {{'=', 4, e.Balance}, {'=', 5, e.Reserved}, {'=', 6, e.TotalVolumeAmount}, {'=', 7, e.State}})
end    

return exports

