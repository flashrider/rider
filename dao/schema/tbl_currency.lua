---
--- currency
---

local exports = {}

CURRENCY_TYPE_FIAT          = 1
CURRENCY_TYPE_CRYPTO        = 2
CURRENCY_TYPE_ELECTRONIC    = 3

if not box.space.CURRENCY then
    log.info('start create space: CURRENCY')
    local tbl = box.schema.space.create('CURRENCY', {
        engine = 'memtx',
        format = {
            { name = 'ID',                  type = 'unsigned' },    --1 instrument id
            { name = 'CODE',                type = 'string' },      --2 EUR/BTC/USD...
            { name = 'TYPE',                type = 'unsigned' },    --3 see currency types
            { name = 'DELISTED',            type = 'boolean' },     --4 true/false. Default false
            { name = 'UPDATE_TIME',         type = 'unsigned' },    --5 update time
            { name = 'ISSUER_CURRENCY_ID',  type = 'unsigned' },    --6 filled only for Tokens. Token exists in currency ETH, EOS 
        },
    })

    box.schema.sequence.create('CURRENCY_PRIMARY_SEQ', {})
    tbl:create_index('PRIMARY', {type = 'hash', sequence = 'CURRENCY_PRIMARY_SEQ', parts = { 1, 'unsigned' } })

    tbl:create_index('CODE_INDEX', {
        type = 'hash',
        unique = true,
        parts = { 2, 'string' },
    })

    log.info('end create')
end
   
-- get currency Id by code
exports.getCurrency = function(currencyCode)
    return box.space.CURRENCY.index.PRIMARY:select(currencyCode)
end


return exports

