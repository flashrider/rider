---
--- orders
--- ArtyomAminov
--- 12.2018
---

local exports = {}

exports.getTblName = function(symbol)
    return symbol .. '_ORDER'
end

exports.createTbl = function(symbol)
    local tblName = orderDAO.getTblName(symbol)

    log.info('start create space: ' .. tblName)
    local tbl = box.schema.space.create(tblName, {
        engine = 'vinyl',
        format = {
            { name = 'ID',              type = 'unsigned' },    --1 order id
            { name = 'POSITION_ID',     type = 'unsigned' },    --2 position ID. When created  Orders (entry+tp+sl) have same positionId after entry execution
            { name = 'ACCOUNT_ID',      type = 'unsigned' },    --3 account id
            { name = 'KIND',            type = 'unsigned' },    --4 see order kind
            { name = 'TYPE',            type = 'unsigned' },    --5 see order types
            { name = 'DIRECTION',       type = 'unsigned' },    --6 see direction ASK/BID
            { name = 'AMOUNT_REST',     type = 'unsigned' },    --7 partial execution. When order created equals quantity. Decrease capacity till 0(status is closed)
            { name = 'PRICE',           type = 'unsigned' },    --8 
            { name = 'PRICE_TRIGGER',   type = 'unsigned' },    --9 See order price trigger. Open order if ask/bid price
            { name = 'AMOUNT',          type = 'unsigned' },    --10 Amount. Never changed.
            { name = 'STATE',           type = 'unsigned' },    --11 see order state
            { name = 'ENTRY_ORDER_ID',  type = 'unsigned' },    --12 Link to Entry orderId may be 0 for entry order.
            { name = 'EXP_TIME',        type = 'unsigned' },    --13 expired time. GTD_ORDER_DURATION. Can be 0
            { name = 'CREATED_TIME',    type = 'unsigned' },    --14
            { name = 'UPD_TIME',        type = 'unsigned' },    --15 last update time
            { name = 'DURATION',        type = 'unsigned' },    --16 order duration
            { name = 'CORRELAT_ID',     type = 'unsigned' },    --17 correlation id from sender sys. Not uniq. Linked to accountId
        },
    })

    tbl:create_index('PRIMARY', {
        type = 'tree',
        unique = true,
        parts = { 1, 'unsigned' },
    })

    tbl:create_index('POSITION_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 2, 'unsigned' },
    })

    tbl:create_index('ACCOUNT_ID_INDEX', {
        type = 'tree',
        unique = false,
        parts = { 3, 'unsigned' },
    })

    tbl:create_index('STATE_INDEX', { 
        type = 'tree',
        unique = false,
        parts = { 11, 'unsigned' } 
    })

    tbl:create_index('CORRELAT_ID', {
        type = 'tree',
        unique = false,
        parts = { 3, 'unsigned', 17, 'unsigned'},
    })

    log.info('end create')
end


-- print stat about instrument txs
exports.stat = function(symbol)
    local tblName = orderDAO.getTblName(symbol)
    local tblSize = box.space[tblName]:bsize()
    local avgSingleTupleSize = tblSize / box.space[tblName]:len() 
    
    return tblSize, avgSingleTupleSize, box.space[tblName].index.PRIMARY:bsize()
end    

exports.getPendingOpenOrderByAccount = function(accountId)
    local rows = {}
    local i = 1
    local tblName = ""
    for _, instr in box.space.INSTRUMENT:pairs() do
        tblName = orderDAO.getTblName(instr.SYMBOL)
        for _, order in box.space[tblName].index.ACCOUNT_ID_INDEX:pairs({accountId}, {iterator = box.index.EQ}) do
            if order.STATE < 4 then
                -- add last
                rows[i] = order:transform(18, 18, instr.ID)
                i = i + 1
            end
        end
    end    
    return rows
end

exports.update = function(tblName, e, updateTime)
    box.space[tblName]:update(e.Id, {{'=', 2, e.PositionId},{'=', 7, e.AmountRest}, {'=', 11, e.State}, {'=', 15, updateTime}})
end

exports.insert = function(tblName, e)
    box.space[tblName]:insert{e.Id, e.PositionId, e.AccountId, e.Kind, e.Type, e.Direction, e.AmountRest, e.Price, e.PriceTrigger,
        e.Amount, e.State, e.EntryOrderId, e.ExpTime, e.CreatedTime, e.OpenTime, e.UpdateTime, e.Duration, e.CorrelationId}
end

return exports

