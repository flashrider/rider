package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
	"time"
)

func TestPositionCRD(t *testing.T) {
	rand.Seed(time.Now().UnixNano())

	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewPositionDao()
	newPosition := &PositionEntity{}
	newPosition.Id = rand.Uint64()
	newPosition.Direction = ORDER_DIRECTION_SELL
	newPosition.CloseOrderId = uint64(2)
	newPosition.AccountId = uint32(1)
	newPosition.OpenTime = uint64(123456)
	newPosition.SettleDate = uint64(789012)
	newPosition.Amount = uint64(4)
	newPosition.OpenPrice = uint64(5)
	newPosition.ClosePrice = uint64(6)
	newPosition.PL = int64(42)
	newPosition.Commission = uint64(7)
	newPosition.State = POSITION_STATE_OPEN

	id, err := dao.Insert(DEFAULT_INSTRUMENT, newPosition)
	if err != nil {
		panic(err)
	}

	position, err := dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, ORDER_DIRECTION_SELL, position.Direction, "Direction")
	assert.Equal(t, uint64(2), position.CloseOrderId, "CloseOrderId")
	assert.Equal(t, uint32(1), position.AccountId, "AccountId")
	assert.Equal(t, uint64(123456), position.OpenTime, "OpenTime")
	assert.Equal(t, uint64(789012), position.SettleDate, "SettleDate")
	assert.Equal(t, uint64(4), position.Amount, "Amount")
	assert.Equal(t, uint64(5), position.OpenPrice, "OpenPrice")
	assert.Equal(t, uint64(6), position.ClosePrice, "ClosePrice")
	assert.Equal(t, int64(42), position.PL, "PL")
	assert.Equal(t, uint64(7), position.Commission, "Commission")
	assert.Equal(t, POSITION_STATE_OPEN, position.State, "State")

	res, err := dao.Delete(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	position, err = dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, position, "Deleted Position should not exist")
}

func TestGetAccountOpenPosition(t *testing.T) {
	rand.Seed(time.Now().UnixNano())

	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewPositionDao()

	accountId := uint32(1)
	anotherAccountId := uint32(2)
	id1 := InsertNewPositionEntity(dao, accountId, POSITION_STATE_OPEN)
	id2 := InsertNewPositionEntity(dao, accountId, POSITION_STATE_CLOSED)
	id3 := InsertNewPositionEntity(dao, anotherAccountId, POSITION_STATE_OPEN)

	fmt.Println(id1, id2, id3)

	positionArr, err := dao.GetAccountOpenPosition(accountId)
	if err != nil {
		panic(err)
	}

	fmt.Println(positionArr)
}

func InsertNewPositionEntity(dao *PositionDao, accountId uint32, state PositionState) uint64 {
	position := &PositionEntity{}
	position.Id = rand.Uint64()
	position.AccountId = accountId
	position.CloseOrderId = uint64(2)
	position.OpenTime = uint64(time.Now().UnixNano())
	position.SettleDate = uint64(789012)
	position.Amount = uint64(4)
	position.OpenPrice = uint64(5)
	position.ClosePrice = uint64(6)
	position.PL = int64(42)
	position.Commission = uint64(7)
	position.State = state
	id, err := dao.Insert(DEFAULT_INSTRUMENT, position)
	if err != nil {
		panic(err)
	}
	return id
}
