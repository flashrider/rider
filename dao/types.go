package dao

/**
Artyom Aminov
12.2018
*/

type PositionState uint8

type AccountEntity struct {
	Id          uint32
	State       AccountState
	OtpKey      string
	UsedKeyTime uint64
	CreatedTime uint64
}

type CurrencyEntity struct {
	Id           uint16       `json:"id"`
	Code         string       `json:"code"`
	Type         CurrencyType `json:"type"`
	Delisted     bool         `json:"dlist"`
	UpdateTime   uint64       `json:"-"`
	IssuerCurrId uint16       `json:"insId"`
}

type ExtTxEntity struct {
	Id         uint64
	AccountId  uint32
	CurrencyId uint16
	Type       TxType
	Amount     uint64
	Timestamp  uint64
}

type FundEntity struct {
	Id                uint64
	AccountId         uint32
	CurrId            uint16
	Balance           uint64
	Reserved          uint64
	TotalVolumeAmount uint64
	State             FundState
	TotalCommission   uint64
	TotalCashIn       uint64
	TotalCashOut      uint64
	ResetStatTime     uint64
}

type InstrumentEntity struct {
	Id                 uint16                 `json:"id"`
	Symbol             string                 `json:"sm"`
	BaseCurrCodeId     uint16                 `json:"b"`
	QuoteCurrCodeId    uint16                 `json:"q"`
	SessionState       InstrumentSessionState `json:"st"`
	SessionOpenTime    uint64                 `json:"so"`
	SessionEndTime     uint64                 `json:"se"`
	MarketType         InstrumentMarketType   `json:"mt"`
	Decimal            uint8                  `json:"d"`
	FeeAccountId       uint32                 `json:"-"`
	UpdateTime         uint64                 `json:"-"`
	Version            uint32                 `json:"v"`
	MinAmount          uint32                 `json:"ma"`
	OrderBookMsgSeqNum uint64                 `json:"-"`
}

type OrderEntity struct {
	Id            uint64
	PositionId    uint64
	AccountId     uint32
	Kind          OrderKind
	Type          OrderType
	Direction     OrderDirection
	AmountRest    uint64
	Price         uint64
	PriceTrigger  OrderPriceTrigger
	Amount        uint64
	State         OrderState
	EntryOrderId  uint64
	ExpTime       uint64
	CreatedTime   uint64
	UpdateTime    uint64
	Duration      OrderDuration
	CorrelationId uint64
}

//short version of order, used only for update db, see for mode details OrderEntity
type OrderChangesEntity struct {
	Id         uint64
	AccountId  uint32
	AmountRest uint64
	State      OrderState
	PositionId uint64
}

type PositionEntity struct {
	Id           uint64
	Direction    OrderDirection
	CloseOrderId uint64
	AccountId    uint32
	OpenTime     uint64
	SettleDate   uint64
	Amount       uint64
	OpenPrice    uint64
	ClosePrice   uint64
	PL           int64
	Commission   uint64
	State        PositionState
}

type SessionHistoryEntity struct {
	Id                 uint64
	Instrument         string
	BaseCurrCodeId     uint16
	QuoteCurrCodeId    uint16
	SessionState       InstrumentSessionState
	SessionOpenTime    uint64
	SessionEndTime     uint64
	MarketType         InstrumentMarketType
	Time               uint64
	Decimal            uint8
	FeeAccountId       uint32
	UpdateTime         uint64
	Version            uint32
	MinAmount          uint32
	OrderBookMsgSeqNum uint64
}

type TradeEntity struct {
	Id         uint64         `json:"id"`
	Direction  OrderDirection `json:"d"`
	AskOrderId uint64         `json:"sI"`
	BidOrderId uint64         `json:"bI"`
	Price      uint64         `json:"p"`
	Quantity   uint64         `json:"q"`
	SettleDate uint64         `json:"s"`
}

type OhlcEntity struct {
	Id         uint64
	Datetime   uint64
	Open       uint64
	High       uint64
	Low        uint64
	Close      uint64
	Volume     uint64
	Resolution ResolutionType
}

type FundCacheModel struct {
	Id                uint64
	AccountId         uint32
	Balance           uint64
	Reserved          uint64
	State             FundState //do not write to DB (only read)
	TotalVolumeAmount uint64
}
