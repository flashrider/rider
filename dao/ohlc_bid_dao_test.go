package dao

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOhlcBidCRD(t *testing.T) {
	NewTarantoolClient(LOCAL_SERVER, LOCAL_USER, LOCAL_PASSWORD, CONNECTIONS)
	defer Close()

	dao := NewOhlcBidDao()
	newOhlcBid := &OhlcEntity{}
	newOhlcBid.Datetime = uint64(1)
	newOhlcBid.Open = uint64(2)
	newOhlcBid.High = uint64(3)
	newOhlcBid.Low = uint64(4)
	newOhlcBid.Close = uint64(5)
	newOhlcBid.Volume = uint64(6)
	newOhlcBid.Resolution = OHLC_RESOLUTION_2S

	id, err := dao.Insert(DEFAULT_INSTRUMENT, newOhlcBid)
	if err != nil {
		panic(err)
	}

	ohlcBid, err := dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Equal(t, uint64(1), ohlcBid.Datetime, "Datetime")
	assert.Equal(t, uint64(2), ohlcBid.Open, "Open")
	assert.Equal(t, uint64(3), ohlcBid.High, "High")
	assert.Equal(t, uint64(4), ohlcBid.Low, "Low")
	assert.Equal(t, uint64(5), ohlcBid.Close, "Close")
	assert.Equal(t, uint64(6), ohlcBid.Volume, "Volume")
	assert.Equal(t, OHLC_RESOLUTION_2S, ohlcBid.Resolution, "Resolution")

	res, err := dao.Delete(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	fmt.Println("res:", res, "err:", err)

	ohlcBid, err = dao.GetById(DEFAULT_INSTRUMENT, id)
	if err != nil {
		panic(err)
	}

	assert.Nil(t, ohlcBid, "Deleted OhlcBid should not exist")
}
