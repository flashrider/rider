package dao

import (
	"github.com/tarantool/go-tarantool"
)

type AccountState uint8

const (
	ACCOUNT_OTP_KEY_INDEX string = "OTP_KEY_INDEX"

	ACCOUNT_STATE_ACTIVE  AccountState = 1
	ACCOUNT_STATE_BLOCKED AccountState = 2
)

//ownerIndex
type AccountDao struct {
}

func NewAccountDao() *AccountDao {
	return &AccountDao{}
}

func (self *AccountDao) GetById(id uint32) (*AccountEntity, error) {
	resp, err := client.Select(SPACE_NAME_ACCOUNT, PRIMARY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{id})
	if err != nil {
		return nil, err
	}
	return self.unpack(resp.Tuples())
}

func (self *AccountDao) GetByOtp(otpKey string) (*AccountEntity, error) {
	resp, err := client.Select(SPACE_NAME_ACCOUNT, ACCOUNT_OTP_KEY_INDEX, 0, 1, tarantool.IterEq,
		[]interface{}{otpKey})
	if err != nil {
		return nil, err
	}
	return self.unpack(resp.Tuples())
}

func (self *AccountDao) GetCount() (uint64, error) {
	return TupleCount(SPACE_NAME_ACCOUNT, PRIMARY_INDEX, []interface{}{})
}

func (self *AccountDao) Insert(entity *AccountEntity) (uint64, error) {
	resp, err := client.Insert(SPACE_NAME_ACCOUNT, []interface{}{nil, entity.State, entity.OtpKey,
		entity.UsedKeyTime, entity.CreatedTime})
	if err != nil {
		return 0, err
	}

	return ToUint64(resp), nil
}

//single object
func (self *AccountDao) unpack(respData [][]interface{}) (*AccountEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

//list
func (self *AccountDao) unpackList(respData [][]interface{}) (list []*AccountEntity, err error) {
	if len(respData) == 0 {
		return nil, nil
	}
	var entity *AccountEntity = nil
	respArr := make([]*AccountEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 {
			entity = self.MapFromInterface(data)
			respArr[i] = entity

		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}

func (dao *AccountDao) MapFromInterface(data []interface{}) *AccountEntity {
	entity := &AccountEntity{}
	entity.Id = uint32(data[0].(uint64))
	entity.State = AccountState(data[1].(uint64))
	entity.OtpKey = data[2].(string)
	entity.UsedKeyTime = data[3].(uint64)
	entity.CreatedTime = data[4].(uint64)
	return entity
}
