package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"github.com/gobwas/ws/wsutil"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rgate/src/messages"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"strconv"
)

/**
ArtyomA
WS income messages router
*/
type WsRouter struct {
	log *flog.Logger
}

func NewWsRouter() *WsRouter {
	log := ctx.LogManager.NewLogger("ws-rout", flog.LEVEL_INFO)
	return &WsRouter{log}
}

func (service *WsRouter) RouteMsg(consumer *ConsumerConnection, msgBinary []byte) {
	//messages come @ format: msgIdNumber:{json}
	i, err := strconv.ParseInt(string(msgBinary[0:2]), 10, 8)
	if err != nil {
		wsApiService.P2p(consumer, ERR_INVALID_REQUEST)
	}
	if i == 0 {
		wsApiService.P2p(consumer, ERR_INVALID_REQUEST)
	}
	//remove msg id
	msgBinary = msgBinary[2:]
	switch uint8(i) {
	case messages.WS_MSG_TYPE_HELLO:
		service.hello(consumer, msgBinary)
	case messages.WS_MSG_TYPE_GET_INSTRUMENTS:
		service.getInstruments(consumer)
	case messages.WS_MSG_TYPE_GET_CURRENCIES:
		service.getCurrencies(consumer)
	case messages.WS_MSG_TYPE_GET_EQUITY:
		service.getEquity(consumer)
	case messages.WS_MSG_TYPE_GET_O_ORDERS:
		service.getOpenOrders(consumer)
	case messages.WS_MSG_TYPE_GET_O_POSITIONS:
		service.getOpenPositions(consumer)
	case messages.WS_MSG_TYPE_OPEN_ORDER:
		service.putOrder(consumer, msgBinary)
	case messages.WS_MSG_TYPE_CANCEL_ORDER:
	case messages.WS_MSG_TYPE_SUBSCRIBE:
		service.subscribe(consumer, msgBinary)
	case messages.WS_MSG_TYPE_UNSUBSCRIBE:
		service.unSubscribe(consumer, msgBinary)
	case messages.WS_MSG_TYPE_GET_OHLC:

	}
}

//all nessesery information
func (service *WsRouter) SendExchangeMainInfo(consumer *ConsumerConnection) {
	//send list of currencies
	service.getCurrencies(consumer)
	//send list of instruments
	service.getInstruments(consumer)
}

//WS_MSG_TYPE_HELLO
func (service *WsRouter) hello(consumer *ConsumerConnection, msgBinary []byte) {
	reqMsg := &messages.HelloMsgReq{}
	if err := util.FromJson(msgBinary, reqMsg); err != nil {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	if reqMsg.Otp == "" {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}

	account, err := accountDao.GetByOtp(reqMsg.Otp)
	if err != nil {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_METHOD_UNAVAILABLE))
		return
	}
	if account == nil {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_ACCOUNT_NOT_FOUND))
		return
	}

	service.log.InfoReqId(strconv.Itoa(int(account.Id)), "hello")
	//is account locked ?
	if account.State != dao.ACCOUNT_STATE_ACTIVE {
		service.log.ErrReqId(fmt.Sprint(account.Id), "err: ERR_OPERATION_NOT_PERMITTED")
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_OPERATION_NOT_PERMITTED))
		return
	}

	//register
	consumer.AccountId = account.Id
	wsApiService.Register(consumer)
	//create response
	response := messages.NewHelloMsgResp(account.Id, account.CreatedTime)
	wsApiService.P2p(consumer, response)
	//send open orders
	service.getOpenOrders(consumer)
	//send open positions
	service.getOpenPositions(consumer)
	//send equity
	service.getEquity(consumer)
}

//WS_MSG_TYPE_GET_INSTRUMENTS
func (service *WsRouter) getInstruments(consumer *ConsumerConnection) {
	cache := instrumentCache.GetCache()
	list := make([]*dao.InstrumentEntity, 0, len(cache))
	for _, val := range cache {
		list = append(list, val)
	}
	wsApiService.P2p(consumer, messages.NewGetInstrumentMsgResp(list))
}

//WS_MSG_TYPE_GET_CURRENCIES
func (service *WsRouter) getCurrencies(consumer *ConsumerConnection) {
	cache := currencyCache.GetCache()
	list := make([]*dao.CurrencyEntity, 0, len(cache))
	for _, val := range cache {
		list = append(list, val)
	}
	wsApiService.P2p(consumer, messages.NewGetCurrenciesMsgResp(list))
}

func (service *WsRouter) subscribe(consumer *ConsumerConnection, msgBinary []byte) {
	req := &messages.SubscribeMsgReq{}
	if err := util.FromJson(msgBinary, req); err != nil {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	if req.InstrumentId == 0 || req.InstrumentId > 1024 {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	wsApiService.subscribeToInstrument(req.InstrumentId, consumer)
	orderBook := instrOrderBookMap[req.InstrumentId]
	resp := messages.NewSubscribeMsgResp(orderBook.sellPriceArr, orderBook.buyPriceArr)
	wsApiService.P2p(consumer, resp)
	//service.log.InfoReqId(strconv.Itoa(int(consumer.AccountId)), "subs")
}

func (service *WsRouter) unSubscribe(consumer *ConsumerConnection, msgBinary []byte) {
	req := &messages.UnsubscribeMsgReq{}
	if err := util.FromJson(msgBinary, req); err != nil {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	if req.InstrumentId == 0 || req.InstrumentId > 1024 {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	wsApiService.unSubscribeInstrument(req.InstrumentId, consumer)
	service.log.InfoReqId(strconv.Itoa(int(consumer.AccountId)), "unSubs")
}

//GetAccountOpenOrder

func (service *WsRouter) getOpenOrders(consumer *ConsumerConnection) {
	orderArr, err := orderDao.GetAccountOpenOrder(consumer.AccountId)
	if err != nil {
		service.log.ErrReqId(strconv.Itoa(int(consumer.AccountId)), "err: "+err.Error())
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_METHOD_UNAVAILABLE))
		return
	}
	wsApiService.P2p(consumer, messages.NewGetOpenOrdersResp(orderArr))
}

func (service *WsRouter) getOpenPositions(consumer *ConsumerConnection) {
	positionArr, err := positionDao.GetAccountOpenPosition(consumer.AccountId)
	if err != nil {
		service.log.ErrReqId(strconv.Itoa(int(consumer.AccountId)), "err: "+err.Error())
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_METHOD_UNAVAILABLE))
		return
	}
	wsApiService.P2p(consumer, messages.NewGetOpenPositionsResp(positionArr))
}

func (service *WsRouter) getEquity(consumer *ConsumerConnection) {
	fundArr, err := fundDao.GetByAccountId(consumer.AccountId)
	if err != nil {
		service.log.ErrReqId(strconv.Itoa(int(consumer.AccountId)), "err: "+err.Error())
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_METHOD_UNAVAILABLE))
		return
	}
	wsApiService.P2p(consumer, messages.NewGetEquityMsgResp(fundArr))
}

func (service *WsRouter) putOrder(consumer *ConsumerConnection, msgBinary []byte) {
	req := &msg.OpenOrderReq{}
	if err := util.FromJson(msgBinary, req); err != nil {
		fmt.Println(err)
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	if req.InstrumentId == 0 || req.InstrumentId > 1024 {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_INVALID_REQUEST))
		return
	}
	if consumer.AccountId == 0 {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_OPERATION_NOT_PERMITTED))
		return
	}
	if consumer.AccountId != req.AccountId {
		wsutil.WriteServerText(consumer.Connection, util.ToJsonBin(ERR_OPERATION_NOT_PERMITTED))
		return
	}
	mainTopicApiPublisher.PublishMsgAsync(req.ToBinary())
}

//service.unSubscribe(consumer, msgBinary)
