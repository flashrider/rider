package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rgate/src/messages"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
)

/**
Order book changes listener
*/
type OrderBookListener struct {
	queue                  chan []byte
	orderBookEventListener *service.MsgListener
	log                    *flog.Logger
}

func NewOrderBookListener() *OrderBookListener {
	mLog.Info("start init tradeEventListener")
	inChan := make(chan []byte, 128)
	orderBookEventListener := service.NewMsgListener(msg.TOPIC_ENGINE_MSG_ORDER_BOOK_FEED, inChan,
		ctx.Env.RengineBroker.Address)
	mLog.InfoS("end init")
	log := ctx.LogManager.NewLogger("ob-feed", flog.LEVEL_INFO)
	return &OrderBookListener{inChan, orderBookEventListener, log}
}

func (service *OrderBookListener) ProcessEvents() {
	go func() {
		//reuse
		event := &msg.OrderBookChangeEvent{}
		for data := range service.queue {
			event.FromBinary(data)
			service.processEvent(event)
		}
	}()
}

//process order book change event
func (service *OrderBookListener) processEvent(event *msg.OrderBookChangeEvent) {
	service.log.InfoIdF(fmt.Sprint(event.InstrumentId), "%8d dir:%d lvl:%3d pr:%8d vol:%8d",
		event.MsgSeqNum, event.Direction, event.PriceLevel, event.Price, event.Volume)
	orderBookIntr := instrOrderBookMap[event.InstrumentId]
	switch event.EventType {
	case msg.ORDER_BOOK_PRICE_EVENT_NEW:
		if event.Direction == dao.ORDER_DIRECTION_SELL {
			orderBook := orderBookIntr.sellPriceArr
			priceLevel := []uint64{event.Price, event.Volume, uint64(event.OrderCount)}
			//len < priceIndex
			if uint16(len(orderBook)) < event.PriceLevel {
				orderBookIntr.sellPriceArr = append(orderBook, priceLevel)
			} else {
				//add to middle or array
				orderBookIntr.sellPriceArr = append(orderBook[:event.PriceLevel],
					append([][]uint64{priceLevel}, orderBook[event.PriceLevel:]...)...)
			}
		} else {
			orderBook := orderBookIntr.buyPriceArr
			priceLevel := []uint64{event.Price, event.Volume, uint64(event.OrderCount)}
			//len < priceIndex
			if uint16(len(orderBook)) < event.PriceLevel {
				orderBookIntr.buyPriceArr = append(orderBook, priceLevel)
			} else {
				//add to middle or array
				orderBookIntr.buyPriceArr = append(orderBook[:event.PriceLevel],
					append([][]uint64{priceLevel}, orderBook[event.PriceLevel:]...)...)
			}
		}
	case msg.ORDER_BOOK_PRICE_EVENT_CHANGE:
		if event.Direction == dao.ORDER_DIRECTION_SELL {
			level := orderBookIntr.sellPriceArr[event.PriceLevel]

			if level[0] != event.Price {
				service.log.ErrReqId(fmt.Sprint(event.InstrumentId), fmt.Sprintf("%d/%d msn:%d level:%d", level[0],
					event.Price, event.MsgSeqNum, event.PriceLevel))
			}
			level[1] = event.Volume
			level[2] = uint64(event.OrderCount)
		} else {
			level := orderBookIntr.buyPriceArr[event.PriceLevel]
			if level[0] != event.Price {
				service.log.ErrReqId(fmt.Sprint(event.InstrumentId), fmt.Sprintf("%d/%d msn:%d level:%d", level[0],
					event.Price, event.MsgSeqNum, event.PriceLevel))
			}
			level[1] = event.Volume
			level[2] = uint64(event.OrderCount)
		}

	case msg.ORDER_BOOK_PRICE_EVENT_DELETE:
		if event.Direction == dao.ORDER_DIRECTION_SELL {
			orderBookIntr.sellPriceArr = append(orderBookIntr.sellPriceArr[:event.PriceLevel],
				orderBookIntr.sellPriceArr[event.PriceLevel+1:]...)
		} else {
			orderBookIntr.buyPriceArr = append(orderBookIntr.buyPriceArr[:event.PriceLevel],
				orderBookIntr.buyPriceArr[event.PriceLevel+1:]...)
		}
		//TODO now we show full market depth
		//TODO for 1,10,20,50 market depth add NEW item from tail
	}
	//update message sequence number
	instrumentCache.Get(event.InstrumentId).OrderBookMsgSeqNum = event.MsgSeqNum
	//send message to all who subscribed to instrument(event.InstrumentId)
	wsApiService.BroadcastBinary(event.InstrumentId, service.ToJson(event))
}

//return as array
func (service *OrderBookListener) ToJson(req *msg.OrderBookChangeEvent) []byte {
	//TODO optimize
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d,%d,%d,%d]}", messages.WS_EVENT_TYPE_INSTR_O_BOOK,
		req.InstrumentId, req.Direction, req.MsgSeqNum, req.PriceLevel, req.EventType, req.Price, req.Volume,
		req.OrderCount))
}

func (service *OrderBookListener) Destroy() {

}
