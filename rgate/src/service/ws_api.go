package service

import (
	"github.com/artjoma/flog"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"gitlab.com/flashrider/rider/util"
	_ "io"
	"net"
	"strconv"
)

const (
	CONSUMER_WRITER_QUEUE_SIZE = 32
)

//ArtyomA
type ConsumerConnection struct {
	AccountId  uint32
	Connection net.Conn
	//every consumer has personal queue, if queue is full, we skip msg,
	//but others not wait user slow connection
	WriteQueue chan []byte
	//TODO OHLC time granulation
}

func (conCon *ConsumerConnection) runAsyncWriter() {
	go func() {
		for message := range conCon.WriteQueue {
			//if nil return see disconnect method
			if conCon.Connection == nil {
				return
			} else {
				wsutil.WriteServerText(conCon.Connection, message)
			}
		}
	}()
}

//web socket API
type WsApiService struct {
	log         *flog.Logger
	tcpListneer net.Listener
	//instrument subscription, index: instrumentId, map: key user queue, val object connection (anonymous and registered)
	consumerSubscArr []map[chan []byte]*ConsumerConnection
	//registered consumers, key accountId
	registConsumMap map[uint32]*ConsumerConnection
	active          bool
}

func NewWsApiService(instrumentCount uint16) *WsApiService {
	log := ctx.LogManager.NewLogger("connection", flog.LEVEL_INFO)
	consumerMap := make(map[uint32]*ConsumerConnection, 10000)
	consumerSubscArr := make([]map[chan []byte]*ConsumerConnection, instrumentCount+20)
	for i := 0; i < len(consumerSubscArr); i++ {
		consumerSubscArr[i] = make(map[chan []byte]*ConsumerConnection, 1000)
	}
	service := &WsApiService{log: log, consumerSubscArr: consumerSubscArr, registConsumMap: consumerMap}
	go service.InitSocket()
	return service
}

func (service *WsApiService) InitSocket() {
	service.log.Info("WS: " + ctx.App.Api.Ws.Interface)
	var err error
	service.tcpListneer, err = net.Listen("tcp", ctx.App.Api.Ws.Interface)
	if err != nil {
		panic(err)
	}
	service.active = true
	for service.active {
		conn, errAcc := service.tcpListneer.Accept()
		if errAcc != nil {
			// handle error
		}
		_, errAcc = ws.Upgrade(conn)
		if errAcc != nil {
			// handle error
		}

		go func() {
			defer conn.Close()
			//reuse variables
			var (
				msgReq   []byte              = nil
				op       ws.OpCode           = 0
				err      error               = nil
				consumer *ConsumerConnection = &ConsumerConnection{}
			)
			consumer.Connection = conn
			consumer.WriteQueue = make(chan []byte, CONSUMER_WRITER_QUEUE_SIZE)
			consumer.runAsyncWriter()
			wsRouterService.SendExchangeMainInfo(consumer)
			service.log.InfoReqId(strconv.Itoa(0), "connected")

			for {
				msgReq, op, err = wsutil.ReadClientData(conn)
				if err != nil {
					service.disconnect(consumer)
					return
				}
				if op == ws.OpClose {
					service.disconnect(consumer)
					return
				}
				wsRouterService.RouteMsg(consumer, msgReq)
			}
		}()
	}
}

//send message to all consumers
func (service *WsApiService) Broadcast(instrumentId uint16, msg interface{}) {
	msgBin := util.ToJsonBin(msg)
	for queue, _ := range service.consumerSubscArr[instrumentId] {
		select {
		case queue <- msgBin: // put ok
		default: // queue is full, sent nothing, skip
		}
	}
}

//broadcast to all who subscribed to this instrument
func (service *WsApiService) BroadcastBinary(instrumentId uint16, msgBin []byte) {
	for queue, _ := range service.consumerSubscArr[instrumentId] {
		select {
		case queue <- msgBin: // put ok
		default: // queue is full, sent nothing, skip
		}
	}
}

//send message to all users (registered or anonymous)
func (service *WsApiService) BroadcastAll(msg interface{}) {
	for _, instrSubs := range service.consumerSubscArr {
		for _, consumer := range instrSubs {
			select {
			case consumer.WriteQueue <- util.ToJsonBin(msg): // put ok
			default: // queue is full, sent nothing, skip
			}
		}
	}
}

//send message only to consumer
func (service *WsApiService) P2p(consumer *ConsumerConnection, msg interface{}) {
	select {
	case consumer.WriteQueue <- util.ToJsonBin(msg): // put ok
	default: // queue is full, sent nothing, skip
	}
}

func (service *WsApiService) P2pBinary(consumer *ConsumerConnection, msg []byte) {
	select {
	case consumer.WriteQueue <- msg: // put ok
	default: // queue is full, sent nothing, skip
	}
}

//may return nil if not found
func (service *WsApiService) GetConnection(accountId uint32) *ConsumerConnection {
	return service.registConsumMap[accountId]
}

//user subscribe to instrument
func (service *WsApiService) subscribeToInstrument(instrumentId uint16, consumer *ConsumerConnection) {
	service.consumerSubscArr[instrumentId][consumer.WriteQueue] = consumer
}

//user unsubscribe to instrument
func (service *WsApiService) unSubscribeInstrument(instrumentId uint16, consumer *ConsumerConnection) {
	delete(service.consumerSubscArr[instrumentId], consumer.WriteQueue)
}

//only after sign-in(OTP)
func (service *WsApiService) Register(consumer *ConsumerConnection) {
	service.registConsumMap[consumer.AccountId] = consumer
}

//consumer disconnect
func (service *WsApiService) disconnect(consumer *ConsumerConnection) {
	//remove from instrument subscription
	for _, userMap := range service.consumerSubscArr {
		delete(userMap, consumer.WriteQueue)
	}
	consumer.Connection = nil

	if consumer.AccountId != 0 {
		delete(service.registConsumMap, consumer.AccountId)
	}
	close(consumer.WriteQueue)
	service.log.InfoIdF(strconv.Itoa(int(consumer.AccountId)), "disconnect size: %d",
		len(service.registConsumMap))
}

func (service *WsApiService) Shutdown() {
	service.active = false
	service.tcpListneer.Close()
}
