package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rgate/src/messages"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
)

//Listen new feeds
type TradeFeedListener struct {
	queue              chan []byte
	tradeEventListener *service.MsgListener
	log                *flog.Logger
}

func NewTradeFeedListener() *TradeFeedListener {
	mLog.Info("start init TradeFeedListener")
	queue := make(chan []byte, 128)
	listener := service.NewMsgListener(msg.TOPIC_ENGINE_MSG_TRADES_FEED, queue,
		ctx.Env.RengineBroker.Address)
	mLog.InfoS("end init")
	log := ctx.LogManager.NewLogger("trade-feed", flog.LEVEL_INFO)
	return &TradeFeedListener{queue, listener, log}
}

func (service *TradeFeedListener) ProcessEvents() {
	go func() {
		instrumentId := uint16(0)
		var trade *dao.TradeEntity
		var err error

		for data := range service.queue {
			instrumentId, trade, err = msg.FromBinaryTradeEntityResp(data)
			if err != nil {
				service.log.Err("err msg:" + err.Error())
				continue
			}
			//set last trade
			instrOrderBookMap[instrumentId].lastTrade = trade
			//create msg
			wsApiService.BroadcastBinary(instrumentId, service.TradeToJson(trade, instrumentId))
		}
	}()
}

func (service *TradeFeedListener) Destroy() {
	service.tradeEventListener.Shutdown()
}

//TODO optimize to buffer
func (service *TradeFeedListener) TradeToJson(trade *dao.TradeEntity, instrumentId uint16) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d,%d,%d]}",
		messages.WS_EVENT_TYPE_INSTR_TRADE, instrumentId, trade.Direction, trade.BidOrderId, trade.AskOrderId,
		trade.Price, trade.Quantity, trade.SettleDate))
}
