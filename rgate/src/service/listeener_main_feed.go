package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rgate/src/messages"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
)

//User operations responses: Put order, cancel order, update order from matching ...
type MainFeedListener struct {
	queue                  chan []byte
	orderBookEventListener *service.MsgListener
	log                    *flog.Logger
}

func NewMainFeedListener() *MainFeedListener {
	mLog.Info("start init MainFeedListener")
	queue := make(chan []byte, 128)
	listener := service.NewMsgListener(msg.TOPIC_ENGINE_MAIN_MSG_RESP, queue,
		ctx.Env.RengineBroker.Address)
	mLog.InfoS("end init")
	log := ctx.LogManager.NewLogger("m-resp", flog.LEVEL_INFO)
	return &MainFeedListener{queue, listener, log}
}

func (service *MainFeedListener) ProcessEvents() {
	go func() {
		for data := range service.queue {
			service.processEvent(data)
		}
	}()
}

func (service *MainFeedListener) processEvent(event []byte) {
	//first byte - command
	switch msg.EngineMessageType(event[0]) {
	case msg.ENGINE_MSG_TYPE_OPEN_ORDER:
		instrumentId, order, err := msg.FromBinaryOpenOrderResp(event)
		if err != nil {
			service.log.Err("ENGINE_MSG_TYPE_OPEN_ORDER err: " + err.Error())
			return
		}
		if connection := wsApiService.GetConnection(order.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.CreateOrderRespToJson(order, instrumentId))
		}
	case msg.ENGINE_MSG_TYPE_CANCEL_ORDER:
	case msg.ENGINE_MSG_TYPE_UPDATE_ORDER:
		instrumentId, orderChanges, err := msg.FromBinaryOrderChangesEntity(event)
		if err != nil {
			service.log.Err("ENGINE_MSG_TYPE_UPDATE_ORDER err: " + err.Error())
			return
		}
		if connection := wsApiService.GetConnection(orderChanges.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.UpdateOrderRespToJson(orderChanges, instrumentId))
		}
	case msg.ENGINE_MSG_TYPE_ERR_RESP:
		appErr := &AppErr{}
		appErr.FromBinary(event)
		if connection := wsApiService.GetConnection(appErr.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.ErrRespToJson(appErr))
		}

	case msg.ENGINE_MSG_TYPE_UPDATE_FUND:
		fundResp, err := msg.FromBinaryFundCacheModelResp(event)
		if err != nil {
			service.log.Err("ENGINE_MSG_TYPE_UPDATE_FUND err: " + err.Error())
			return
		}
		if connection := wsApiService.GetConnection(fundResp.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.FundCacheModelRespToJson(fundResp))
		}
	case msg.ENGINE_MSG_TYPE_OPEN_POSITION:
		instrumentId, position, err := msg.FromBinaryPositionEntity(event)
		if err != nil {
			service.log.Err("ENGINE_MSG_TYPE_OPEN_POSITION err: " + err.Error())
			return
		}
		if connection := wsApiService.GetConnection(position.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.OpenPositionToJson(position, instrumentId))
		}
	case msg.ENGINE_MSG_TYPE_UPDATE_POSITION:
		instrumentId, position, err := msg.FromBinaryPositionEntity(event)
		if err != nil {
			service.log.Err("ENGINE_MSG_TYPE_UPDATE_POSITION err: " + err.Error())
			return
		}
		if connection := wsApiService.GetConnection(position.AccountId); connection != nil {
			wsApiService.P2pBinary(connection, service.UpdatePositionToJson(position, instrumentId))
		}
	}

}

func (service *MainFeedListener) Destroy() {
	service.orderBookEventListener.Shutdown()
}

//TODO optimize to buffer
func (service *MainFeedListener) CreateOrderRespToJson(order *dao.OrderEntity, instrumentId uint16) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d]}",
		messages.WS_MSG_TYPE_OPEN_ORDER, order.Id, order.PositionId, order.AccountId, order.Kind,
		order.Type, order.Direction, order.AmountRest, order.Price, order.PriceTrigger,
		order.Amount, order.State, order.EntryOrderId, order.ExpTime, order.CreatedTime,
		order.UpdateTime, order.Duration, order.CorrelationId, instrumentId))
}

func (service *MainFeedListener) FundCacheModelRespToJson(resp *dao.FundCacheModel) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d]}",
		messages.WS_MSG_TYPE_UPDATE_FUND, resp.Id, resp.Balance, resp.Reserved, resp.State,
		resp.TotalVolumeAmount))
}

func (service *MainFeedListener) UpdateOrderRespToJson(order *dao.OrderChangesEntity, instrumentId uint16) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d]}",
		messages.WS_MSG_TYPE_UPDATE_ORDER, instrumentId, order.Id, order.AmountRest, order.State))
}

func (service *MainFeedListener) ErrRespToJson(err *AppErr) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d]}", messages.WS_MSG_TYPE_ERR, err.Id, err.CorrelationId))
}

func (service *MainFeedListener) OpenPositionToJson(pos *dao.PositionEntity, instrumentId uint16) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d,%d,%d,%d]}",
		messages.WS_MSG_TYPE_OPEN_POSITION, instrumentId, pos.Id, pos.Direction, pos.OpenTime, pos.Amount,
		pos.OpenPrice, pos.State, pos.PL))
}

func (service *MainFeedListener) UpdatePositionToJson(pos *dao.PositionEntity, instrumentId uint16) []byte {
	return []byte(fmt.Sprintf("{\"mT\":%d,\"ev\":[%d,%d,%d,%d,%d,%d,%d]}",
		messages.WS_MSG_TYPE_UPDATE_POSITION, instrumentId, pos.Id, pos.CloseOrderId,
		pos.SettleDate, pos.ClosePrice, pos.State, pos.PL))
}
