package service

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

var (
	/*
		error mapping to rEngine errors
	*/
	//not available DB, some error, panic
	ERR_METHOD_UNAVAILABLE = appErr(1)
	//invalid request, no mandatory fields
	ERR_INVALID_REQUEST = appErr(2)
	//not found data
	ERR_ACCOUNT_NOT_FOUND = appErr(3)
	//Operation not permitted
	ERR_OPERATION_NOT_PERMITTED = appErr(6)
	//instrument found but session not active, see state
	ERR_INSTRUMENT_ACTIVE_SESSION_NOT_FOUND = appErr(8)
	ERR_INSTRUMENT_NOT_FOUND                = appErr(9)
)

func GetErrById(errId uint8) *AppErr {
	switch errId {
	case ERR_METHOD_UNAVAILABLE.Id:
		return ERR_METHOD_UNAVAILABLE
	case ERR_INVALID_REQUEST.Id:
		return ERR_INVALID_REQUEST
	case ERR_ACCOUNT_NOT_FOUND.Id:
		return ERR_ACCOUNT_NOT_FOUND
	case ERR_OPERATION_NOT_PERMITTED.Id:
		return ERR_OPERATION_NOT_PERMITTED
	default:
		return appErr(errId)
	}
}

func appErr(Id uint8) *AppErr {
	return &AppErr{Id, 0, 0}
}

type AppErr struct {
	Id            uint8  `json:"id"`
	AccountId     uint32 `json:"aId,omitempty"`
	CorrelationId uint64 `json:"cId,omitempty"`
}

func (objErr *AppErr) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid AppErr message format. Err: %v", e)
		}
	}()
	buffer := bytes.NewBuffer(data[1:])
	binary.Read(buffer, binary.BigEndian, &objErr.Id)
	binary.Read(buffer, binary.BigEndian, &objErr.AccountId)
	binary.Read(buffer, binary.BigEndian, &objErr.CorrelationId)
	return nil
}

func (err *AppErr) GetId() uint8 {
	return err.Id
}
