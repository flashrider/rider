package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util/msg"
)

/**
Application context and config
*/
type AppContext struct {
	Env struct {
		Name  string `yaml:"name"`
		Nosql struct {
			Server  string `yaml:"server"`
			User    string `yaml:"user"`
			Pass    string `yaml:"pass"`
			Buffers uint32 `yaml:"buffers"`
		}
		RengineBroker struct {
			Address string `yaml:"address"`
		}
	}
	App struct {
		Version string `yaml:"version"` //app version
		Name    string `yaml:"name"`

		Api struct {
			Ws struct {
				Interface string `yaml:"interface"`
			}
		}
		Logging struct {
			Console     bool   `yaml:"console"`
			Threshold   rune   `yaml:threshold`
			MaxFileSize uint64 `yaml:maxfilesize`
		}
	}

	LogManager *flog.LogManager
	Logger     *flog.Logger //main logger

	AppFolder string
	StartTime uint32 //when app started
	Active    bool   //is app active
}

type InstrumentOrderBook struct {
	sellPriceArr [][]uint64       //0-price, 1-volume, 2-orders count
	buyPriceArr  [][]uint64       //0-price, 1-volume, 2-orders count
	lastTrade    *dao.TradeEntity //last trade
}

func NewOrderBook() *InstrumentOrderBook {
	sellPriceArr := make([][]uint64, 1000)
	for i, _ := range sellPriceArr {
		sellPriceArr[i] = make([]uint64, 3)
	}
	buyPriceArr := make([][]uint64, 1000)
	for i, _ := range buyPriceArr {
		buyPriceArr[i] = make([]uint64, 3)
	}
	return &InstrumentOrderBook{sellPriceArr, buyPriceArr, nil}
}

func (book *InstrumentOrderBook) FromSnapshot(resp *msg.InstrOrderBookSnapshotResp) {
	book.sellPriceArr = resp.OrderSellArr
	book.buyPriceArr = resp.OrderBuyArr
}
