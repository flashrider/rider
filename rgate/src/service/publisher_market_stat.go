package service

import (
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rgate/src/messages"
	"time"
)

/*
	Pereodic market data sender
	Every user should get statistics(periodically update) about each instrument (best ASK, best BID)
*/
type MarketStatPublisher struct {
}

func NewMarketStatPublisher() *MarketStatPublisher {
	publisher := &MarketStatPublisher{}
	go publisher.publish()
	return publisher
}

func (service *MarketStatPublisher) publish() {
	for ctx.Active {
		var marketStatEvent *messages.MarketStatEvent = nil
		var instrEntity *dao.InstrumentEntity = nil

		for instrId, book := range instrOrderBookMap {
			marketStatEvent = messages.NewMarketStatEvent()
			marketStatEvent.InstrumentId = instrId
			if len(book.sellPriceArr) > 0 {
				marketStatEvent.Ask = book.sellPriceArr[0] //best seller
			} else {
				marketStatEvent.Ask = nil
			}
			if len(instrOrderBookMap[instrId].buyPriceArr) > 0 {
				marketStatEvent.Bid = instrOrderBookMap[instrId].buyPriceArr[0] //best buyer
			} else {
				marketStatEvent.Bid = nil
			}
			marketStatEvent.LastTrade = book.lastTrade
			instrEntity = instrumentCache.Get(instrId)
			marketStatEvent.SessionState = instrEntity.SessionState
			marketStatEvent.MessageSeqNumber = instrEntity.OrderBookMsgSeqNum
			wsApiService.BroadcastAll(marketStatEvent)
		}

		time.Sleep(time.Second * 2)
	}
}
