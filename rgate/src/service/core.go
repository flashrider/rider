package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
	"time"
)

var (
	ctx               *AppContext
	mLog              *flog.Logger //main log
	wsApiService      *WsApiService
	wsRouterService   *WsRouter
	accountDao        *dao.AccountDao
	orderDao          *dao.OrderDao
	positionDao       *dao.PositionDao
	currencyDao       *dao.CurrencyDao
	fundDao           *dao.FundDao
	instrumentDao     *dao.InstrumentDao
	tradeDao          *dao.TradeDao
	extTxDao          *dao.ExtTxDao
	sessionHistoryDao *dao.SessionHistoryDao
	ohlcAskDao        *dao.OhlcDao
	ohlcBidDao        *dao.OhlcDao

	instrOrderBookMap map[uint16]*InstrumentOrderBook

	currencyCache   *service.CurrencyCache
	instrumentCache *service.InstrumentCache
	/*
		Listeners
	*/
	orderBookListener *OrderBookListener
	mainFeedListener  *MainFeedListener
	tradeListener     *TradeFeedListener
	/*
		Publishers
	*/
	mainTopicApiPublisher *service.MsgPublisher
	/*
		Periodically publishers
	*/
	marketStatPublisher *MarketStatPublisher
)

func InitServices(_ctx *AppContext) {
	ctx = _ctx
	mLog = ctx.Logger
	accountDao = dao.NewAccountDao()
	orderDao = dao.NewOpenOrderDao()
	positionDao = dao.NewPositionDao()
	currencyDao = dao.NewCurrencyDao()
	fundDao = dao.NewFundDao()
	instrumentDao = dao.NewInstrumentDao()
	tradeDao = dao.NewTradeDao()
	extTxDao = dao.NewExtTxDao()
	sessionHistoryDao = dao.NewSessionHistoryDao()
	ohlcAskDao = dao.NewOhlcAskDao()
	ohlcBidDao = dao.NewOhlcBidDao()
	mLog.Info("broker api: " + ctx.Env.RengineBroker.Address)
	ctx.Active = true
	/**
	Init caches services
	*/
	mLog.Info("start init currencyCache")
	currencyCache = service.NewCurrencyCache(ctx.LogManager, currencyDao)
	mLog.InfoS("end init")

	mLog.Info("start init instrumentCache")
	instrumentCache = service.NewInstrumentCache(ctx.LogManager, instrumentDao)
	mLog.InfoS("end init")

	mLog.Info("start init mainTopicApiPublisher")
	mainTopicApiPublisher = service.NewMsgPublisher(msg.TOPIC_ENGINE_MAIN_MSG_REQ, ctx.Env.RengineBroker.Address)
	mLog.InfoS("end init")

	mLog.Info("start init wsAPI")
	wsApiService = NewWsApiService(instrumentCache.GetCacheSize())
	wsRouterService = NewWsRouter()
	GetInstrumentOrderBookSnapshot()

	mLog.Info("start init orderBookListener")
	orderBookListener = NewOrderBookListener()
	orderBookListener.ProcessEvents()
	mLog.InfoS("end init")

	mLog.Info("start init mainFeedListener")
	mainFeedListener = NewMainFeedListener()
	mainFeedListener.ProcessEvents()
	mLog.InfoS("end init")

	mLog.Info("start init tradeListener")
	tradeListener = NewTradeFeedListener()
	tradeListener.ProcessEvents()
	mLog.InfoS("end init")

	mLog.Info("start init marketStatPublisher")
	marketStatPublisher = NewMarketStatPublisher()
	mLog.InfoS("end init")

}

//get orderBookSnapshot
func GetInstrumentOrderBookSnapshot() {
	brokerAddr := ctx.Env.RengineBroker.Address
	queue := make(chan []byte, 512)
	listener := service.NewMsgListener(msg.TOPIC_ENGINE_MSG_GET_SNAPSHOT_RESP, queue, brokerAddr)
	instrOrderBookMap = make(map[uint16]*InstrumentOrderBook, instrumentCache.GetCacheSize())

	for k, _ := range instrumentCache.GetCache() {
		strId := fmt.Sprint(k)
		mLog.InfoReqId(strId, "start send req to order book snapshot")
		instrOrderBookMap[k] = NewOrderBook()
		req := &msg.InstrOrderBookSnapshotReq{InstrumentId: k}
		mainTopicApiPublisher.PublishMsgAsync(req.ToBinary())
		mLog.InfoReqId(strId, "end send")
	}
	time.Sleep(time.Second)

	mLog.InfoS("snapshots queue len: ", len(queue))
	run := true
	for run {
		select {
		case msgByte := <-queue:
			resp := &msg.InstrOrderBookSnapshotResp{}
			util.FromBinary(msgByte, resp)
			strId := fmt.Sprint(resp.InstrumentId)
			mLog.InfoIdF(strId, "start apply snapshot. Msq:%d sellPriceLev:%d buyPriceLev:%d ",
				resp.MsgSequenceNumber, len(resp.OrderBuyArr), len(resp.OrderSellArr))
			instrOrderBookMap[resp.InstrumentId].FromSnapshot(resp)
			if len(resp.OrderSellArr) > 0 {
				mLog.InfoIdF(strId, "Best sell price lvl: %s", fmt.Sprint(resp.OrderSellArr[0]))
			}
			mLog.InfoIdF(strId, "Sell count lvl: %d", len(resp.OrderSellArr))

			if len(resp.OrderBuyArr) > 0 {
				mLog.InfoIdF(strId, "Best buy price lvl: %s", fmt.Sprint(resp.OrderBuyArr[0]))
			}
			mLog.InfoIdF(strId, "Buy count lvl: %d", len(resp.OrderBuyArr))

			instrumentCache.Get(resp.InstrumentId).OrderBookMsgSeqNum = resp.MsgSequenceNumber
			mLog.InfoReqId(strId, "end apply snapshot")
		default:
			mLog.Info("snapshots queue is empty")
			run = false
		}
	}
	//no need, free resources
	listener.Shutdown()
}

func DestroyServices() {
	ctx.Active = false
	//shutdown listeners and other resources
	mainTopicApiPublisher.Shutdown()
}
