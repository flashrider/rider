package messages

import (
	"gitlab.com/flashrider/rider/dao"
)

const (
	WS_MSG_TYPE_ERR             uint8 = 0
	WS_MSG_TYPE_HELLO           uint8 = 1
	WS_MSG_TYPE_GET_INSTRUMENTS uint8 = 2 //all instruments
	WS_MSG_TYPE_GET_CURRENCIES  uint8 = 3
	WS_MSG_TYPE_GET_EQUITY      uint8 = 4
	WS_MSG_TYPE_GET_O_ORDERS    uint8 = 5
	WS_MSG_TYPE_GET_O_POSITIONS uint8 = 6
	WS_MSG_TYPE_OPEN_ORDER      uint8 = 7
	WS_MSG_TYPE_CANCEL_ORDER    uint8 = 8
	WS_MSG_TYPE_SUBSCRIBE       uint8 = 9 //
	WS_MSG_TYPE_UNSUBSCRIBE     uint8 = 10
	WS_MSG_TYPE_GET_OHLC        uint8 = 11 //account subscribed to OHLC(ASK 1h), only req. for resp. see: WS_EVENT_TYPE_INSTR_OHLC
	WS_MSG_TYPE_UPDATE_ORDER    uint8 = 12 //order matched, we should send to account changed order
	WS_MSG_TYPE_UPDATE_FUND     uint8 = 13 //fund changed
	WS_MSG_TYPE_OPEN_POSITION   uint8 = 14 //open position
	WS_MSG_TYPE_UPDATE_POSITION uint8 = 15 //update(including close) position

	//EVENTS
	WS_EVENT_TYPE_INSTR_OHLC   uint8 = 70 //account subscribed OHLC
	WS_EVENT_TYPE_INSTR_TRADE  uint8 = 71 //new trade
	WS_EVENT_TYPE_INSTR_O_BOOK uint8 = 72 //order book changes
	WS_EVENT_TYPE_MARKET_STAT  uint8 = 73 //instruments changes (OHLC 2sec), session active or halted, session open time/close time
)

/*
	Public messages for WebSocket
*/
type HelloMsgReq struct {
	Otp string `json:"otp"`
}

type HelloMsgResp struct {
	MessageTypeId uint8  `json:"mT"`
	AccountId     uint32 `json:"accId"`
	Created       uint64 `json:"accCt"`
}

func NewHelloMsgResp(accountId uint32, created uint64) *HelloMsgResp {
	return &HelloMsgResp{WS_MSG_TYPE_HELLO, accountId, created}
}

type GetInstrumentsMsgResp struct {
	MessageTypeId uint8                   `json:"mT"`
	InstrumentArr []*dao.InstrumentEntity `json:"iList"`
}

func NewGetInstrumentMsgResp(iList []*dao.InstrumentEntity) *GetInstrumentsMsgResp {
	return &GetInstrumentsMsgResp{WS_MSG_TYPE_GET_INSTRUMENTS, iList}
}

type GetCurrenciesMsgResp struct {
	MessageTypeId uint8                 `json:"mT"`
	CurrencyArr   []*dao.CurrencyEntity `json:"cList"`
}

func NewGetCurrenciesMsgResp(cList []*dao.CurrencyEntity) *GetCurrenciesMsgResp {
	return &GetCurrenciesMsgResp{WS_MSG_TYPE_GET_CURRENCIES, cList}
}

//get list of account funds
type GetEquityMsgResp struct {
	MessageTypeId uint8           `json:"mT"`
	FundArr       [][]interface{} `json:"fund"`
}

func NewGetEquityMsgResp(fundArr [][]interface{}) *GetEquityMsgResp {
	return &GetEquityMsgResp{WS_MSG_TYPE_GET_EQUITY, fundArr}
}

//type GetOpenOrdersReq struct {
//}

type GetOpenOrdersResp struct {
	MessageTypeId uint8           `json:"mT"`
	OrderArr      [][]interface{} `json:"ord"`
}

func NewGetOpenOrdersResp(orderArr [][]interface{}) *GetOpenOrdersResp {
	return &GetOpenOrdersResp{WS_MSG_TYPE_GET_O_ORDERS, orderArr}
}

type GetOpenPositionsReq struct {
}

type GetOpenPositionsResp struct {
	MessageTypeId uint8           `json:"mT"`
	PositionArr   [][]interface{} `json:"pos"`
}

func NewGetOpenPositionsResp(positionArr [][]interface{}) *GetOpenPositionsResp {
	return &GetOpenPositionsResp{WS_MSG_TYPE_GET_O_POSITIONS, positionArr}
}

type SubscribeMsgReq struct {
	InstrumentId uint16 `json:"iId"`
	//OHLC resolution if filled send OHLC data (see WS_MSG_TYPE_OHLC_SUBSCRIBE)
}

type SubscribeMsgResp struct {
	MessageTypeId uint8      `json:"mT"`
	SellOrders    [][]uint64 `json:"sellArr"`
	BuyOrders     [][]uint64 `json:"buyArr"`
}

func NewSubscribeMsgResp(sellOrderArr, buyOrderArr [][]uint64) *SubscribeMsgResp {
	return &SubscribeMsgResp{WS_MSG_TYPE_SUBSCRIBE, sellOrderArr, buyOrderArr}
}

type UnsubscribeMsgReq struct {
	InstrumentId uint16 `json:"iId"`
}

type GetOHLCMsgReq struct {
	//OHLC type
}

type ChangeOHLCResolutMsgResp struct {
	//OHLC data by time range
}

//subscribed instrument OHLC changes
type InstrumentOHLCEvent struct {
}

//(all instruments) last trade
type MarketStatEvent struct {
	MessageTypeId    uint8                      `json:"mT"`
	InstrumentId     uint16                     `json:"iId"`
	SessionState     dao.InstrumentSessionState `json:"st"`
	LastTrade        *dao.TradeEntity           `json:"tr"`
	Ask              []uint64                   `json:"ask"`
	Bid              []uint64                   `json:"bid"`
	MessageSeqNumber uint64                     `json:"msq"`
}

func NewMarketStatEvent() *MarketStatEvent {
	return &MarketStatEvent{MessageTypeId: WS_EVENT_TYPE_MARKET_STAT}
}
