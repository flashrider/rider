# Rider - crypto/fiat order matching engine

Orders:
* Limit
* Market + Stop

Benefits:
* Low latency
* Ultra fast
* Lang: Golang >=1.11 
* DB: Tarantool

TODO:
* Close positions after matching
* Overal tests
