package binary

import (
	"testing"
	"time"
)

func TestApi(t *testing.T) {
	api := NewEngineBinaryApi("127.0.0.1:4161", "info")
	time.Sleep(3 * time.Minute)
	api.Shutdown()
}
