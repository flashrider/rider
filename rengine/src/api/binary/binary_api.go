package binary

import (
	"github.com/nsqio/nsq/nsqd"
)

//NSQ daemon (nsqd)
type EngineBinaryApi struct {
	nsqd *nsqd.NSQD
}

func NewEngineBinaryApi(brokerAddr, nsqBrokerLogLevel string) *EngineBinaryApi {
	opts := nsqd.NewOptions()
	opts.MaxMsgSize = 1024 * 1024 * 128
	opts.MaxBodySize = 1024 * 1024 * 128 * 5
	opts.LogLevel = nsqBrokerLogLevel
	opts.TCPAddress = brokerAddr
	nsqd := nsqd.New(opts)
	go nsqd.Main()

	return &EngineBinaryApi{nsqd}
}

func (api *EngineBinaryApi) Shutdown() {
	api.nsqd.Exit()
}
