package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"strconv"
)

type DbWriterService struct {
	queue chan *MemSnapshot
	log   *flog.Logger
}

func NewDbWriterService() *DbWriterService {
	queue := make(chan *MemSnapshot, 256)
	log := ctx.LogManager.NewLogger("db-writer", flog.LEVEL_INFO)
	writer := &DbWriterService{queue, log}
	go writer.writeToDb()
	return writer
}

func (writer *DbWriterService) ProcessSnapshot(snapshot *MemSnapshot) {
	writer.queue <- snapshot
}

func (writer *DbWriterService) writeToDb() {
	snapshotId := ""
	ok := false
	var err error = nil
	for snapshot := range writer.queue {
		snapshotId = strconv.FormatInt(snapshot.Id, 10)
		writer.log.InfoReqId(snapshotId, snapshot.Symbol)
		//result := true get from stored procedure result
		ok, err = dao.WriteSnapshot(snapshot)
		writer.log.InfoIdF(snapshotId, "done: %t", ok)
		if err != nil {
			writer.log.ErrReqId(snapshotId, "err: "+err.Error())
			exchangeHalted = true
		}
		if !ok {
			exchangeHalted = true
		}
	}
}
