package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"strconv"
)

/**
router service is messages routing using channel
*/
type RouterService struct {
	queue chan []byte //income message queue
	rLog  *flog.Logger
}

func NewRouterService(logManager *flog.LogManager) *RouterService {
	rLog := logManager.NewLogger("rejected", flog.LEVEL_INFO)
	service := &RouterService{make(chan []byte, 8), rLog}
	//process messages in diff thread
	go service.processMsg()
	return service
}

/**
Put message
*/
func (router *RouterService) AddMessage(msg []byte) {
	if len(msg) < 7 || len(msg) > 256 {
		router.rLog.Err("rejected by length")
		return
	}
	//put to queue
	router.queue <- msg
}

//process message, execution happens in single thread
func (router *RouterService) processMsg() {
	//we can reuse, to prevent make a garbage
	openOrderReg := &msg.OpenOrderReq{}
	cancelOrderReq := &msg.CancelOrderReq{}
	for message := range router.queue {
		startTime := util.NowMk()
		maLog.InfoF("msgType: %d", message[0])
		switch msg.EngineMessageType(message[0]) {
		case msg.ENGINE_MSG_TYPE_OPEN_ORDER:
			err := openOrderReg.FromBinary(message)
			//invalid msg
			if err == nil {
				orderService.OpenOrder(openOrderReg)
			} else {
				router.rLog.Err(err.Error())
			}
		case msg.ENGINE_MSG_TYPE_CANCEL_ORDER:
			err := cancelOrderReq.FromBinary(message)
			//invalid msg
			if err == nil {
				cancelOrderService.CancelOrder(cancelOrderReq)
			} else {
				router.rLog.Err(err.Error())
			}
		case msg.ENGINE_MSG_ORDER_EXPIRED:

		case msg.ENGINE_MSG_TYPE_GET_ORDER_BOOK_SNAPSHOT:
			req := &msg.InstrOrderBookSnapshotReq{}
			req.FromBinary(message)
			resp := instrumentOrderMap[req.InstrumentId].CreateSnapshot()
			instrOBSnapPubl.PublishMsgAsync(util.ToBinary(resp))
		default:
			router.rLog.Err("msgId not found!")
		}
		maLog.InfoReqId("", "elt: "+strconv.Itoa(int(util.NowMk()-startTime)))
	}
}

/** validation move to API
func (router *RouterService) validationInstrument(instrumentId uint16) AppErr {
	instrument := instrumentCache.Get(instrumentId)
	if instrument == nil{
		return ERR_INSTRUMENT_NOT_FOUND
	}
	if instrumentCache.Get(instrumentId).SessionState == dao.INSTRUMENT_SESSION_OPEN_STATE {
		return nil
	} else {
		return ERR_INSTRUMENT_ACTIVE_SESSION_NOT_FOUND
	}
	return true
}
*/
