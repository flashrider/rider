package service

import (
	"fmt"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util/msg"
)

/**
Open orders cache + (Depth of Market – DOM).
Slice tricks: https://github.com/golang/go/wiki/SliceTricks
Artyom Aminov
*/
const (
	ORDER_COUNT_PER_PRICE_ALLOC = 2000
)

type InstrumentOrderCache struct {
	InstrumentId uint16 //instrument id
	Symbol       string //instrument symbol
	BCurrId      uint16 //base  currency
	QCurrId      uint16 //quote currency
	/*
		Limit orders
	*/
	LimitSellPriceArr []*PriceLvlContainer //price sorted 10,12,18...
	LimitBuyPriceArr  []*PriceLvlContainer //price sorted 8,5,2...

	StopSellAskMap map[uint64][]*OrderCacheModel //sell ask. key: price. value:slice of orders.
	StopSellBidMap map[uint64][]*OrderCacheModel //sell bid then. key: price. value:slice of orders.
	StopBuyAskMap  map[uint64][]*OrderCacheModel //buy ask then. key: price. value:slice of orders.
	StopBuyBidMap  map[uint64][]*OrderCacheModel //buy bid then. key: price. value:slice of orders.
	/*
		Total volume in limit orders
	*/
	sellVolTotal uint64 //see vol. vol always by base curr
	buyVolTotal  uint64 //buy vol. vol always by base curr

	//OrderMap     map[uint64]*OrderCacheModel //all active orders(not filled, ....). key : order id
	orderIdSeq      uint64                         //auto increment
	msgSeqNumber    uint64                         //order book change event index (incremental)
	LastTrade       *dao.TradeEntity               //last trade
	positionMap     map[uint64]*PositionCacheModel // key: position id, [0] - TP, [1] - SL
	orderExpTimeMap map[uint64]*OrderCacheModel    //key: orderId, value: order
}

func NewInstrumentOrderCache(instrumentId uint16, instrumentName string, baseCurId,
	quoteCurrId uint16, loadFromDb bool) *InstrumentOrderCache {
	sellPrice := make([]*PriceLvlContainer, 0, 1000)
	buyPrice := make([]*PriceLvlContainer, 0, 1000)
	//orderMap := make(map[uint64]*OrderCacheModel, 20000)
	stopSellLMap := make(map[uint64][]*OrderCacheModel, 1000)
	stopSellGMap := make(map[uint64][]*OrderCacheModel, 1000)
	stopBuyLMap := make(map[uint64][]*OrderCacheModel, 1000)
	stopBuyGMap := make(map[uint64][]*OrderCacheModel, 1000)
	positionMap := make(map[uint64]*PositionCacheModel, 50000)

	orderExpTimeMap := make(map[uint64]*OrderCacheModel, 10000)

	instrCache := &InstrumentOrderCache{instrumentId, instrumentName, baseCurId, quoteCurrId,
		sellPrice, buyPrice, stopSellLMap, stopSellGMap, stopBuyLMap, stopBuyGMap, 0, 0, 0, 0,
		nil, positionMap, orderExpTimeMap}
	if loadFromDb {
		mLog.InfoReqId(instrumentName, "start load from DB open orders with state <= ORDER_STATE_FILLED")
		//list of orders
		list, err := orderDao.GetOrdersLessState(instrumentName, dao.ORDER_STATE_PARTIALLY_FILLED)
		if err != nil {
			mLog.ErrReqId(instrumentName, "orderDao.GetOrdersLessState: "+err.Error())
			panic("err load " + err.Error())
		}
		instrCache.LoadFromEntityList(instrumentName, list)

		lastId, err := orderDao.GetCount(instrumentName)
		if err != nil {
			mLog.ErrReqId(instrumentName, "orderDao.GetCount(instrumentName): "+err.Error())
			panic("err load " + err.Error())
		}
		//extract seq number
		instrCache.orderIdSeq, _, _ = instrCache.ExtractSeqId(lastId)
		mLog.InfoIdF(instrumentName, "order last id: %11d reqId: %11d", lastId, instrCache.orderIdSeq)
		mLog.InfoReqId(instrumentName, "end load")
	}
	return instrCache
}

func (cache *InstrumentOrderCache) LoadFromEntityList(instrumentName string, list []*dao.OrderEntity) {
	mLog.InfoIdF(instrumentName, "size: %d", len(list))
	var order *OrderCacheModel = nil

	orderMap := make(map[uint64]*OrderCacheModel, 50000)
	for _, entity := range list {
		order = &OrderCacheModel{}
		order.Id = entity.Id
		order.Price = entity.Price
		order.Amount = entity.Amount
		order.Duration = entity.Duration
		order.AccountId = entity.AccountId
		order.Type = entity.Type
		order.AmountRest = entity.AmountRest
		order.EntryOrderId = entity.EntryOrderId
		order.Kind = entity.Kind
		order.PriceTrigger = entity.PriceTrigger
		order.Direction = entity.Direction
		order.State = entity.State
		order.ExpTime = entity.ExpTime

		if entity.ExpTime > 0 {
			cache.PutExpTimeOrder(order)
		}

		if order.Type == dao.ORDER_TYPE_LIMIT {
			orderMap[order.Id] = order
			if order.State == dao.ORDER_STATE_PLACED || order.State == dao.ORDER_STATE_PARTIALLY_FILLED {
				cache.PutLimitOrder(order)
				if order.Kind == dao.ORDER_KIND_TP {
					cache.InitPutToPosition(order)
				}
			}
			if order.EntryOrderId != 0 {
				if orderMap[order.EntryOrderId] != nil {
					if order.Kind == dao.ORDER_KIND_TP {
						orderMap[order.EntryOrderId].TpOrder = order
					}
				}
			}
		} else if order.Type == dao.ORDER_TYPE_STOP {
			if order.State == dao.ORDER_STATE_PLACED {
				cache.PutStopOrder(order)
				cache.InitPutToPosition(order)
			} else {
				orderMap[order.Id] = order
			}
			if order.EntryOrderId != 0 {
				if orderMap[order.EntryOrderId] != nil {
					orderMap[order.EntryOrderId].SlOrder = order
				}
			}
		} else {
			panic("not supported order type. id: " + fmt.Sprint(order.Id))
		}
	}

	mLog.InfoF("position count: %d", cache.positionMap)

	//get position PL from db
	for id, position := range cache.positionMap {
		positionEntity, err := positionDao.GetById(instrumentName, id)
		if err != nil {
			panic(err)
		}
		position.PL = positionEntity.PL
		position.Amount = positionEntity.Amount
		position.Direction = positionEntity.Direction
		position.AccountId = positionEntity.AccountId
	}
}

//Put to positionMap TP or SL
func (cache *InstrumentOrderCache) InitPutToPosition(order *OrderCacheModel) {
	position := cache.positionMap[order.EntryOrderId]
	if position == nil {
		position = &PositionCacheModel{}
		cache.positionMap[order.EntryOrderId] = position
	}

	if order.Kind == dao.ORDER_KIND_TP {
		position.TpOrder = order
	}
	if order.Kind == dao.ORDER_KIND_SL {
		position.SlORder = order
	}
}

func (cache *InstrumentOrderCache) PutExpTimeOrder(order *OrderCacheModel) {
	cache.orderExpTimeMap[order.Id] = order
}

//when order touched we should remove from order exp time map
func (cache *InstrumentOrderCache) RemoveExpTimeOrder(orderId uint64) {
	delete(cache.orderExpTimeMap, orderId)
}

//return
// bool true - new price, false update
// int - price level (position)
func (cache *InstrumentOrderCache) PutLimitOrder(order *OrderCacheModel) (msg.OrderBookPriceIndexEventType, uint16, *PriceLvlContainer) {
	var priceCache []*PriceLvlContainer = nil

	if order.Direction == dao.ORDER_DIRECTION_SELL {
		priceCache = cache.LimitSellPriceArr
	} else {
		priceCache = cache.LimitBuyPriceArr
	}

	//try found by price already existing orders
	_, priceContainer := cache.searchPriceContainer(order.Price, priceCache)
	priceLevel := uint16(0)
	var eventType msg.OrderBookPriceIndexEventType = 0
	if priceContainer == nil {
		//new price
		priceContainer = &PriceLvlContainer{Price: order.Price}
		priceContainer.OrderArr = make([]*OrderCacheModel, 1, ORDER_COUNT_PER_PRICE_ALLOC)
		priceContainer.OrderArr[0] = order
		//add price to list of prices
		priceLevel, priceCache = cache.addPrice(order.Direction, priceContainer, priceCache)
		if order.Direction == dao.ORDER_DIRECTION_SELL {
			cache.LimitSellPriceArr = priceCache
		} else {
			cache.LimitBuyPriceArr = priceCache
		}
		eventType = msg.ORDER_BOOK_PRICE_EVENT_NEW
	} else {
		//add to tail
		priceContainer.OrderArr = append(priceContainer.OrderArr, order)
		//update volume
		priceLevelAllowMin, _ := cache.searchPriceContainer(order.Price, priceCache)
		if priceLevelAllowMin == -1 {
			panic("pricePosition=-1 !")
		}
		//found!
		priceLevel = uint16(priceLevelAllowMin)
		eventType = msg.ORDER_BOOK_PRICE_EVENT_CHANGE
	}
	priceContainer.Volume += order.Amount
	if order.Direction == dao.ORDER_DIRECTION_SELL {
		cache.sellVolTotal += order.Amount
	} else {
		cache.buyVolTotal += order.Amount
	}
	cache.msgSeqNumber++
	return eventType, priceLevel, priceContainer
}

//put stop order. Stop order will execute as market order
func (cache *InstrumentOrderCache) PutStopOrder(order *OrderCacheModel) []*OrderCacheModel {
	var stopMap map[uint64][]*OrderCacheModel = nil
	if order.Direction == dao.ORDER_DIRECTION_BUY {
		if order.PriceTrigger == dao.ORDER_PRICE_TRIGGER_BID_G {
			stopMap = cache.StopBuyBidMap
		} else {
			stopMap = cache.StopBuyAskMap
		}
	} else {
		if order.PriceTrigger == dao.ORDER_PRICE_TRIGGER_ASK_L {
			stopMap = cache.StopSellAskMap
		} else {
			stopMap = cache.StopSellBidMap
		}
	}
	//add to orders list
	orderList, ok := stopMap[order.Price]
	if ok {
		orderList = append(orderList, order)
	} else {
		orderList = make([]*OrderCacheModel, 1, 1000)
		orderList[0] = order
	}

	stopMap[order.Price] = orderList
	return orderList
}

//return removed price level
func (cache *InstrumentOrderCache) RemovePriceContainer(priceLvlIndex uint16, direction dao.OrderDirection,
	priceLvl *PriceLvlContainer) uint64 {
	if direction == dao.ORDER_DIRECTION_SELL {
		cache.LimitSellPriceArr = cache.removePriceWithIndex(priceLvlIndex, cache.LimitSellPriceArr)
	} else {
		cache.LimitBuyPriceArr = cache.removePriceWithIndex(priceLvlIndex, cache.LimitBuyPriceArr)
	}
	cache.msgSeqNumber++
	return cache.msgSeqNumber
}

func (cache *InstrumentOrderCache) RemoveLimitOrderSingle(order *OrderCacheModel) (msg.OrderBookPriceIndexEventType,
	uint16, *PriceLvlContainer) {
	priceArr := cache.LimitSellPriceArr
	if order.Direction == dao.ORDER_DIRECTION_BUY {
		priceArr = cache.LimitBuyPriceArr
	}
	return cache.RemoveLimitOrder(order, priceArr)
}

func (cache *InstrumentOrderCache) RemoveLimitOrder(order *OrderCacheModel,
	priceArr []*PriceLvlContainer) (msg.OrderBookPriceIndexEventType, uint16, *PriceLvlContainer) {
	orderBookPriceIndexEventType := msg.ORDER_BOOK_PRICE_EVENT_DELETE
	priceLvl, priceLvlContainer := cache.searchPriceContainer(order.Price, priceArr)
	priceLvlContainer.Volume -= order.Amount
	cache.msgSeqNumber++
	if order.Direction == dao.ORDER_DIRECTION_SELL {
		cache.sellVolTotal -= order.Amount
	} else {
		cache.buyVolTotal -= order.Amount
	}
	if len(priceLvlContainer.OrderArr) == 1 {
		//remove full container
		if order.Direction == dao.ORDER_DIRECTION_SELL {
			cache.LimitSellPriceArr = cache.removePriceWithIndex(uint16(priceLvl), priceArr)
		} else {
			cache.LimitBuyPriceArr = cache.removePriceWithIndex(uint16(priceLvl), priceArr)
		}
		return orderBookPriceIndexEventType, uint16(priceLvl), priceLvlContainer
	} else {
		orderBookPriceIndexEventType = msg.ORDER_BOOK_PRICE_EVENT_CHANGE
		for orderIndex, orderObj := range priceLvlContainer.OrderArr {
			if orderObj.Id == order.Id {
				cache.removeLimitOrder(uint32(orderIndex), priceLvlContainer)
				return orderBookPriceIndexEventType, uint16(priceLvl), priceLvlContainer
			}
		}
	}

	return orderBookPriceIndexEventType, 0, priceLvlContainer
}

/*
	remove limit order, update volume, etc
*/
func (cache *InstrumentOrderCache) MarkRemovedLimitOrder(orderIndex uint32, priceLvl uint16, order *OrderCacheModel,
	priceLvlContainer *PriceLvlContainer, priceArr []*PriceLvlContainer) msg.OrderBookPriceIndexEventType {
	orderBookPriceIndexEventType := msg.ORDER_BOOK_PRICE_EVENT_DELETE
	if len(priceLvlContainer.OrderArr) == 1 {
		//remove full container
		if order.Direction == dao.ORDER_DIRECTION_SELL {
			cache.LimitSellPriceArr = cache.removePriceWithIndex(priceLvl, priceArr)
		} else {
			cache.LimitBuyPriceArr = cache.removePriceWithIndex(priceLvl, priceArr)
		}
		cache.msgSeqNumber++
	} else {
		orderBookPriceIndexEventType = msg.ORDER_BOOK_PRICE_EVENT_CHANGE
		//cache.removeLimitOrder(orderIndex, priceLvlContainer)
	}

	return orderBookPriceIndexEventType
}

/*
	remove stop order, update volume, etc
	return true - if removed
*/
func (cache *InstrumentOrderCache) RemoveStopOrder(order *OrderCacheModel) bool {
	var stopMap map[uint64][]*OrderCacheModel = nil
	if order.Direction == dao.ORDER_DIRECTION_BUY {
		if order.PriceTrigger == dao.ORDER_PRICE_TRIGGER_BID_G {
			stopMap = cache.StopBuyBidMap
		} else {
			stopMap = cache.StopBuyAskMap
		}
	} else {
		if order.PriceTrigger == dao.ORDER_PRICE_TRIGGER_ASK_L {
			stopMap = cache.StopSellAskMap
		} else {
			stopMap = cache.StopSellBidMap
		}
	}

	if val, found := stopMap[order.Price]; found {
		return cache.removeStopOrder(order, stopMap, val)
	} else {
		return false
	}
}
func (cache *InstrumentOrderCache) removeStopOrder(order *OrderCacheModel, stopMap map[uint64][]*OrderCacheModel,
	orderList []*OrderCacheModel) bool {
	if len(orderList) == 1 {
		delete(stopMap, order.Price)
		return true
	} else {
		for i, ord := range orderList {
			if order.Id == ord.Id {
				stopMap[order.Price] = append(orderList[:i], orderList[i+1:]...)
				return true
			}
		}
		return false
	}
}

//return nil if not found
func (cache *InstrumentOrderCache) searchPriceContainer(price uint64,
	priceArr []*PriceLvlContainer) (int16, *PriceLvlContainer) {
	//TODO Artyom replace to binary search
	for i := 0; i < len(priceArr); i++ {
		if priceArr[i].Price == price {
			return int16(i), priceArr[i]
		}
	}
	//should never happen
	return -1, nil
}

func (cache *InstrumentOrderCache) searchOrder(price uint64, orderId uint64) *OrderCacheModel {
	_, orderType, orderDirection := cache.ExtractSeqId(orderId)
	//limit or stop
	if orderType == dao.ORDER_TYPE_LIMIT {
		var priceLvlContainer *PriceLvlContainer = nil
		if orderDirection == dao.ORDER_DIRECTION_SELL {
			_, priceLvlContainer = cache.searchPriceContainer(price, cache.LimitSellPriceArr)
		} else {
			_, priceLvlContainer = cache.searchPriceContainer(price, cache.LimitBuyPriceArr)
		}
		if priceLvlContainer == nil {
			return nil
		}
		for _, order := range priceLvlContainer.OrderArr {
			if order.Id == orderId {
				return order
			}
		}
		return nil
	} else if orderType == dao.ORDER_TYPE_STOP {
		if orderDirection == dao.ORDER_DIRECTION_SELL {
			orderArr, ok := cache.StopSellAskMap[price]
			if ok {
				for _, order := range orderArr {
					if order.Id == orderId {
						return order
					}
				}
			}
			orderArr, ok = cache.StopSellBidMap[price]
			if ok {
				for _, order := range orderArr {
					if order.Id == orderId {
						return order
					}
				}
			}
			return nil
		} else {
			orderArr, ok := cache.StopBuyAskMap[price]
			if ok {
				for _, order := range orderArr {
					if order.Id == orderId {
						return order
					}
				}
			}
			orderArr, ok = cache.StopBuyBidMap[price]
			if ok {
				for _, order := range orderArr {
					if order.Id == orderId {
						return order
					}
				}
			}
			return nil
		}
	}

	return nil
}

//low level remove limit order
func (cache *InstrumentOrderCache) removeLimitOrder(orderIndex uint32, priceLvlContainer *PriceLvlContainer) {
	priceLvlContainer.OrderArr = append(priceLvlContainer.OrderArr[:orderIndex], priceLvlContainer.OrderArr[orderIndex+1:]...)
}

//low level
func (cache *InstrumentOrderCache) removePrice(priceLvl *PriceLvlContainer, priceArr []*PriceLvlContainer) (int16, []*PriceLvlContainer) {
	index, _ := cache.searchPriceContainer(priceLvl.Price, priceArr)
	return index, append(priceArr[:index], priceArr[index+1:]...)
}
func (cache *InstrumentOrderCache) removePriceWithIndex(priceLevel uint16, priceArr []*PriceLvlContainer) []*PriceLvlContainer {
	return append(priceArr[:priceLevel], priceArr[priceLevel+1:]...)
}

//move from index to end existing data in slice
func (cache *InstrumentOrderCache) insertPrice(priceLevel uint16, priceLvl *PriceLvlContainer, priceArr []*PriceLvlContainer) []*PriceLvlContainer {
	priceArr = append(priceArr, priceLvl)
	copy(priceArr[priceLevel+1:], priceArr[priceLevel:])
	priceArr[priceLevel] = priceLvl
	return priceArr
}

func (cache *InstrumentOrderCache) addPrice(direction dao.OrderDirection, priceLvl *PriceLvlContainer,
	priceArr []*PriceLvlContainer) (uint16, []*PriceLvlContainer) {
	priceLevel := uint16(0)

	if len(priceArr) == 0 {
		//length 0, set first
		priceArr = append(priceArr, priceLvl)
		priceLevel = 0
	} else if direction == dao.ORDER_DIRECTION_SELL {
		if priceArr[0].Price > priceLvl.Price {
			//set first
			priceArr = cache.insertPrice(0, priceLvl, priceArr)
			priceLevel = 0
		} else if priceArr[len(priceArr)-1].Price < priceLvl.Price {
			//set last
			priceArr = append(priceArr, priceLvl)
			priceLevel = uint16(len(priceArr)) - 1
		} else {
			//search position to insert between prices
			for i := 1; i < len(priceArr); i++ {
				if priceLvl.Price < priceArr[i].Price {
					priceLevel = uint16(i)
					priceArr = cache.insertPrice(priceLevel, priceLvl, priceArr)
					break
				}
			}
		}
	} else {
		if priceArr[0].Price < priceLvl.Price {
			//set first
			priceArr = cache.insertPrice(0, priceLvl, priceArr)
			priceLevel = 0
		} else if priceArr[len(priceArr)-1].Price > priceLvl.Price {
			//set last
			priceArr = append(priceArr, priceLvl)
			priceLevel = uint16(len(priceArr) - 1)
		} else {
			//search position to insert between prices
			for i := 1; i < len(priceArr); i++ {
				if priceLvl.Price > priceArr[i].Price {
					priceLevel = uint16(i)
					priceArr = cache.insertPrice(priceLevel, priceLvl, priceArr)
					break
				}
			}
		}
	}

	return priceLevel, priceArr
}

//Create order book snapshot for RApi
func (cache *InstrumentOrderCache) CreateSnapshot() *msg.InstrOrderBookSnapshotResp {
	resp := &msg.InstrOrderBookSnapshotResp{}
	resp.InstrumentId = cache.InstrumentId
	orderBookSell := make([][]uint64, len(cache.LimitSellPriceArr))
	orderBookBuy := make([][]uint64, len(cache.LimitBuyPriceArr))

	var priceContainer *PriceLvlContainer = nil
	for i := 0; i < len(cache.LimitSellPriceArr); i++ {
		priceContainer = cache.LimitSellPriceArr[i]
		orderBookSell[i] = []uint64{priceContainer.Price, priceContainer.Volume, uint64(len(priceContainer.OrderArr))}
	}
	for i := 0; i < len(cache.LimitBuyPriceArr); i++ {
		priceContainer = cache.LimitBuyPriceArr[i]
		orderBookBuy[i] = []uint64{priceContainer.Price, priceContainer.Volume, uint64(len(priceContainer.OrderArr))}
	}
	resp.OrderSellArr = orderBookSell
	resp.OrderBuyArr = orderBookBuy
	resp.MsgSequenceNumber = cache.msgSeqNumber
	return resp
}

func (cache *InstrumentOrderCache) GetVolumeTotal() (uint64, uint64) {
	return cache.sellVolTotal, cache.buyVolTotal
}

func (cache *InstrumentOrderCache) SetOrderId(orderId uint64) {
	cache.orderIdSeq = orderId
}

func (cache *InstrumentOrderCache) GetOrderId() uint64 {
	return cache.orderIdSeq
}

//format: ID + order type + order direction
func (cache *InstrumentOrderCache) GetNexOrderId(orderType dao.OrderType, direction dao.OrderDirection) uint64 {
	cache.orderIdSeq++
	return cache.orderIdSeq*100 + uint64(orderType)*10 + uint64(direction)
}

//Return order seq. number, order type, order direction from orderId
func (cache *InstrumentOrderCache) ExtractSeqId(orderId uint64) (uint64, dao.OrderType, dao.OrderDirection) {
	data := orderId % 100
	return orderId / 100, dao.OrderType(data / 10), dao.OrderDirection(data % 10)
}

/*
func (cache *InstrumentOrderCache) IncSeqNumber() uint64 {
	cache.msgSeqNumber += 1
	return cache.msgSeqNumber
}
*/
