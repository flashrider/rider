package service

import (
	"fmt"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util/msg"
)

/**
Cancel order and order expire time logic
*/
type CancelOrderService struct {
	removeOrderCh chan *OrderCtx
}

func NewCancelOrder() *CancelOrderService {
	removeOrderCh := make(chan *OrderCtx, 1024)
	return &CancelOrderService{removeOrderCh}
}

func (service *CancelOrderService) RegisterOrderForDelete(oCtx *OrderCtx) {
	service.removeOrderCh <- oCtx
}

func (service *CancelOrderService) CancelOrder(cancelOrderReq *msg.CancelOrderReq) {
	instrumentOrderCache := instrumentOrderMap[cancelOrderReq.InstrumentId]
	if instrumentOrderCache == nil {
		apiMainPublisher.PublishMsgAsync(ERR_INVALID_REQUEST.ToBinary(cancelOrderReq.AccountId, cancelOrderReq.CorrelationId))
		return
	}
	order := instrumentOrderCache.searchOrder(cancelOrderReq.Price, cancelOrderReq.OrderId)
	if order == nil {
		apiMainPublisher.PublishMsgAsync(ERR_ORDER_NOT_FOUND.ToBinary(cancelOrderReq.AccountId, cancelOrderReq.CorrelationId))
		//not found
	} else {
		//check order owner
		if order.AccountId == cancelOrderReq.AccountId {
			order.State = dao.ORDER_STATE_CANCELED
			service.removeOrder(order, instrumentOrderCache, cancelOrderReq.CorrelationId)
		} else {
			apiMainPublisher.PublishMsgAsync(ERR_NOT_OWNER.ToBinary(cancelOrderReq.AccountId, cancelOrderReq.CorrelationId))
		}
		//create mem snapshot
		//CancelOrderReq
	}
}

//remove marked orders, remove from order book
func (service *CancelOrderService) EvictMarkedOrders() {
	for orderCtx := range service.removeOrderCh {
		//between put to queue end eviction order can be executed
		if orderCtx.takerOrder.State == dao.ORDER_STATE_EXPIRED {
			service.removeOrder(orderCtx.takerOrder, orderCtx.instrOrderCache, 0)
		}
	}
}

func (service *CancelOrderService) removeOrder(order *OrderCacheModel, instrumentOrderCache *InstrumentOrderCache,
	correlationId uint64) {
	//mem snapshot
	memSnapshot := NewMemSnapshot(instrumentOrderCache)
	if order.Type == dao.ORDER_TYPE_LIMIT {
		instrumentOrderCache.RemoveLimitOrderSingle(order)
	} else {
		instrumentOrderCache.RemoveStopOrder(order)
	}
	memSnapshot.UpdateOrder(orderService.ToOrderChangeEntity(order))
	//remove TP order
	if order.TpOrder != nil {
		order.TpOrder.State = dao.ORDER_STATE_EXPIRED
		instrumentOrderCache.RemoveLimitOrderSingle(order.TpOrder)
		memSnapshot.UpdateOrder(orderService.ToOrderChangeEntity(order.TpOrder))
	}
	//remove SL order
	if order.SlOrder != nil {
		order.SlOrder.State = dao.ORDER_STATE_EXPIRED
		instrumentOrderCache.RemoveStopOrder(order.SlOrder)
		memSnapshot.UpdateOrder(orderService.ToOrderChangeEntity(order.SlOrder))
	}

	//if entry return money back
	if order.Kind == dao.ORDER_KIND_ENTRY {
		bCurrId, qCurrId, found := instrumentCache.GetInstrumentCurrency(instrumentOrderCache.InstrumentId)
		if !found {
			maLog.Err("instrument not found: " + fmt.Sprint(instrumentOrderCache.InstrumentId))
			apiMainPublisher.PublishMsgAsync(ERR_INSTRUMENT_NOT_FOUND.ToBinary(order.AccountId, correlationId))
			return
		}
		//return money back
		bFund := fundCache.Get(order.AccountId, bCurrId)
		qFund := fundCache.Get(order.AccountId, qCurrId)
		if order.Direction == dao.ORDER_DIRECTION_SELL {
			bFund.Reserved -= order.Amount
			memSnapshot.UpdateFund(&(*bFund)) //make a copy
		} else {
			qFund.Reserved -= order.Amount * order.Price / FIN_DECIMAL
			memSnapshot.UpdateFund(&(*qFund)) //make a copy
		}
	}

	//send notification messages
	msgWriteService.ProcessSnapshot(memSnapshot)
	//write snapshot
	//dbWriterService.ProcessSnapshot(memSnapshot)
}
