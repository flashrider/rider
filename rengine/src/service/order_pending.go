package service

import (
	"gitlab.com/flashrider/rider/dao"
	"strconv"
)

/**
Limit/Stop(Entry) orders executor
ArtyomA
*/
type PendingOrderService struct {
}

func NewPendingOrderService() *PendingOrderService {
	return &PendingOrderService{}
}

func (service *PendingOrderService) Put(oCtx *OrderCtx) *AppErr {
	//create entry order entity
	//we don't have open position (false) related to this order(s)
	entryEntity := orderService.CreateEntryOrderEntity(oCtx.takerOrder, false)
	//set exp time
	extTime, err := orderService.OrderTimeDuratinCalc(oCtx.openOrderReq.Duration, oCtx.openOrderReq.Time)
	if err != nil {
		return err
	}
	entryEntity.ExpTime = extTime

	var fund dao.FundCacheModel
	orderIdStr := strconv.FormatUint(oCtx.orderId, 10)

	bestAsk := uint64(0)
	bestBid := uint64(0)

	//decrease a balance
	if entryEntity.Direction == dao.ORDER_DIRECTION_SELL {
		maLog.InfoIdF(orderIdStr, "b balance: %7d/%7d", oCtx.takerBFund.Balance, oCtx.takerBFund.Reserved)
		oCtx.takerBFund.Reserved += oCtx.takerOrder.Amount
		fund = *oCtx.takerBFund //make a copy
		maLog.InfoIdF(orderIdStr, "b reserved: %7d", fund.Reserved)
		if len(oCtx.instrOrderCache.LimitSellPriceArr) != 0 {
			oCtx.bestAsk = oCtx.instrOrderCache.LimitSellPriceArr[0].Price
		}
	} else {
		maLog.InfoIdF(orderIdStr, "q balance: %7d/%7d", oCtx.takerQFund.Balance, oCtx.takerQFund.Reserved)
		oCtx.takerQFund.Reserved += oCtx.takerOrder.Amount * oCtx.takerOrder.Price / FIN_DECIMAL
		fund = *oCtx.takerQFund //make a copy
		maLog.InfoIdF(orderIdStr, "q reserved: %7d", oCtx.takerQFund.Reserved)
		if len(oCtx.instrOrderCache.LimitBuyPriceArr) != 0 {
			oCtx.bestBid = oCtx.instrOrderCache.LimitBuyPriceArr[0].Price
		}
	}
	oCtx.memSnp.UpdateFund(&fund)

	//add stop order to
	if oCtx.takerOrder.Type == dao.ORDER_TYPE_STOP {
		orderList := oCtx.instrOrderCache.PutStopOrder(oCtx.takerOrder)
		//print orders count by stop price
		maLog.InfoIdF(orderIdStr, "entry stop, orders:%d", len(orderList))
	} else {
		orderService.PutLimitOrder(orderIdStr, oCtx.instrOrderCache, oCtx.takerOrder)
	}

	//set user take profit order ?
	if oCtx.openOrderReq.TPPrice != 0 {
		tpOrder := orderService.CreateTPOrder(oCtx.takerOrder, oCtx.openOrderReq)
		tpOrder.Id = oCtx.instrOrderCache.GetNexOrderId(tpOrder.Type, tpOrder.Direction)
		oCtx.memSnp.NewOrder(orderService.CreateTPEntity(tpOrder))
	}

	//set user stop loss order ?
	if oCtx.openOrderReq.SLPrice != 0 {
		slOrder := orderService.CreateSLOrder(oCtx.takerOrder, oCtx.openOrderReq)
		slOrder.Id = oCtx.instrOrderCache.GetNexOrderId(slOrder.Type, slOrder.Direction)
		oCtx.memSnp.NewOrder(orderService.CreateSLEntity(slOrder))
	}

	//should be a last
	oCtx.memSnp.NewOrder(entryEntity)
	//try execute stop orders. Call only if new best ASK/BID price
	if entryEntity.Direction == dao.ORDER_DIRECTION_SELL {
		if oCtx.instrOrderCache.LimitSellPriceArr[0].Price != bestAsk {
			service.TryExecuteStopOrders(entryEntity.Direction, oCtx)
		}
	} else {
		if oCtx.instrOrderCache.LimitBuyPriceArr[0].Price != bestBid {
			service.TryExecuteStopOrders(entryEntity.Direction, oCtx)
		}
	}

	return nil
}

func (service *PendingOrderService) TryExecuteStopOrders(orderTriggerDirection dao.OrderDirection, oCtx *OrderCtx) *AppErr {
	exit := true
	if orderTriggerDirection == dao.ORDER_DIRECTION_SELL {
		priceLvlArr := oCtx.instrOrderCache.LimitSellPriceArr
		//changed sell book
		//best ask
		ask := priceLvlArr[0].Price
		for priceLvl, list := range oCtx.instrOrderCache.StopSellAskMap {
			if priceLvl >= ask {
				exit = false
				for _, order := range list {
					if appErr := marketOrderService.ExecuteStopOrders(oCtx, order); appErr != nil {
						return appErr
					}
				}
				delete(oCtx.instrOrderCache.StopSellAskMap, priceLvl)
			}
		}
	} else {
		priceLvlArr := oCtx.instrOrderCache.LimitBuyPriceArr
		//price down, changed buy book
		bid := priceLvlArr[0].Price
		for priceLvl, list := range oCtx.instrOrderCache.StopBuyBidMap {
			if priceLvl <= bid {
				exit = false
				for _, order := range list {
					if appErr := marketOrderService.ExecuteStopOrders(oCtx, order); appErr != nil {
						return appErr
					}
				}
				delete(oCtx.instrOrderCache.StopBuyBidMap, priceLvl)
			}
		}
	}

	if exit {
		return nil
	} else {
		return service.TryExecuteStopOrders(orderTriggerDirection, oCtx)
	}
}
