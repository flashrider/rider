package service

import (
	"fmt"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"strconv"
	"time"
)

/**
Main service for orders
ArtyomA
*/
const (
	FIN_DECIMAL = 100000 //money and amount min decimals: 0.00001
)

type OrderService struct {
}

func NewOrderService() *OrderService {
	orderService := &OrderService{}
	go orderService.CheckOrderExpTime()
	return orderService
}

func (service *OrderService) CheckOrderExpTime() {
	now := uint64(0)
	found := false
	for {
		for _, instrOrderCache := range instrumentOrderMap {
			now = util.NowMk()
			for orderId, order := range instrOrderCache.orderExpTimeMap {
				if order.State == dao.ORDER_STATE_IFD || order.State == dao.ORDER_STATE_PLACED {
					if order.ExpTime <= now {
						found = true
						oCtx := &OrderCtx{}
						order.State = dao.ORDER_STATE_EXPIRED
						oCtx.takerOrder = order
						oCtx.instrOrderCache = instrOrderCache
						cancelOrderService.RegisterOrderForDelete(oCtx)
						delete(instrOrderCache.orderExpTimeMap, orderId)
					}
				}
			}
		}
		if found {
			routerService.AddMessage([]byte{uint8(msg.ENGINE_MSG_ORDER_EXPIRED)})
			found = false
		}

		//sleep can drift but for us not important ~1sec
		time.Sleep(time.Millisecond * 900)
	}
}

/*
	Prepare objects(account, funds) data for order to execution
*/
func (orderService *OrderService) OpenOrder(orderReq *msg.OpenOrderReq) {
	orderCtx := &OrderCtx{}
	bCurrId, qCurrId, found := instrumentCache.GetInstrumentCurrency(orderReq.InstrumentId)
	if !found {
		maLog.Err("instrument not found: " + fmt.Sprint(orderReq.InstrumentId))
		apiMainPublisher.PublishMsgAsync(ERR_INSTRUMENT_NOT_FOUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
		return
	}
	/* TODO move check to rapi!

	if uint64(instrumentCache.Get(orderReq.InstrumentId).MinAmount) > orderReq.Amount{
		apiMainPublisher.PublishMsgAsync(ERR_NOT_INSTRUMENT_MIN_AMOUNT.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
		return
	}
	*/
	//get from cache base fund
	orderCtx.takerBFund = fundCache.Get(orderReq.AccountId, bCurrId)
	if orderCtx.takerBFund == nil {
		apiMainPublisher.PublishMsgAsync(ERR_ACCOUNT_NOT_FOUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
		return
	}
	if orderCtx.takerBFund.Id == 0 {
		apiMainPublisher.PublishMsgAsync(ERR_BASE_FUND_NOT_FOUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
		return
	}

	orderCtx.takerQFund = fundCache.Get(orderReq.AccountId, qCurrId)
	if orderCtx.takerQFund.Id == 0 {
		apiMainPublisher.PublishMsgAsync(ERR_QUOTE_FUND_NOT_FOUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
		return
	}

	//check balance for limit order
	if orderReq.Type == dao.ORDER_TYPE_LIMIT {
		if orderReq.Direction == dao.ORDER_DIRECTION_SELL {
			if orderCtx.takerBFund.Balance-orderCtx.takerBFund.Reserved < orderReq.Amount {
				apiMainPublisher.PublishMsgAsync(ERR_INSUFFICIENT_FUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
				return
			}
		} else {
			if orderCtx.takerQFund.Balance-orderCtx.takerQFund.Reserved < orderReq.Amount*orderReq.Price/FIN_DECIMAL {
				apiMainPublisher.PublishMsgAsync(ERR_INSUFFICIENT_FUND.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
				return
			}
		}
	}

	instrumentOrderCache := instrumentOrderMap[orderReq.InstrumentId]
	//create mem snapshot
	memSnapshot := NewMemSnapshot(instrumentOrderCache)
	entryOrder := &OrderCacheModel{State: dao.ORDER_STATE_PLACED}
	//close position, manually
	if orderReq.PositionId == 0 {
		entryOrder.FromOpenOrderMsg(orderReq)
	} else {
		//check position
		position := instrumentOrderCache.positionMap[orderReq.PositionId]
		if position.AccountId != orderReq.AccountId {
			apiMainPublisher.PublishMsgAsync(ERR_NOT_OWNER.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
			return
		}
		//create an order to close position
		orderCtx.closePosition = position
		entryOrder.AccountId = position.AccountId
		entryOrder.Amount = position.Amount
		entryOrder.Kind = dao.ORDER_KIND_ENTRY
		entryOrder.Type = dao.ORDER_TYPE_MKT
		entryOrder.AmountRest = position.Amount
		//direction reverse
		if position.Direction == dao.ORDER_DIRECTION_BUY {
			entryOrder.Direction = dao.ORDER_DIRECTION_SELL
		} else {
			entryOrder.Direction = dao.ORDER_DIRECTION_BUY
		}
	}

	//allocate next orderReq id
	entryOrderId := instrumentOrderCache.GetNexOrderId(entryOrder.Type, entryOrder.Direction)
	entryOrder.Id = entryOrderId
	orderIdStr := strconv.FormatUint(entryOrderId, 10)
	maLog.InfoIdF(orderIdStr, "instr:%4d aId:%8d dir:%d %10d/%10d mSnp:%12d", orderReq.InstrumentId, orderReq.AccountId,
		orderReq.Direction, orderReq.Amount, orderReq.Price, memSnapshot.Id)

	//order context
	orderCtx.takerOrder = entryOrder
	orderCtx.instrOrderCache = instrumentOrderCache
	orderCtx.orderIdStr = orderIdStr
	orderCtx.orderId = entryOrderId
	orderCtx.memSnp = memSnapshot
	orderCtx.openOrderReq = orderReq
	var appErr *AppErr = nil

	switch orderReq.Type {
	case dao.ORDER_TYPE_MKT:
		appErr = marketOrderService.ExecuteMarketOrder(orderCtx)
		if appErr == nil {
			marketOrderService.TryExecuteStopOrders(orderCtx)
		}
	case dao.ORDER_TYPE_STOP, dao.ORDER_TYPE_LIMIT:
		appErr = pendingOrderService.Put(orderCtx)
	}

	//print order book
	if maLog.IsDebugLvl() {
		maLog.DebugIdF(orderIdStr, "sell: %v", instrumentOrderCache.LimitSellPriceArr)
		maLog.DebugIdF(orderIdStr, "buy: %v", instrumentOrderCache.LimitBuyPriceArr)
	}
	if appErr == nil {
		//finalize changes at order context
		orderCtx.Commit()
		//send notification messages
		msgWriteService.ProcessSnapshot(memSnapshot)
		//write snapshot
		//dbWriterService.ProcessSnapshot(memSnapshot)
	} else {
		maLog.ErrReqId(orderIdStr, "err id: "+fmt.Sprint(appErr.Id)+" details: "+appErr.Detail)
		//send error to user
		apiMainPublisher.PublishMsgAsync(appErr.ToBinary(orderReq.AccountId, orderReq.CorrelationId))
	}

}

func (orderService *OrderService) CreateEntryOrderEntity(entryOrder *OrderCacheModel, setPositionId bool) *dao.OrderEntity {
	entity := entryOrder.ToOrderEntity()
	entity.CreatedTime = util.NowMk()
	if setPositionId {
		entity.PositionId = entity.Id
	}
	return entity
}

/**
Create TP order
*/
func (orderService *OrderService) CreateTPOrder(entryOrder *OrderCacheModel,
	openOrdReq *msg.OpenOrderReq) *OrderCacheModel {
	//order, make a copy
	order := *entryOrder
	order.Kind = dao.ORDER_KIND_TP
	order.Type = dao.ORDER_TYPE_LIMIT
	order.State = dao.ORDER_STATE_IFD
	order.Price = openOrdReq.TPPrice
	order.PriceTrigger = openOrdReq.TPPriceTrigger
	order.EntryOrderId = entryOrder.Id
	if entryOrder.Direction == dao.ORDER_DIRECTION_SELL {
		order.Direction = dao.ORDER_DIRECTION_BUY
	} else {
		order.Direction = dao.ORDER_DIRECTION_SELL
	}
	entryOrder.TpOrder = &order
	return entryOrder.TpOrder
}

func (orderService *OrderService) CreateTPEntity(tpOrder *OrderCacheModel) *dao.OrderEntity {
	entity := tpOrder.ToOrderEntity()
	entity.CreatedTime = util.NowMk()
	return entity
}

/**
Create SL order
*/
func (orderService *OrderService) CreateSLOrder(entryOrder *OrderCacheModel,
	putOrderMsg *msg.OpenOrderReq) *OrderCacheModel {
	//order, make a copy
	order := *entryOrder
	order.Kind = dao.ORDER_KIND_SL
	order.Type = dao.ORDER_TYPE_STOP
	order.State = dao.ORDER_STATE_IFD
	order.Price = putOrderMsg.SLPrice
	order.PriceTrigger = putOrderMsg.SLPriceTrigger
	order.EntryOrderId = entryOrder.Id
	if entryOrder.Direction == dao.ORDER_DIRECTION_SELL {
		order.Direction = dao.ORDER_DIRECTION_BUY
	} else {
		order.Direction = dao.ORDER_DIRECTION_SELL
	}
	entryOrder.SlOrder = &order
	return entryOrder.SlOrder
}

func (orderService *OrderService) CreateSLEntity(slOrder *OrderCacheModel) *dao.OrderEntity {
	entity := slOrder.ToOrderEntity()
	entity.CreatedTime = util.NowMk()
	return entity
}

func (orderService *OrderService) PutLimitOrder(orderIdStr string, instrOrderCache *InstrumentOrderCache,
	orderCacheModel *OrderCacheModel) {
	eventType, priceLevel, priceCont := instrOrderCache.PutLimitOrder(orderCacheModel)
	maLog.InfoIdF(orderIdStr, "put limit, event:%d d:%d l:%3d v:%8d c:%3d", eventType, orderCacheModel.Direction, priceLevel, priceCont.Volume, len(priceCont.OrderArr))
	//create order book change event
	event := &msg.OrderBookChangeEvent{}
	event.InstrumentId = instrOrderCache.InstrumentId
	event.MsgSeqNum = instrOrderCache.msgSeqNumber
	event.Volume = priceCont.Volume
	event.OrderCount = uint32(len(priceCont.OrderArr))
	event.EventType = eventType
	event.PriceLevel = priceLevel
	event.Price = orderCacheModel.Price
	event.Direction = orderCacheModel.Direction
	oBookChangesPublisher.PublishMsgAsync(event.ToBinary())
}

/*
	ORDER_DURATION_IOC    OrderDuration = 4 // Immediate or Cancel Order
	ORDER_DURATION_FOK    OrderDuration = 5 // Fill-Or-Kill
	ORDER_DURATION_AON    OrderDuration = 6 // All-or-None, till end of session
*/
//TODO Fill fully pls !
func (orderService *OrderService) OrderTimeDuratinCalc(duration dao.OrderDuration,
	expTime uint64) (uint64, *AppErr) {
	now := time.Now().UTC()
	switch duration {
	case dao.ORDER_DURATION_DAY:
		return uint64(now.Add(time.Hour*24).UnixNano() / 1000), nil
	case dao.ORDER_DURATION_GTC:
		return 0, nil
	case dao.ORDER_DURATION_GTD:
		if expTime+10000000000 <= util.NowMk() {
			return 0, ERR_INVALID_ORDER_EXP_TIME
		}
		return expTime, nil
	case dao.ORDER_DURATION_MINUTE:
		{
			//overwrite invalid value
			if expTime == 0 || expTime > 10000 {
				expTime = 1
			}
			return uint64(now.Add(time.Minute*time.Duration(expTime)).UnixNano() / 1000), nil
		}
	default:
		return 0, nil
	}
}

func (service *OrderService) ToOrderChangeEntity(order *OrderCacheModel) *dao.OrderChangesEntity {
	orderChangeEntity := &dao.OrderChangesEntity{}
	orderChangeEntity.Id = order.Id
	orderChangeEntity.State = order.State
	orderChangeEntity.AccountId = order.AccountId
	orderChangeEntity.AmountRest = order.AmountRest
	if order.Kind == dao.ORDER_KIND_ENTRY {
		orderChangeEntity.PositionId = order.Id
	} else {
		orderChangeEntity.PositionId = order.EntryOrderId
	}
	return orderChangeEntity
}
