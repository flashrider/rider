package service

import (
	"fmt"
	"github.com/robfig/cron"
	"testing"
	"time"
)

func TestRunScheduler(t *testing.T) {
	c := cron.New()
	c.AddFunc("0/2 * * * * *", func() {
		now := time.Now().UTC()
		fmt.Println("Every second: ", now.Second(), now.UnixNano()/int64(time.Microsecond))
	})
	c.Start()
	time.Sleep(time.Second * 10)
}
