package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rengine/src/api/binary"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
	"time"
)

//global variables and services
var (
	ctx    *AppContext
	mLog   *flog.Logger //main rLog
	maLog  *flog.Logger //matching rLog
	active bool         //application active
	//dao
	accountDao    *dao.AccountDao
	orderDao      *dao.OrderDao
	currencyDao   *dao.CurrencyDao
	fundDao       *dao.FundDao
	instrumentDao *dao.InstrumentDao
	tradeDao      *dao.TradeDao
	extTxDao      *dao.ExtTxDao
	ohlcAskDao    *dao.OhlcDao
	ohlcBidDao    *dao.OhlcDao
	positionDao   *dao.PositionDao

	//service
	currencyCache       *service.CurrencyCache
	instrumentCache     *service.InstrumentCache
	fundCache           *FundCache
	instrumentOrderMap  map[uint16]*InstrumentOrderCache //key instrument id
	routerService       *RouterService
	orderService        *OrderService
	marketOrderService  *MarketOrderService
	pendingOrderService *PendingOrderService
	cancelOrderService  *CancelOrderService
	diagnosticService   *service.DiagnosticService
	dbWriterService     *DbWriterService
	msgWriteService     *MsgWriterService
	instrOBSnapPubl     *service.MsgPublisher //order book snapshot publisher
	//listeners and publishers
	binaryAPI             *binary.EngineBinaryApi
	apiMainListener       *service.MsgListener  //main-req
	apiMainPublisher      *service.MsgPublisher //main-resp
	oBookChangesPublisher *service.MsgPublisher //order book changes
	apiTradePublisher     *service.MsgPublisher //new trades publisher
	exchangeHalted        bool                  //true if can't write to db or matching error
)

/*
	Init services life cycle
*/
func InitServices(_ctx *AppContext) {
	ctx = _ctx
	mLog = ctx.Logger
	maLog = ctx.LogManager.NewLogger("matching", ctx.App.MatchingLogThreshold)
	accountDao = dao.NewAccountDao()
	orderDao = dao.NewOpenOrderDao()
	currencyDao = dao.NewCurrencyDao()
	fundDao = dao.NewFundDao()
	instrumentDao = dao.NewInstrumentDao()
	tradeDao = dao.NewTradeDao()
	extTxDao = dao.NewExtTxDao()
	ohlcAskDao = dao.NewOhlcAskDao()
	ohlcBidDao = dao.NewOhlcBidDao()
	positionDao = dao.NewPositionDao()

	active = true
	/**
	Init caches services
	*/
	mLog.Info("start init currencyCache")
	currencyCache = service.NewCurrencyCache(ctx.LogManager, currencyDao)
	mLog.InfoS("end init")

	mLog.Info("start init instrumentCache")
	instrumentCache = service.NewInstrumentCache(ctx.LogManager, instrumentDao)
	mLog.InfoS("end init")

	mLog.Info("start init fundCache")
	fundCache = NewFundCache(instrumentCache.GetCacheSize()+10, true)
	mLog.InfoS("end init")

	mLog.Info("start load from DB orders")
	instrumentOrderMap = make(map[uint16]*InstrumentOrderCache, instrumentCache.GetCacheSize()+10)
	for _, instr := range instrumentCache.GetCache() {
		instrumentOrderMap[instr.Id] = NewInstrumentOrderCache(instr.Id, instr.Symbol, instr.BaseCurrCodeId,
			instr.QuoteCurrCodeId, true)
	}
	mLog.Info("end load")
	/*
		Init services
	*/
	mLog.Info("start init dbWriterService")
	dbWriterService = NewDbWriterService()
	mLog.InfoS("end init")

	mLog.Info("start init msgWriteService")
	msgWriteService = NewMsgWriterService()
	mLog.InfoS("end init")

	mLog.Info("start init diagnosticService")
	diagnosticService = service.NewDiagnosticService(ctx.LogManager)
	mLog.InfoS("end init:", diagnosticService)

	mLog.Info("start init orderService")
	orderService = NewOrderService()
	mLog.InfoS("end init:", orderService)

	mLog.Info("start init marketOrderService")
	marketOrderService = NewMarketOrderService()
	mLog.InfoS("end init:", marketOrderService)

	mLog.Info("start init routerService")
	routerService = NewRouterService(ctx.LogManager)
	mLog.InfoS("end init:", routerService)

	mLog.Info("start init cancelOrderService")
	cancelOrderService = NewCancelOrder()
	mLog.InfoS("end init:", cancelOrderService)

	/*
		API and listener should be last
	*/
	//init http API
	mLog.Info("binary api: " + ctx.App.Api.Binary.Interface)
	binaryAPI = binary.NewEngineBinaryApi(ctx.App.Api.Binary.Interface, ctx.App.Api.Binary.BrokerLogLevel)
	time.Sleep(time.Second)
	mLog.Info("start init instrOBSnapPubl")
	instrOBSnapPubl = service.NewMsgPublisher(msg.TOPIC_ENGINE_MSG_GET_SNAPSHOT_RESP, ctx.App.Api.Binary.Interface)
	mLog.InfoS("end init:", instrOBSnapPubl)
	mLog.Info("start init apiMainPublisher")
	apiMainPublisher = service.NewMsgPublisher(msg.TOPIC_ENGINE_MAIN_MSG_RESP, ctx.App.Api.Binary.Interface)
	mLog.InfoS("end init")
	mLog.Info("start init oBookChangesPublisher")
	oBookChangesPublisher = service.NewMsgPublisher(msg.TOPIC_ENGINE_MSG_ORDER_BOOK_FEED, ctx.App.Api.Binary.Interface)
	mLog.InfoS("end init")
	mLog.Info("start init apiTradePublisher")
	apiTradePublisher = service.NewMsgPublisher(msg.TOPIC_ENGINE_MSG_TRADES_FEED, ctx.App.Api.Binary.Interface)
	mLog.InfoS("end init")
	//Listener should be last
	mLog.Info("start init apiMainListener")
	apiMainListener = service.NewMsgListener(msg.TOPIC_ENGINE_MAIN_MSG_REQ, routerService.queue, ctx.App.Api.Binary.Interface)
	mLog.InfoS("end init")

}

/*
	Destroy services life cycle
*/
func DestroyServices() {
	exchangeHalted = true
	active = false
	apiMainListener.Shutdown()
	time.Sleep(time.Second)
	apiMainPublisher.Shutdown()
	binaryAPI.Shutdown()

}

/*
func toStr(i uint64) string {
	return strconv.FormatUint(i, 10)
}

func toStr32(i uint32) string {
	return strconv.FormatUint(uint64(i), 10)
}

func toStr8(i uint8) string {
	return strconv.FormatUint(uint64(i), 10)
}
*/
