package service

import "gitlab.com/flashrider/rider/dao"

/**
Store funds at mem
ArtyomAminov
*/
type FundCache struct {
	accBuff [][]*dao.FundCacheModel //[accountId][currencyId] - index, value fundId
}

func NewFundCache(currencyIdCount uint16, loadFromDb bool) *FundCache {
	accountCount := uint64(100000)
	if loadFromDb {
		count, err := accountDao.GetCount()
		if err != nil {
			mLog.Info("accountDao.GetCount(): " + err.Error())
			panic(err)
		}
		mLog.InfoF("db account count: %d", count)
		accountCount += count
	}

	cache := make([][]*dao.FundCacheModel, accountCount)
	currencyIdCount += 50
	for i := range cache {
		cache[i] = make([]*dao.FundCacheModel, currencyIdCount)
	}
	fundCache := &FundCache{cache}

	if loadFromDb {
		mLog.Info("start load funds")
		accArr, fundArr, currArr, balanceArr, inOpenOrderArr, stateArr, err := fundDao.GetAllCompact()
		if err != nil {
			mLog.Err("fundDao.GetAllCompact: " + err.Error())
			panic(err)
		}
		accId, currId := uint32(0), uint16(0)
		var cacheFund *dao.FundCacheModel = nil
		for i, accIdInt := range accArr {
			cacheFund = &dao.FundCacheModel{}
			accId = uint32(accIdInt.(uint64))
			cacheFund.Id = fundArr[i].(uint64)
			currId = uint16(currArr[i].(uint64))
			cacheFund.Balance = balanceArr[i].(uint64)
			cacheFund.AccountId = accId
			cacheFund.Reserved = inOpenOrderArr[i].(uint64)
			cacheFund.State = dao.FundState(stateArr[i].(uint64))
			fundCache.Put(accId, currId, cacheFund)
		}
		mLog.InfoF("end load funds. Count: %d", len(accArr))
	}
	return fundCache
}

//update acc fund
func (cache *FundCache) Put(accId uint32, fundCurrId uint16, model *dao.FundCacheModel) {
	cache.accBuff[accId][fundCurrId] = model
}

// get fundId
func (cache *FundCache) Get(accId uint32, fundCurrId uint16) *dao.FundCacheModel {
	return cache.accBuff[accId][fundCurrId]
}
