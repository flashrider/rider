package service

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/flashrider/rider/util/msg"
)

var (
	//not available DB, some error, panic
	ERR_METHOD_UNAVAILABLE = appErr(1)
	//invalid request, no mandatory fields
	ERR_INVALID_REQUEST = appErr(2)
	//not found data
	ERR_ACCOUNT_NOT_FOUND = appErr(3)
	//when account already created
	ERR_ACCOUNT_ALREADY_CREATED = appErr(4)
	//Balance
	ERR_INSUFFICIENT_FUND = appErr(5)
	//Operation not permitted
	ERR_OPERATION_NOT_PERMITTED = appErr(6)
	// Invalid OTP token
	ERR_INVALID_OTP_TOKEN = appErr(7)
	//instrument found but session not active, see state
	ERR_INSTRUMENT_ACTIVE_SESSION_NOT_FOUND = appErr(8)
	//instrument not found
	ERR_INSTRUMENT_NOT_FOUND = appErr(9)
	//user not created base fund (never cashIn)
	ERR_BASE_FUND_NOT_FOUND = appErr(10)
	//user not created quote fund (never cashIn)
	ERR_QUOTE_FUND_NOT_FOUND = appErr(11)
	//limit orders can't cover market order
	ERR_NOT_ENOUGH_VOL_TO_EXEC_MKT = appErr(12)

	ERR_INVALID_ORDER_EXP_TIME = appErr(13)
	//instrument invalid min amount
	ERR_NOT_INSTRUMENT_MIN_AMOUNT = appErr(14)
	//order max slippage limit
	ERR_ORDER_MAX_SLIPPAGE_LIMIT = appErr(15)
	//not found
	ERR_ORDER_NOT_FOUND = appErr(16)
	//account not owner of order
	ERR_NOT_OWNER = appErr(17)
)

func GetErrById(errId uint8) *AppErr {
	switch errId {
	case ERR_METHOD_UNAVAILABLE.Id:
		return ERR_METHOD_UNAVAILABLE
	case ERR_INVALID_REQUEST.Id:
		return ERR_INVALID_REQUEST
	case ERR_ACCOUNT_NOT_FOUND.Id:
		return ERR_ACCOUNT_NOT_FOUND
	case ERR_ACCOUNT_ALREADY_CREATED.Id:
		return ERR_ACCOUNT_ALREADY_CREATED
	case ERR_INSUFFICIENT_FUND.Id:
		return ERR_INSUFFICIENT_FUND
	case ERR_OPERATION_NOT_PERMITTED.Id:
		return ERR_OPERATION_NOT_PERMITTED
	case ERR_INVALID_OTP_TOKEN.Id:
		return ERR_INVALID_OTP_TOKEN
	default:
		return appErr(errId)
	}
}

func appErr(Id uint8) *AppErr {
	return &AppErr{Id, 0, 0, ""}
}

type AppErr struct {
	Id            uint8 `json:"id"`
	AccountId     uint32
	CorrelationId uint64
	Detail        string
}

//request correlation id maybe 0
func (err *AppErr) ToBinary(accountId uint32, correlationId uint64) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(16)
	binary.Write(buffer, binary.BigEndian, msg.ENGINE_MSG_TYPE_ERR_RESP)
	binary.Write(buffer, binary.BigEndian, err.Id)
	binary.Write(buffer, binary.BigEndian, accountId)
	binary.Write(buffer, binary.BigEndian, correlationId)
	return buffer.Bytes()
}

func (objErr *AppErr) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid AppErr message format. Err: %v", e)
		}
	}()
	buffer := bytes.NewBuffer(data[1:])
	binary.Read(buffer, binary.BigEndian, &objErr.Id)
	binary.Read(buffer, binary.BigEndian, &objErr.AccountId)
	binary.Read(buffer, binary.BigEndian, &objErr.CorrelationId)
	return nil
}

func (err *AppErr) GetId() uint8 {
	return err.Id
}

func (err *AppErr) WithDetails(detail string) *AppErr {
	err.Detail = detail
	return err
}
