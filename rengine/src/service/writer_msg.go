package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util/msg"
	"strconv"
)

/*
	Write snapshot to msg broker
*/
type MsgWriterService struct {
	queue chan *MemSnapshot
	log   *flog.Logger
}

func NewMsgWriterService() *MsgWriterService {
	queue := make(chan *MemSnapshot, 256)
	log := ctx.LogManager.NewLogger("msg-publisher", flog.LEVEL_INFO)
	writer := &MsgWriterService{queue, log}
	go writer.writeToDb()
	return writer
}

//publish snapshot data to consumers(order changes, trades, funds ...)
func (writer *MsgWriterService) ProcessSnapshot(snapshot *MemSnapshot) {
	writer.queue <- snapshot
}

func (writer *MsgWriterService) writeToDb() {
	snapshotId := ""
	ok := false
	var err error = nil
	for snapshot := range writer.queue {
		snapshotId = strconv.FormatInt(snapshot.Id, 10)
		writer.log.InfoReqId(snapshotId, snapshot.Symbol)

		for _, order := range snapshot.NewOrderList {
			if order.Type != dao.ORDER_TYPE_MKT {
				if err = apiMainPublisher.PublishMsgAsync(msg.ToBinaryOpenOrderResp(snapshot.InstrumentId, order)); err != nil {
					writer.log.ErrReqId(snapshotId, "snapshot.NewOrderList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
				}
			}
		}
		for _, order := range snapshot.UpdateOrderList {
			if err = apiMainPublisher.PublishMsgAsync(msg.ToBinaryOrderChangesEntity(snapshot.InstrumentId, order)); err != nil {
				writer.log.ErrReqId(snapshotId, "snapshot.UpdateOrderList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
			}
		}
		for _, fund := range snapshot.UpdateFundList {
			if err = apiMainPublisher.PublishMsgAsync(msg.ToBinaryFundCacheModelResp(fund)); err != nil {
				writer.log.ErrReqId(snapshotId, "snapshot.UpdateFundList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
			}
		}
		for _, trade := range snapshot.TradeList {
			if err = apiTradePublisher.PublishMsgAsync(msg.ToBinaryTradeEntityResp(snapshot.InstrumentId, trade)); err != nil {
				writer.log.ErrReqId(snapshotId, "snapshot.UpdateFundList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
			}
		}

		for _, position := range snapshot.NewPositionList {
			if err = apiMainPublisher.PublishMsgAsync(msg.ToBinaryPositionEntity(snapshot.InstrumentId,
				position, msg.ENGINE_MSG_TYPE_OPEN_POSITION)); err != nil {
				writer.log.ErrReqId(snapshotId, "snapshot.NewPositionList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
			}
		}

		for _, position := range snapshot.UpdatePositionList {
			if err = apiMainPublisher.PublishMsgAsync(msg.ToBinaryPositionEntity(snapshot.InstrumentId,
				position, msg.ENGINE_MSG_TYPE_UPDATE_POSITION)); err != nil {
				writer.log.ErrReqId(snapshotId, "snapshot.NewPositionList. apiMainPublisher.PublishMsgAsync. details: "+err.Error())
			}
		}

		writer.log.InfoIdF(snapshotId, "done")
		if err != nil {
			writer.log.ErrReqId(snapshotId, "err: "+err.Error())
			exchangeHalted = true
		}
		if !ok {
			exchangeHalted = true
		}
	}
}
