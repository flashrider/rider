package service

import (
	"fmt"
	"github.com/emirpasic/gods/maps/treemap"
	"github.com/emirpasic/gods/utils"
	"github.com/pquerna/ffjson/ffjson"
	"github.com/stretchr/testify/assert"
	"gitlab.com/flashrider/rider/dao"
	"testing"
)

/**
Artyom Aminov
Test scenario
ASKS:
2 -	250
3 - 200
1 - 150
5 - 100
BIDS:
2 - 90
7 - 80
3 - 70
*/
//Test insert first
func TestCacheSellPutOrder(t *testing.T) {
	cache := NewInstrumentOrderCache(1, "EURUSD", 1, 2, false)
	order1 := &OrderCacheModel{Amount: 5, Price: 100, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ := cache.PutLimitOrder(order1)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order2 := &OrderCacheModel{Amount: 6, Price: 100, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ = cache.PutLimitOrder(order2)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order6 := &OrderCacheModel{Amount: 3, Price: 200, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ = cache.PutLimitOrder(order6)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order3 := &OrderCacheModel{Amount: 1, Price: 150, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ = cache.PutLimitOrder(order3)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order4 := &OrderCacheModel{Amount: 2, Price: 50, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ = cache.PutLimitOrder(order4)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order5 := &OrderCacheModel{Amount: 2, Price: 150, Direction: dao.ORDER_DIRECTION_SELL}
	eventType, index, _ = cache.PutLimitOrder(order5)
	fmt.Println("eventType: ", eventType, "index: ", index)

	assert.Equal(t, 4, len(cache.LimitSellPriceArr), "LimitSellPriceArr")
	assert.Equal(t, uint64(19), cache.sellVolTotal, "sellVolTotal")
	assert.Equal(t, uint64(50), cache.LimitSellPriceArr[0], "price position should be 0")
	assert.Equal(t, uint64(200), cache.LimitSellPriceArr[3], "price position should be 200")
	printState(cache)
}

func TestCacheBuyPutOrder(t *testing.T) {
	cache := NewInstrumentOrderCache(1, "EURUSD", 1, 2, false)
	order1 := &OrderCacheModel{Amount: 3, Price: 70, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ := cache.PutLimitOrder(order1)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order2 := &OrderCacheModel{Amount: 2, Price: 90, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ = cache.PutLimitOrder(order2)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order6 := &OrderCacheModel{Amount: 3, Price: 70, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ = cache.PutLimitOrder(order6)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order3 := &OrderCacheModel{Amount: 2, Price: 90, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ = cache.PutLimitOrder(order3)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order4 := &OrderCacheModel{Amount: 7, Price: 80, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ = cache.PutLimitOrder(order4)
	fmt.Println("eventType: ", eventType, "index: ", index)
	order5 := &OrderCacheModel{Amount: 1, Price: 80, Direction: dao.ORDER_DIRECTION_BUY}
	eventType, index, _ = cache.PutLimitOrder(order5)
	fmt.Println("eventType: ", eventType, "index: ", index)

	assert.Equal(t, 3, len(cache.LimitBuyPriceArr), "LimitBuyPriceArr")
	assert.Equal(t, uint64(18), cache.buyVolTotal, "buyVolTotal")
	assert.Equal(t, uint64(80), cache.LimitBuyPriceArr[1], "price position should be 1")

	printState(cache)
}

//print order cache state
func printState(cache *InstrumentOrderCache) {
	askVolTotal, bidVolTotal := cache.GetVolumeTotal()
	fmt.Println("sellVolTotal: ", askVolTotal, "bidVolTotal: ", bidVolTotal)
	buff, err := ffjson.Marshal(cache.LimitSellPriceArr)
	if err != nil {
		panic(err)
	}
	fmt.Println("askMap: ", string(buff))
	buff, err = ffjson.Marshal(cache.LimitBuyPriceArr)
	if err != nil {
		panic(err)
	}
	fmt.Println("bidMap: ", string(buff))
	fmt.Println("askPriceArr: ", cache.LimitSellPriceArr)
	fmt.Println("bidPriceArr: ", cache.LimitBuyPriceArr)
}

func TestStopSellOrder(t *testing.T) {
	treeMap := treemap.NewWith(utils.UInt64Comparator)
	//treeMap :=
	treeMap.Put(uint64(2), 100)
	treeMap.Put(uint64(1), 100)
	treeMap.Put(uint64(4), 100)
	treeMap.Put(uint64(6), 100)
	treeMap.Put(uint64(7), 100)

	key, value := treeMap.Min()
	fmt.Println("min: ", key, ":", value)
	key, value = treeMap.Max()
	fmt.Println("max:", key, ":", value)
	key, value = treeMap.Ceiling(uint64(6))
	fmt.Println("floor: ", key, value)

	fmt.Println(treeMap)

}

func TestIDNumber(t *testing.T) {
	id := 999*100 + uint64(dao.ORDER_TYPE_LIMIT)*10 + uint64(dao.ORDER_DIRECTION_SELL)
	fmt.Println(id)
	fmt.Println(id / 100)
	data := id % 100
	fmt.Println(data / 10)
	fmt.Println(data % 10)

}
