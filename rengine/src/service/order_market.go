package service

import (
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"strconv"
)

/*
	ExecuteMarketOrder MKT order + matching
*/
type MarketOrderService struct {
	totalValue        uint64
	amount            uint64
	contraValue       uint64
	totalContraValue  uint64
	contraBFund       *dao.FundCacheModel
	contraQFund       *dao.FundCacheModel
	currPriceLevel    *PriceLvlContainer
	contraOrder       *OrderCacheModel
	contraPosition    *PositionCacheModel
	currentOrderIndex uint32
	currEventType     msg.OrderBookPriceIndexEventType
	priceLvlArr       []*PriceLvlContainer
	directionCover    dao.OrderDirection
}

func NewMarketOrderService() *MarketOrderService {
	return &MarketOrderService{}
}

//Execute MKT order
func (service *MarketOrderService) ExecuteMarketOrder(oCtx *OrderCtx) *AppErr {
	if oCtx.openOrderReq.Direction == dao.ORDER_DIRECTION_SELL {
		if oCtx.instrOrderCache.buyVolTotal < oCtx.openOrderReq.Amount {
			return ERR_NOT_ENOUGH_VOL_TO_EXEC_MKT
		}
		service.priceLvlArr = oCtx.instrOrderCache.LimitBuyPriceArr
		service.directionCover = dao.ORDER_DIRECTION_BUY
	} else {
		if oCtx.instrOrderCache.sellVolTotal < oCtx.openOrderReq.Amount {
			return ERR_NOT_ENOUGH_VOL_TO_EXEC_MKT
		}
		service.priceLvlArr = oCtx.instrOrderCache.LimitSellPriceArr
		service.directionCover = dao.ORDER_DIRECTION_SELL
	}
	/*************************************************************************
	check max slippage > 0
	*/
	maLog.InfoIdF(oCtx.orderIdStr, "max slippage: %d", oCtx.openOrderReq.MaxSlippage)
	if oCtx.openOrderReq.MaxSlippage > 0 {
		accumAmount := uint64(0)
		endPrice := uint64(0)
		for _, priceLvl := range service.priceLvlArr {
			//maLog.InfoS(level, price)
			accumAmount += priceLvl.Volume
			if oCtx.openOrderReq.Amount <= accumAmount {
				endPrice = priceLvl.Price
				break
			}
		}
		//check max slippage if present (maxSlip. > 0)
		if endPrice > oCtx.openOrderReq.Price+uint64(oCtx.openOrderReq.MaxSlippage) {
			return ERR_ORDER_MAX_SLIPPAGE_LIMIT
		}
		maLog.InfoIdF(oCtx.orderIdStr, "endPrice: %d", endPrice)
	}

	/*************************************************************************
	check balance and reserve
	*/
	service.totalValue = uint64(0) //sell: amount, buy: amount * price
	if oCtx.openOrderReq.Direction == dao.ORDER_DIRECTION_SELL {
		if oCtx.openOrderReq.Amount > oCtx.takerBFund.Balance-oCtx.takerBFund.Reserved {
			return ERR_INSUFFICIENT_FUND
		}
		oCtx.takerBFund.Balance -= oCtx.openOrderReq.Amount
		maLog.InfoIdF(oCtx.orderIdStr, "takerBFund: %11d -%11d %11d", oCtx.takerBFund.Balance,
			oCtx.openOrderReq.Amount, oCtx.takerBFund.Reserved)
	} else {
		buyAccumAmount := uint64(0)
		for _, priceLvl := range service.priceLvlArr {
			for _, order := range priceLvl.OrderArr {
				if buyAccumAmount+order.AmountRest >= oCtx.openOrderReq.Amount {
					amount := oCtx.openOrderReq.Amount - buyAccumAmount
					service.totalValue += amount * priceLvl.Price / FIN_DECIMAL
					if service.totalValue > oCtx.takerQFund.Balance-oCtx.takerQFund.Reserved {
						return ERR_INSUFFICIENT_FUND
					}
					goto ExitCycle
				} else {
					buyAccumAmount += order.AmountRest
					service.totalValue += order.AmountRest * priceLvl.Price / FIN_DECIMAL
				}
			}
		}
	ExitCycle:
		oCtx.takerQFund.Balance -= service.totalValue
		maLog.InfoIdF(oCtx.orderIdStr, "takerQFund: %11d -%11d %11d", oCtx.takerQFund.Balance, service.totalValue, oCtx.takerQFund.Reserved)
	}

	/*
		Create SL and TP orders, entity for taker
	*/
	if oCtx.openOrderReq.TPPrice != 0 {
		order := orderService.CreateTPOrder(oCtx.takerOrder, oCtx.openOrderReq)
		order.Id = oCtx.instrOrderCache.GetNexOrderId(order.Type, order.Direction)
		maLog.InfoIdF(oCtx.orderIdStr, "TP: %d %d %11d", oCtx.takerOrder.TpOrder.State,
			oCtx.takerOrder.TpOrder.Direction, oCtx.takerOrder.TpOrder.Price)
	}
	if oCtx.openOrderReq.SLPrice != 0 {
		order := orderService.CreateSLOrder(oCtx.takerOrder, oCtx.openOrderReq)
		order.Id = oCtx.instrOrderCache.GetNexOrderId(order.Type, order.Direction)
		maLog.InfoIdF(oCtx.orderIdStr, "SL: %d %d %11d", oCtx.takerOrder.SlOrder.State,
			oCtx.takerOrder.SlOrder.Direction, oCtx.takerOrder.SlOrder.Price)
	}
	//call matching
	return service.Matching(oCtx)
}

/*
	Try execute stop orders after market order
	Call recursively
*/
func (service *MarketOrderService) TryExecuteStopOrders(oCtx *OrderCtx) *AppErr {
	exit := true
	if service.directionCover == dao.ORDER_DIRECTION_SELL {
		limitSellPriceArr := oCtx.instrOrderCache.LimitSellPriceArr
		//changed sell book
		if len(limitSellPriceArr) > 0 {
			//best ask
			ask := limitSellPriceArr[0].Price
			for priceLvl, list := range oCtx.instrOrderCache.StopBuyAskMap {
				if priceLvl <= ask {
					exit = false
					for _, order := range list {
						if appErr := service.ExecuteStopOrders(oCtx, order); appErr != nil {
							return appErr
						}
					}
					delete(oCtx.instrOrderCache.StopBuyAskMap, priceLvl)
				}
			}
		}
	} else {
		limitBuyPriceArr := oCtx.instrOrderCache.LimitBuyPriceArr
		//price down, changed buy book
		if len(limitBuyPriceArr) > 0 {
			bid := limitBuyPriceArr[0].Price
			for priceLvl, list := range oCtx.instrOrderCache.StopSellBidMap {
				if priceLvl >= bid {
					exit = false
					for _, order := range list {
						if appErr := service.ExecuteStopOrders(oCtx, order); appErr != nil {
							return appErr
						}
					}
					delete(oCtx.instrOrderCache.StopSellBidMap, priceLvl)
				}
			}
		}
	}
	if exit {
		return nil
	} else {
		return service.TryExecuteStopOrders(oCtx)
	}
}

func (service *MarketOrderService) ExecuteStopOrders(oCtx *OrderCtx, order *OrderCacheModel) *AppErr {
	oCtx.orderIdStr = strconv.FormatUint(order.Id, 10)
	maLog.InfoIdF(oCtx.orderIdStr, "exec stop: %d %11d %11d", order.Kind, order.Price, order.Amount)
	oCtx.takerOrder = order
	oCtx.orderId = order.Id
	oCtx.takerBFund = fundCache.Get(order.AccountId, oCtx.instrOrderCache.BCurrId)
	oCtx.takerQFund = fundCache.Get(order.AccountId, oCtx.instrOrderCache.QCurrId)

	return service.Matching(oCtx)
}

/*
	Order matching
*/
func (service *MarketOrderService) Matching(oCtx *OrderCtx) *AppErr {
	/*****************************************
	can total vol cover market order ?
	*/
	if oCtx.openOrderReq.Direction == dao.ORDER_DIRECTION_SELL {
		service.priceLvlArr = oCtx.instrOrderCache.LimitBuyPriceArr
		service.directionCover = dao.ORDER_DIRECTION_BUY
	} else {
		service.priceLvlArr = oCtx.instrOrderCache.LimitSellPriceArr
		service.directionCover = dao.ORDER_DIRECTION_SELL
	}

	if oCtx.takerOrder.Kind == dao.ORDER_KIND_SL {
		//instrOrderCache.RemoveStopOrder(contraOrder)
		position := oCtx.instrOrderCache.positionMap[oCtx.takerOrder.EntryOrderId]
		//try remove second order
		if position.TpOrder != nil {
			slEventType, lvl, priceLvlCont := oCtx.instrOrderCache.RemoveLimitOrder(position.TpOrder, service.priceLvlArr)
			event := &msg.OrderBookChangeEvent{InstrumentId: oCtx.instrOrderCache.InstrumentId, Direction: service.directionCover}
			event.MsgSeqNum = oCtx.instrOrderCache.msgSeqNumber
			event.EventType = slEventType
			event.PriceLevel = lvl
			event.Price = position.TpOrder.Price
			event.Volume = priceLvlCont.Volume
			event.OrderCount = uint32(len(priceLvlCont.OrderArr))
			oBookChangesPublisher.PublishMsgAsync(event.ToBinary())
			maLog.DebugIdF(oCtx.orderIdStr, "remove TP: %d %11d %10d", event.Direction, event.Price, event.MsgSeqNum)

			position.TpOrder.State = dao.ORDER_STATE_CANCELLED_IFD_GROUP
		}
	}

	/*************************************************************************
	matching
	*/
	service.totalValue = 0
	for priceLvlIndex := 0; priceLvlIndex < len(service.priceLvlArr); priceLvlIndex++ {
		service.currPriceLevel = service.priceLvlArr[priceLvlIndex]

		maLog.DebugIdF(oCtx.orderIdStr, "priceLvl: %4d %11d %11d", priceLvlIndex, service.currPriceLevel.Price, service.currPriceLevel.Volume)
		for i := 0; i < len(service.currPriceLevel.OrderArr); i++ {
			service.contraOrder = service.currPriceLevel.OrderArr[i]
			service.currentOrderIndex = uint32(i)
			if service.totalValue+service.contraOrder.AmountRest > oCtx.takerOrder.Amount {
				service.amount = oCtx.takerOrder.Amount - service.totalValue
			} else {
				service.amount = service.contraOrder.AmountRest
			}
			service.totalValue += service.amount
			maLog.DebugIdF(oCtx.orderIdStr, "tVal: %11d, amnt: %11d", service.totalValue, service.amount)
			service.contraBFund = fundCache.Get(service.contraOrder.AccountId, oCtx.instrOrderCache.BCurrId)
			service.contraQFund = fundCache.Get(service.contraOrder.AccountId, oCtx.instrOrderCache.QCurrId)
			service.contraValue = service.amount * service.contraOrder.Price / FIN_DECIMAL
			service.totalContraValue += service.contraValue
			//change sender balance, contra agent also
			if oCtx.takerOrder.Direction == dao.ORDER_DIRECTION_SELL {
				oCtx.takerQFund.Balance += service.contraValue
				//contra agent
				service.contraBFund.Balance += service.amount
				service.contraQFund.Balance -= service.contraValue
				service.contraQFund.Reserved -= service.contraValue
			} else {
				oCtx.takerBFund.Balance += service.amount
				service.contraBFund.Balance -= service.amount
				service.contraBFund.Reserved -= service.amount
				service.contraQFund.Balance += service.contraValue
			}
			//decrease rest
			service.contraOrder.AmountRest -= service.amount
			service.currPriceLevel.Volume -= service.amount
			if service.directionCover == dao.ORDER_DIRECTION_SELL {
				oCtx.instrOrderCache.sellVolTotal -= service.amount
			} else {
				oCtx.instrOrderCache.buyVolTotal -= service.amount
			}
			//update position
			if service.contraOrder.Kind != dao.ORDER_KIND_ENTRY {
				service.contraPosition = oCtx.instrOrderCache.positionMap[service.contraOrder.EntryOrderId]
				if service.contraOrder.Direction == dao.ORDER_DIRECTION_SELL {
					service.contraPosition.PL += int64(service.contraValue)
				} else {
					service.contraPosition.PL -= int64(service.contraValue)
				}
			}
			//order touched
			oCtx.instrOrderCache.RemoveExpTimeOrder(service.contraOrder.Id)
			//check contraOrder FILLED or not
			if service.contraOrder.AmountRest == 0 {
				maLog.DebugIdF(oCtx.orderIdStr, "order filled id: %9d amnt: %11d", service.contraOrder.Id, service.contraOrder.Amount)
				//delete order
				service.currEventType = oCtx.instrOrderCache.MarkRemovedLimitOrder(service.currentOrderIndex,
					uint16(priceLvlIndex), service.contraOrder, service.currPriceLevel, service.priceLvlArr)
				if service.currEventType == msg.ORDER_BOOK_PRICE_EVENT_DELETE {
					event := &msg.OrderBookChangeEvent{InstrumentId: oCtx.instrOrderCache.InstrumentId, Direction: service.directionCover}
					event.MsgSeqNum = oCtx.instrOrderCache.msgSeqNumber
					event.EventType = msg.ORDER_BOOK_PRICE_EVENT_DELETE
					event.Price = service.currPriceLevel.Price
					oBookChangesPublisher.PublishMsgAsync(event.ToBinary())
					maLog.DebugIdF(oCtx.orderIdStr, "remove priceLvl: %d %11d %10d", event.Direction, event.Price, event.MsgSeqNum)
				}
				//entry order touched activate TP, SL orders
				if service.contraOrder.Kind == dao.ORDER_KIND_ENTRY {
					//open position
					oCtx.memSnp.NewPosition(service.openPosition(oCtx.orderIdStr, oCtx.instrOrderCache, service.contraOrder, 0))
					if service.contraOrder.TpOrder != nil {
						service.contraOrder.TpOrder.State = dao.ORDER_STATE_PLACED
						oCtx.memSnp.UpdateOrder(orderService.ToOrderChangeEntity(service.contraOrder.TpOrder))
						orderService.PutLimitOrder(oCtx.orderIdStr, oCtx.instrOrderCache, service.contraOrder.TpOrder)
					}
					if service.contraOrder.SlOrder != nil {
						service.contraOrder.SlOrder.State = dao.ORDER_STATE_PLACED
						oCtx.memSnp.UpdateOrder(orderService.ToOrderChangeEntity(service.contraOrder.SlOrder))
						oCtx.instrOrderCache.PutStopOrder(service.contraOrder.SlOrder)
					}
					//reserve money for TP/SL
					if service.contraOrder.TpOrder != nil || service.contraOrder.SlOrder != nil {
						if service.contraOrder.Direction == dao.ORDER_DIRECTION_SELL {
							if service.contraOrder.TpOrder == nil {
								service.contraQFund.Reserved += service.contraOrder.Amount * service.contraOrder.SlOrder.Price / FIN_DECIMAL
							} else {
								service.contraQFund.Reserved += service.contraOrder.Amount * service.contraOrder.TpOrder.Price / FIN_DECIMAL
							}
						} else {
							service.contraBFund.Reserved += service.contraOrder.Amount
						}
					}
				}
				//order limit but TP
				if service.contraOrder.Kind == dao.ORDER_KIND_TP {
					position := oCtx.instrOrderCache.positionMap[service.contraOrder.EntryOrderId]
					//try remove second order group SL/TP
					slOrder := position.SlORder
					if slOrder != nil {
						maLog.DebugIdF(oCtx.orderIdStr, "CANCELLED_IFD_GROUP SL  id: %d10", slOrder.Id)
						slOrder.State = dao.ORDER_STATE_CANCELLED_IFD_GROUP
						oCtx.memSnp.UpdateOrder(orderService.ToOrderChangeEntity(slOrder))
						oCtx.instrOrderCache.RemoveStopOrder(slOrder)
					}
					oCtx.memSnp.UpdatePosition(service.closePosition(oCtx.orderIdStr, oCtx.instrOrderCache,
						service.contraOrder.EntryOrderId, service.contraOrder, service.currPriceLevel))
				} // else if service.contraOrder.TpOrder == nil && service.contraOrder.SlOrder == nil {
				//oCtx.memSnp.UpdatePosition(service.closePosition(oCtx.orderIdStr, oCtx.instrOrderCache, service.contraOrder.Id,
				//	service.contraOrder, service.currPriceLevel))
				//}
				service.contraOrder.State = dao.ORDER_STATE_FILLED
			} else {
				service.currEventType = msg.ORDER_BOOK_PRICE_EVENT_CHANGE
				service.contraOrder.State = dao.ORDER_STATE_PARTIALLY_FILLED
				if service.contraOrder.Kind != dao.ORDER_KIND_ENTRY {
					oCtx.memSnp.UpdatePosition(service.updatePosition(oCtx.orderIdStr, service.contraOrder.EntryOrderId, service.contraPosition))
				}
			}
			//update contra agent fund
			oCtx.memSnp.UpdateFund(&(*service.contraBFund))
			oCtx.memSnp.UpdateFund(&(*service.contraQFund))
			//update order
			oCtx.memSnp.UpdateOrder(orderService.ToOrderChangeEntity(service.contraOrder))
			oCtx.memSnp.NewTrade(service.makeTx(oCtx.takerOrder, service.contraOrder, service.amount))
			//stop process
			if oCtx.takerOrder.Amount == service.totalValue {
				oCtx.takerOrder.State = dao.ORDER_STATE_FILLED
				maLog.DebugIdF(oCtx.orderIdStr, "done: %11d", service.totalValue)
				goto ExitMatch
			}
		}
	}

ExitMatch:
	//cut from currentOrderIndex till tail
	if service.currEventType != msg.ORDER_BOOK_PRICE_EVENT_DELETE {
		if service.contraOrder.AmountRest == 0 {
			//last order from order price level executed
			service.currPriceLevel.OrderArr = service.currPriceLevel.OrderArr[service.currentOrderIndex+1:]
		} else {
			service.currPriceLevel.OrderArr = service.currPriceLevel.OrderArr[service.currentOrderIndex:]
		}
		event := &msg.OrderBookChangeEvent{InstrumentId: oCtx.instrOrderCache.InstrumentId, Direction: service.directionCover}
		event.MsgSeqNum = oCtx.instrOrderCache.msgSeqNumber
		event.EventType = msg.ORDER_BOOK_PRICE_EVENT_CHANGE
		event.Price = service.currPriceLevel.Price
		event.Volume = service.currPriceLevel.Volume
		event.OrderCount = uint32(len(service.currPriceLevel.OrderArr))
		oBookChangesPublisher.PublishMsgAsync(event.ToBinary())
		maLog.DebugIdF(oCtx.orderIdStr, "upd priceLvl: %d %11d %10d %10d", event.Direction, event.Price, event.Volume, event.MsgSeqNum)
	}

	//SL already has an open position
	if oCtx.takerOrder.Kind != dao.ORDER_KIND_SL {
		//open position
		oCtx.memSnp.NewPosition(service.openPosition(oCtx.orderIdStr, oCtx.instrOrderCache, oCtx.takerOrder, service.totalContraValue))
	}

	//order doesn't have TP or SL orders,  order close position by self
	//if oCtx.takerOrder.TpOrder == nil && oCtx.takerOrder.SlOrder == nil {
	//	oCtx.memSnp.UpdatePosition(service.closePosition(oCtx.orderIdStr, oCtx.instrOrderCache, oCtx.takerOrder.Id, oCtx.takerOrder,
	//		service.currPriceLevel))
	//} else {
	//set user take profit order ?
	if oCtx.takerOrder.TpOrder != nil {
		orderService.PutLimitOrder(oCtx.orderIdStr, oCtx.instrOrderCache, oCtx.takerOrder.TpOrder)
		oCtx.memSnp.NewOrder(orderService.CreateTPEntity(oCtx.takerOrder.TpOrder))
	}
	//set user stop loss order ?
	if oCtx.takerOrder.SlOrder != nil {
		oCtx.instrOrderCache.PutStopOrder(oCtx.takerOrder.SlOrder)
		oCtx.memSnp.NewOrder(orderService.CreateSLEntity(oCtx.takerOrder.SlOrder))
	}
	if oCtx.takerOrder.TpOrder != nil || oCtx.takerOrder.SlOrder != nil {
		//reserve money for TP/SL
		if oCtx.takerOrder.Direction == dao.ORDER_DIRECTION_SELL {
			oCtx.takerQFund.Reserved += oCtx.takerOrder.Amount * oCtx.takerOrder.Price / FIN_DECIMAL
		} else {
			oCtx.takerBFund.Reserved += oCtx.takerOrder.Amount
		}
	}
	//MK
	if oCtx.takerOrder.Type == dao.ORDER_TYPE_MKT {
		//create new order
		oCtx.memSnp.NewOrder(orderService.CreateEntryOrderEntity(oCtx.takerOrder, true))
	} else {
		//SL, change order
		oCtx.memSnp.UpdateOrder(orderService.ToOrderChangeEntity(oCtx.takerOrder))
	}

	//update fund of MKT order sender
	oCtx.memSnp.UpdateFund(&(*oCtx.takerBFund))
	oCtx.memSnp.UpdateFund(&(*oCtx.takerQFund))

	return nil
}

func (service *MarketOrderService) makeTx(marketOrder, contraAgentOrder *OrderCacheModel,
	amount uint64) *dao.TradeEntity {
	trade := &dao.TradeEntity{}
	trade.Direction = marketOrder.Direction
	trade.Quantity = amount
	trade.Price = contraAgentOrder.Price
	trade.SettleDate = util.NowMk()
	if marketOrder.Direction == dao.ORDER_DIRECTION_SELL {
		trade.AskOrderId = marketOrder.Id
		trade.BidOrderId = contraAgentOrder.Id
	} else {
		trade.AskOrderId = contraAgentOrder.Id
		trade.BidOrderId = marketOrder.Id
	}
	return trade
}

func (service *MarketOrderService) openPosition(orderIdStr string, instrOrderCache *InstrumentOrderCache,
	entryOrder *OrderCacheModel, totalVal uint64) *dao.PositionEntity {
	positionCache := &PositionCacheModel{}
	instrOrderCache.positionMap[entryOrder.Id] = positionCache
	if entryOrder.TpOrder != nil {
		positionCache.TpOrder = entryOrder.TpOrder
	}
	if entryOrder.SlOrder != nil {
		positionCache.SlORder = entryOrder.SlOrder
	}
	position := &dao.PositionEntity{}
	position.State = dao.POSITION_STATE_OPEN
	position.Direction = entryOrder.Direction
	position.Amount = entryOrder.Amount
	position.Id = entryOrder.Id
	position.AccountId = entryOrder.AccountId
	position.OpenTime = util.NowMk()
	position.OpenPrice = entryOrder.Price

	if entryOrder.Type == dao.ORDER_TYPE_LIMIT {
		totalVal = entryOrder.Amount * entryOrder.Price / FIN_DECIMAL
	}
	if entryOrder.Direction == dao.ORDER_DIRECTION_SELL {
		position.PL = int64(totalVal)
		positionCache.PL = int64(totalVal)
	} else {
		position.PL -= int64(totalVal)
		positionCache.PL -= int64(totalVal)
	}
	maLog.InfoIdF(orderIdStr, "opn pos: %11d %11d", entryOrder.Id, positionCache.PL)
	return position
}

func (service *MarketOrderService) updatePosition(orderIdStr string, positionId uint64,
	positionCache *PositionCacheModel) *dao.PositionEntity {
	maLog.InfoIdF(orderIdStr, "upd pos: %11d %11d", positionId, positionCache.PL)
	position := &dao.PositionEntity{}
	position.Id = positionId
	position.State = dao.POSITION_STATE_OPEN
	position.PL = positionCache.PL

	return position
}

//currPriceLevel used for STOP/MK/SL
func (service *MarketOrderService) closePosition(orderIdStr string, instrOrderCache *InstrumentOrderCache,
	positionId uint64, order *OrderCacheModel, currPriceLevel *PriceLvlContainer) *dao.PositionEntity {

	positionCache := instrOrderCache.positionMap[positionId]
	maLog.InfoIdF(orderIdStr, "cls pos: %11d %11d", positionId, positionCache.PL)
	position := &dao.PositionEntity{}
	position.State = dao.POSITION_STATE_CLOSED
	position.Id = positionId
	position.AccountId = order.AccountId
	position.CloseOrderId = order.Id
	position.ClosePrice = currPriceLevel.Price
	position.SettleDate = util.NowMk()
	position.PL = positionCache.PL
	//commission TODO
	position.Commission = 0
	delete(instrOrderCache.positionMap, positionId)
	return position
}

//TODO
func (service *MarketOrderService) calcFee() {

}
