package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"github.com/robfig/cron"
	"gitlab.com/flashrider/rider/dao"
	"sync"
	"time"
)

/*
	Write snapshot to msg broker
*/
type OhlcWriterService struct {
	queue       chan *OrderCtx
	log         *flog.Logger
	ohlcAskMap  map[uint16]map[dao.ResolutionType]*dao.OhlcEntity
	ohlcBidMap  map[uint16]map[dao.ResolutionType]*dao.OhlcEntity
	newBarMutex *sync.Mutex
}

func NewOhlcWriterService(loadFromDb bool) *OhlcWriterService {
	queue := make(chan *OrderCtx, 256)
	log := ctx.LogManager.NewLogger("ohlc", flog.LEVEL_INFO)
	ohlcAskMap := make(map[uint16]map[dao.ResolutionType]*dao.OhlcEntity, instrumentCache.GetCacheSize())
	ohlcBidMap := make(map[uint16]map[dao.ResolutionType]*dao.OhlcEntity, instrumentCache.GetCacheSize())
	newBarMutex := &sync.Mutex{}
	writer := &OhlcWriterService{queue, log, ohlcAskMap, ohlcBidMap, newBarMutex}

	var err error = nil
	for _, instr := range instrumentCache.GetCache() {
		//each instrument has personal resolution
		resolutionAskMap := make(map[dao.ResolutionType]*dao.OhlcEntity)
		resolutionBidMap := make(map[dao.ResolutionType]*dao.OhlcEntity)
		ohlcAskMap[instr.Id] = resolutionAskMap
		ohlcBidMap[instr.Id] = resolutionBidMap

		//load last ohlc value
		if loadFromDb {
			for _, resolution := range dao.RESOLUTION_LIST {
				if resolutionAskMap[resolution], err = ohlcAskDao.GetLast(instr.Symbol, resolution); err == nil {
					log.InfoIdF(instr.Symbol, "resolution: %d ohlc: %s", resolution, fmt.Sprint(resolutionAskMap[resolution]))
				} else {
					log.ErrReqId(instr.Symbol, "err read resolution: "+err.Error())
					panic(err)
				}
				if resolutionBidMap[resolution], err = ohlcBidDao.GetLast(instr.Symbol, resolution); err == nil {
					log.InfoIdF(instr.Symbol, "resolution: %d ohlc: %s", resolution, fmt.Sprint(resolutionBidMap[resolution]))
				} else {
					log.ErrReqId(instr.Symbol, "err read resolution: "+err.Error())
					panic(err)
				}
			}
		} else {
			for _, resolution := range dao.RESOLUTION_LIST {
				resolutionAskMap[resolution] = &dao.OhlcEntity{}
			}
			for _, resolution := range dao.RESOLUTION_LIST {
				resolutionBidMap[resolution] = &dao.OhlcEntity{}
			}
		}
	}
	c := cron.New()
	//every 2 seconds
	c.AddFunc("0/2 * * * * *", writer.NewBarGenerator)
	//TOOD
	//c.Start()
	go writer.updateOHLC()

	return writer
}

//publish snapshot data to consumers(order changes, trades, funds ...)
func (writer *OhlcWriterService) ProcessSnapshot(snapshot *OrderCtx) {
	writer.queue <- snapshot
}

func (service *OhlcWriterService) updateOHLC() {
	for snapshot := range service.queue {

		bestAskPrice := snapshot.bestAsk
		bestBidPrice := snapshot.bestBid
		//direction sell or buy
		resolutionAskMap := service.ohlcAskMap[snapshot.instrOrderCache.InstrumentId]
		//bestPrice =
		resolutionBidMap := service.ohlcBidMap[snapshot.instrOrderCache.InstrumentId]
		updated := false
		//lock change ohlc entity object
		service.newBarMutex.Lock()
		for _, ohlc := range resolutionAskMap {
			if ohlc.High < bestAskPrice {
				ohlc.High = bestAskPrice
				updated = true
			} else if ohlc.Low > bestAskPrice {
				ohlc.Low = bestAskPrice
				updated = true
			}
			if updated {
				//update in db HL
			}
		}
		for _, ohlc := range resolutionBidMap {
			if ohlc.High < bestBidPrice {
				ohlc.High = bestBidPrice
				updated = true
			} else if ohlc.Low > bestBidPrice {
				ohlc.Low = bestBidPrice
				updated = true
			}
			if updated {
				//update in db HL
			}
		}
		for _, trade := range snapshot.memSnp.TradeList {
			for _, ohlc := range resolutionAskMap {
				ohlc.Volume += trade.Quantity
			}
			for _, ohlc := range resolutionBidMap {
				ohlc.Volume += trade.Quantity
			}
			//update in db volume
		}
		service.newBarMutex.Unlock()
	}
}

//should called every 2 sec
func (service *OhlcWriterService) NewBarGenerator() {
	now := time.Now().UTC()

	service.log.Info("start: ")
	service.newBarMutex.Lock()

	//TODO
	if now.Second()%2 == 0 {

	}

	if now.Hour()%2 == 0 {

	}

	service.newBarMutex.Unlock()
	service.log.Info("end")
}
