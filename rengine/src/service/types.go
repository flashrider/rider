package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util/msg"
	"time"
)

/**
Application context and config
*/
type AppContext struct {
	Env struct {
		Name  string `yaml:"name"`
		Nosql struct {
			Server  string `yaml:"server"`
			User    string `yaml:"user"`
			Pass    string `yaml:"pass"`
			Buffers uint32 `yaml:"buffers"`
		}
	}
	App struct {
		Version              string `yaml:"version"` //app version
		Name                 string `yaml:"name"`
		MatchingLogThreshold rune   `yaml:"matchingLogThreshold"`
		Api                  struct {
			Binary struct {
				Interface      string `yaml:"interface"`
				BrokerLogLevel string `yaml:"brokerLogLevel"`
			}
		}
		Logging struct {
			Console     bool   `yaml:"console"`
			Threshold   rune   `yaml:threshold`
			MaxFileSize uint64 `yaml:maxfilesize`
		}
	}

	LogManager *flog.LogManager
	Logger     *flog.Logger //main logger

	AppFolder string
	StartTime uint32 //when app started
}

//Matching context
type OrderCtx struct {
	orderId         uint64
	orderIdStr      string
	instrOrderCache *InstrumentOrderCache
	memSnp          *MemSnapshot
	takerBFund      *dao.FundCacheModel
	takerQFund      *dao.FundCacheModel
	takerOrder      *OrderCacheModel
	openOrderReq    *msg.OpenOrderReq
	bestAsk         uint64
	bestBid         uint64
	closePosition   *PositionCacheModel //close position
}

//finalize changes
func (oCtx *OrderCtx) Commit() {
	if len(oCtx.instrOrderCache.LimitSellPriceArr) != 0 {
		oCtx.bestAsk = oCtx.instrOrderCache.LimitSellPriceArr[0].Price
	}
	if len(oCtx.instrOrderCache.LimitBuyPriceArr) != 0 {
		oCtx.bestBid = oCtx.instrOrderCache.LimitBuyPriceArr[0].Price
	}
}

/***
Caches
*/
type OrderCacheModel struct {
	Id           uint64
	AccountId    uint32
	Kind         dao.OrderKind
	Type         dao.OrderType
	PriceTrigger dao.OrderPriceTrigger
	Price        uint64
	Amount       uint64
	//filled only for TP and SL orders
	EntryOrderId uint64
	Duration     dao.OrderDuration
	AmountRest   uint64
	State        dao.OrderState
	Direction    dao.OrderDirection
	ExpTime      uint64
	//filled only for entry order (IFD), may be nil if user not spec.
	TpOrder *OrderCacheModel
	//filled only for entry order (IFD), may be nil if user not spec.
	SlOrder *OrderCacheModel
}

func (order *OrderCacheModel) FromOpenOrderMsg(msg *msg.OpenOrderReq) {
	order.AccountId = msg.AccountId
	order.Type = msg.Type
	order.Kind = dao.ORDER_KIND_ENTRY
	order.Price = msg.Price
	order.PriceTrigger = msg.PriceTrigger
	order.Amount = msg.Amount
	order.Duration = msg.Duration
	order.AmountRest = msg.Amount
	order.Direction = msg.Direction
	order.ExpTime = msg.Time
}

func (order *OrderCacheModel) ToOrderEntity() *dao.OrderEntity {
	entity := &dao.OrderEntity{}
	entity.Id = order.Id
	entity.AccountId = order.AccountId
	entity.Kind = order.Kind
	entity.Type = order.Type
	entity.PriceTrigger = order.PriceTrigger
	entity.Price = order.Price
	entity.Amount = order.Amount
	entity.EntryOrderId = order.EntryOrderId
	entity.Duration = order.Duration
	entity.AmountRest = order.AmountRest
	entity.State = order.State
	entity.Direction = order.Direction
	return entity
}

//Price level container
type PriceLvlContainer struct {
	OrderArr []*OrderCacheModel //FILO
	Volume   uint64             //total volume by price from all orders
	Price    uint64
}

type PositionCacheModel struct {
	TpOrder   *OrderCacheModel
	SlORder   *OrderCacheModel
	PL        int64 //profit(+) or loss(-). Always in quote currency
	Amount    uint64
	Direction dao.OrderDirection //position direction
	AccountId uint32             //owner
}

/*
	Memory snapshot
*/
type MemSnapshot struct {
	Id                 int64                     //snapshot uniq id
	InstrumentId       uint16                    //instrument id
	Symbol             string                    //instrument symbol
	MemSeqNumber       uint64                    //last mem seq number
	NewOrderList       []*dao.OrderEntity        //new orders
	UpdateOrderList    []*dao.OrderChangesEntity //update orders
	UpdateFundList     []*dao.FundCacheModel     //update fund
	TradeList          []*dao.TradeEntity        //matching result
	NewPositionList    []*dao.PositionEntity
	UpdatePositionList []*dao.PositionEntity
	//TODO extTx
}

func NewMemSnapshot(instrumentOrderCache *InstrumentOrderCache) *MemSnapshot {
	id := time.Now().UnixNano()
	newOrderList := make([]*dao.OrderEntity, 0, 5) //new order + stop order!
	updateOrderList := make([]*dao.OrderChangesEntity, 0, 3000)
	fundList := make([]*dao.FundCacheModel, 0, 5000)
	tradeList := make([]*dao.TradeEntity, 0, 2000)
	newPositionList := make([]*dao.PositionEntity, 0, 3000)
	updatePositionList := make([]*dao.PositionEntity, 0, 3000)
	return &MemSnapshot{id, instrumentOrderCache.InstrumentId, instrumentOrderCache.Symbol, 0, newOrderList, updateOrderList, fundList, tradeList,
		newPositionList, updatePositionList}
}

func (m *MemSnapshot) ToString() string {
	return fmt.Sprintf("orders: %d/%4d funds: %3d trades: %3d positions: %3d/%3d",
		len(m.NewOrderList), len(m.UpdateOrderList), len(m.UpdateFundList), len(m.TradeList),
		len(m.NewPositionList), len(m.UpdatePositionList))
}
func (m *MemSnapshot) NewOrder(order *dao.OrderEntity) {
	m.NewOrderList = append(m.NewOrderList, order)
}
func (m *MemSnapshot) UpdateOrder(order *dao.OrderChangesEntity) {
	m.UpdateOrderList = append(m.UpdateOrderList, order)
}
func (m *MemSnapshot) UpdateFund(fund *dao.FundCacheModel) {
	m.UpdateFundList = append(m.UpdateFundList, fund)
}
func (m *MemSnapshot) NewTrade(trade *dao.TradeEntity) {
	m.TradeList = append(m.TradeList, trade)
}
func (m *MemSnapshot) NewPosition(position *dao.PositionEntity) {
	m.NewPositionList = append(m.NewPositionList, position)
}
func (m *MemSnapshot) UpdatePosition(position *dao.PositionEntity) {
	m.UpdatePositionList = append(m.UpdatePositionList, position)
}
