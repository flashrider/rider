package service

import (
	"fmt"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/util"
	"gitlab.com/flashrider/rider/util/msg"
	"gitlab.com/flashrider/rider/util/service"
	"math/rand"
	"testing"
	"time"
)

func TestLimitOrder(t *testing.T) {
	publisher := service.NewMsgPublisher(msg.TOPIC_ENGINE_MAIN_MSG_REQ, "127.0.0.1:4161")

	queue := make(chan []byte, 64)
	listener := service.NewMsgListener(msg.TOPIC_ENGINE_MAIN_MSG_RESP, queue, "127.0.0.1:4161")
	go func() {
		for message := range queue {
			fmt.Println("message", message)
			msgType := msg.EngineMessageType(message[0])

			if msgType == 0 {
				appErr := &AppErr{}
				err := appErr.FromBinary(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("err", appErr)
			} else if msgType == msg.ENGINE_MSG_TYPE_OPEN_ORDER {
				_, order, err := msg.FromBinaryOpenOrderResp(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("resp", util.ToJson(order))
			} else if msgType == msg.ENGINE_MSG_TYPE_UPDATE_FUND {
				fund, err := msg.FromBinaryFundCacheModelResp(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("resp", util.ToJson(fund))
			}
		}
	}()

	req := msg.OpenOrderReq{}
	req.InstrumentId = 1
	req.AccountId = 1
	req.CorrelationId = util.NowMk()
	req.Type = dao.ORDER_TYPE_LIMIT
	req.Direction = dao.ORDER_DIRECTION_BUY
	req.Amount = 10
	req.Price = 350000000
	req.PriceTrigger = dao.ORDER_PRICE_TRIGGER_ASK_G
	req.Duration = dao.ORDER_DURATION_GTC
	req.Time = 0
	req.TPPrice = req.Price + 1000
	req.TPPriceTrigger = dao.ORDER_PRICE_TRIGGER_ASK_G
	req.SLPrice = req.Price - 1000
	req.SLPriceTrigger = dao.ORDER_PRICE_TRIGGER_BID_L

	err := publisher.PublishMsgAsync(req.ToBinary())
	if err != nil {
		panic(err)
	}

	time.Sleep(2 * time.Second)
	listener.Shutdown()
	publisher.Shutdown()
}

func TestLimitOrderBulk(t *testing.T) {
	publisher := service.NewMsgPublisher(msg.TOPIC_ENGINE_MAIN_MSG_REQ, "127.0.0.1:4161")

	queue := make(chan []byte, 128)
	listener := service.NewMsgListener(msg.TOPIC_ENGINE_MAIN_MSG_RESP, queue, "127.0.0.1:4161")
	go func() {
		for message := range queue {
			fmt.Println("message", message)
			msgType := msg.EngineMessageType(message[0])
			if msgType == 0 {
				appErr := &AppErr{}
				err := appErr.FromBinary(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("err", appErr)
			} else if msgType == msg.ENGINE_MSG_TYPE_OPEN_ORDER {
				_, order, err := msg.FromBinaryOpenOrderResp(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("resp", util.ToJson(order))
			} else if msgType == msg.ENGINE_MSG_TYPE_UPDATE_FUND {
				fund, err := msg.FromBinaryFundCacheModelResp(message)
				if err != nil {
					panic(err)
				}
				fmt.Println("resp", util.ToJson(fund))
			}
		}
	}()

	for instrId := 1; instrId < 61; instrId++ {
		flex := instrId*10 + rand.Intn(10000000)
		bestSellPrice := uint64(310000004 - flex)
		bestBuyPrice := uint64(310000000 - flex)

		for j := 0; j < rand.Intn(60); j++ {
			PublishDataTest(publisher, uint16(instrId), bestSellPrice, uint64(j), dao.ORDER_DIRECTION_SELL)
			PublishDataTest(publisher, uint16(instrId), bestBuyPrice, uint64(j), dao.ORDER_DIRECTION_BUY)
		}
	}

	time.Sleep(4 * time.Second)
	listener.Shutdown()
	publisher.Shutdown()
}

func PublishDataTest(publisher *service.MsgPublisher, instrumentId uint16, price, i uint64,
	direction dao.OrderDirection) {
	req := msg.OpenOrderReq{}
	req.InstrumentId = instrumentId
	req.AccountId = 1
	req.CorrelationId = util.NowMk()
	req.Type = dao.ORDER_TYPE_LIMIT
	req.Direction = direction
	//min 0.01 = 1000
	req.Amount = 10000 + uint64(rand.Intn(1000))
	if direction == dao.ORDER_DIRECTION_SELL {
		req.Price = price + i*10
	} else {
		req.Price = price - i*10
	}

	req.PriceTrigger = dao.ORDER_PRICE_TRIGGER_ASK_G
	req.Duration = dao.ORDER_DURATION_GTC
	req.Time = 0
	req.TPPrice = req.Price + uint64(i)
	req.TPPriceTrigger = dao.ORDER_PRICE_TRIGGER_ASK_G
	req.SLPrice = req.Price - uint64(i)
	req.SLPriceTrigger = dao.ORDER_PRICE_TRIGGER_BID_L

	err := publisher.PublishMsgAsync(req.ToBinary())
	if err != nil {
		panic(err)
	}
}

func TestMath(t *testing.T) {
	price := uint64(350000900)
	amount := uint64(1000)
	quote := price * amount
	fmt.Println(quote)
	fmt.Println(quote / 100000)

}
