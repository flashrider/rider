package main

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"gitlab.com/flashrider/rider/rengine/src/service"
	"gitlab.com/flashrider/rider/util"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"
)

const (
	APP_CONFIG_FILE_NAME = "config.yml"
	APP_MAIN_LOGGER      = "main"
)

func main() {
	context := NewContext(os.Args)
	sigs := make(chan os.Signal, 1)
	if runtime.GOOS != "windows" {
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	}
	createServices(context)
	sig := <-sigs
	fmt.Println("shutdown. Signal: ", sig)
	close(sigs)
	destroy(context)
}

func NewContext(processArgs []string) *service.AppContext {
	util.LogI("Start create app context")

	workPath, err := os.Getwd()
	if err != nil {
		panic("can't get work path")
	}

	util.LogI("work path: " + workPath)

	//read appContext
	configFile, err := os.Open(filepath.Join(workPath, APP_CONFIG_FILE_NAME))
	if err != nil {
		panic("application appContext file doesn't exist !")
	}
	defer configFile.Close()
	fileContent, err := ioutil.ReadAll(configFile)
	if err != nil {
		panic(err)
	}
	appContext := &service.AppContext{}
	err = yaml.Unmarshal(fileContent, &appContext)
	if err != nil {
		panic("invalid appContext file! " + err.Error())
	}
	//init logger manager
	util.LogIs("logger console:", appContext.App.Logging.Console, " MaxFileSize:", appContext.App.Logging.MaxFileSize)
	var logManager *flog.LogManager = nil
	if appContext.App.Logging.Console {
		logManager = flog.NewLogManagerConsole()
	} else {
		logManager = flog.NewLogManagerFile(workPath, appContext.App.Logging.MaxFileSize)
	}
	util.LogIs("logThreshold: ", appContext.App.Logging.Threshold)
	//create main logger
	logger := logManager.NewLogger(APP_MAIN_LOGGER, appContext.App.Logging.Threshold)
	appContext.LogManager = logManager
	appContext.Logger = logger
	appContext.AppFolder = workPath

	return appContext
}

/*
	Setup modules
*/
func createServices(ctx *service.AppContext) {
	log := ctx.Logger
	log.Info("APP name: " + ctx.App.Name)
	log.Info("ENV: " + ctx.Env.Name + "/" + ctx.App.Version)
	log.Info("start create services")
	/*
		NOSQL DB
	*/
	log.Info("start init Nosql client")
	dao.NewTarantoolClient(ctx.Env.Nosql.Server, ctx.Env.Nosql.User, ctx.Env.Nosql.Pass, ctx.Env.Nosql.Buffers)
	log.Info("end init Nosql client")
	//init services
	service.InitServices(ctx)

	ctx.StartTime = uint32(time.Now().Unix())
}

func destroy(context *service.AppContext) {
	//get main logging
	log := context.Logger
	log.Info("start destroy application")

	//binary.ShutdownBinaryApi()
	service.DestroyServices()

	time.Sleep(time.Millisecond * 500)
	err := dao.Close()
	if err != nil {
		panic(err)
	}
	log.Info("end destroy application")
	util.LogI("process shutdown")
}
