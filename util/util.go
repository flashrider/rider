package util

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"github.com/pquerna/ffjson/ffjson"
)

func Uint64ToBytes(number uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, number)
	return b
}

func FromJson(jsonData []byte, obj interface{}) error {
	// Decode
	return ffjson.Unmarshal(jsonData, obj)
}

//panic if error
func ToJson(obj interface{}) string {
	// Encode
	buf, _ := ffjson.Marshal(obj)
	return string(buf)
}

func ToJsonBin(obj interface{}) []byte {
	// Encode
	buf, _ := ffjson.Marshal(obj)
	return buf
}

//GOB format
func ToBinary(obj interface{}) []byte {
	buff := &bytes.Buffer{}
	enc := gob.NewEncoder(buff)
	enc.Encode(obj)
	return buff.Bytes()
}

//GOB format
func FromBinary(byteArr []byte, to interface{}) {
	dec := gob.NewDecoder(bytes.NewBuffer(byteArr))
	dec.Decode(to)
}
