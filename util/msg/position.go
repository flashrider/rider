package msg

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/flashrider/rider/dao"
)

/*
	Trades
*/
func ToBinaryPositionEntity(instrumentId uint16, entity *dao.PositionEntity, messageType EngineMessageType) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(50)
	binary.Write(buffer, binary.BigEndian, messageType)
	binary.Write(buffer, binary.BigEndian, instrumentId)
	binary.Write(buffer, binary.BigEndian, entity.Id)
	binary.Write(buffer, binary.BigEndian, entity.AccountId)
	binary.Write(buffer, binary.BigEndian, entity.OpenTime)
	binary.Write(buffer, binary.BigEndian, entity.Direction)
	binary.Write(buffer, binary.BigEndian, entity.SettleDate)
	binary.Write(buffer, binary.BigEndian, entity.State)
	binary.Write(buffer, binary.BigEndian, entity.Amount)
	binary.Write(buffer, binary.BigEndian, entity.OpenPrice)
	binary.Write(buffer, binary.BigEndian, entity.ClosePrice)
	binary.Write(buffer, binary.BigEndian, entity.CloseOrderId)
	binary.Write(buffer, binary.BigEndian, entity.PL)
	return buffer.Bytes()
}

func FromBinaryPositionEntity(data []byte) (instrumentId uint16, entity *dao.PositionEntity, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid ENGINE_MSG_TYPE_UPDATE_FUND message format. Err: %v", e)
		}
	}()
	instrumentId = uint16(0)
	//remove msg type id
	entity = &dao.PositionEntity{}
	buffer := bytes.NewBuffer(data[1:])
	binary.Read(buffer, binary.BigEndian, &instrumentId)
	binary.Read(buffer, binary.BigEndian, &entity.Id)
	binary.Read(buffer, binary.BigEndian, &entity.AccountId)
	binary.Read(buffer, binary.BigEndian, &entity.OpenTime)
	binary.Read(buffer, binary.BigEndian, &entity.Direction)
	binary.Read(buffer, binary.BigEndian, &entity.SettleDate)
	binary.Read(buffer, binary.BigEndian, &entity.State)
	binary.Read(buffer, binary.BigEndian, &entity.Amount)
	binary.Read(buffer, binary.BigEndian, &entity.OpenPrice)
	binary.Read(buffer, binary.BigEndian, &entity.ClosePrice)
	binary.Read(buffer, binary.BigEndian, &entity.CloseOrderId)
	binary.Read(buffer, binary.BigEndian, &entity.PL)
	return instrumentId, entity, err
}
