package msg

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/flashrider/rider/dao"
)

//See feed: TOPIC_ENGINE_MSG_GET_SNAPSHOT_REQ
type InstrOrderBookSnapshotReq struct {
	InstrumentId uint16
}

func (req *InstrOrderBookSnapshotReq) ToBinary() []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(3)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_GET_ORDER_BOOK_SNAPSHOT)
	binary.Write(buffer, binary.BigEndian, req.InstrumentId)
	return buffer.Bytes()
}

func (req *InstrOrderBookSnapshotReq) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid CancelOrderResp message format. Err: %v", e)
		}
	}()
	if err == nil {
		buffer := bytes.NewBuffer(data[1:])
		binary.Read(buffer, binary.BigEndian, &req.InstrumentId)
	}
	return err
}

//See feed: TOPIC_ENGINE_MSG_GET_SNAPSHOT_RESP
type InstrOrderBookSnapshotResp struct {
	InstrumentId      uint16
	MsgSequenceNumber uint64
	OrderSellArr      [][]uint64 //0 - price, 1 - vol, 2 - orderCount
	OrderBuyArr       [][]uint64 //0 - price, 1 - vol, 2 - orderCount
}

//See feed TOPIC_ENGINE_MSG_ORDER_BOOK_FEED
type OrderBookChangeEvent struct {
	InstrumentId uint16
	Direction    dao.OrderDirection
	MsgSeqNum    uint64
	PriceLevel   uint16
	EventType    OrderBookPriceIndexEventType
	Price        uint64
	Volume       uint64
	OrderCount   uint32
}

func (req *OrderBookChangeEvent) ToBinary() []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(26)
	binary.Write(buffer, binary.BigEndian, req.InstrumentId)
	binary.Write(buffer, binary.BigEndian, req.Direction)
	binary.Write(buffer, binary.BigEndian, req.MsgSeqNum)
	binary.Write(buffer, binary.BigEndian, req.PriceLevel)
	binary.Write(buffer, binary.BigEndian, req.EventType)
	binary.Write(buffer, binary.BigEndian, req.Price)
	binary.Write(buffer, binary.BigEndian, req.Volume)
	binary.Write(buffer, binary.BigEndian, req.OrderCount)
	return buffer.Bytes()
}
func (req *OrderBookChangeEvent) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid PutOrderResp message format. Err: %v", e)
		}
	}()

	buffer := bytes.NewBuffer(data)
	binary.Read(buffer, binary.BigEndian, &req.InstrumentId)
	binary.Read(buffer, binary.BigEndian, &req.Direction)
	binary.Read(buffer, binary.BigEndian, &req.MsgSeqNum)
	binary.Read(buffer, binary.BigEndian, &req.PriceLevel)
	binary.Read(buffer, binary.BigEndian, &req.EventType)
	binary.Read(buffer, binary.BigEndian, &req.Price)
	binary.Read(buffer, binary.BigEndian, &req.Volume)
	binary.Read(buffer, binary.BigEndian, &req.OrderCount)
	return err
}
