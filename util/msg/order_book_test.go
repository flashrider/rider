package msg

import (
	"fmt"
	"gitlab.com/flashrider/rider/util"
	"testing"
)

func TestReadOrderBook(t *testing.T) {
	req := &InstrOrderBookSnapshotReq{InstrumentId: 39}
	bin := util.ToBinary(req)
	reqRestrore := &InstrOrderBookSnapshotReq{}
	util.FromBinary(bin, reqRestrore)
	fmt.Println(reqRestrore)
}
