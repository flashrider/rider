package msg

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"gitlab.com/flashrider/rider/dao"
)

/**
ArtyomAminov
Communication messages
*/

const (
	// Messages length
	OPEN_ORDER_REQ_MSG_LENGTH    = 71
	OPEN_ORDER_RESP_MSG_LENGTH   = 85
	CANCEL_ORDER_REQ_MSG_LENGTH  = 31
	CANCEL_ORDER_RESP_MSG_LENGTH = 15
)

type CommunicationMessage interface {
	ToBinary() []byte
	FromBinary(data []byte) (err error)
}

// put order message
type OpenOrderReq struct {
	AccountId      uint32                `json:"ac"`
	InstrumentId   uint16                `json:"i"`
	CorrelationId  uint64                `json:"ci"` //correlation id
	Price          uint64                `json:"p"`
	Amount         uint64                `json:"a"`
	Type           dao.OrderType         `json:"t"` //order type
	Direction      dao.OrderDirection    `json:"d"`
	PriceTrigger   dao.OrderPriceTrigger `json:"pt"`
	Duration       dao.OrderDuration     `json:"du"`
	Time           uint64                `json:"e"` //exp time, minutes count...
	TPPrice        uint64                `json:"tp"`
	TPPriceTrigger dao.OrderPriceTrigger `json:"tpt"`
	SLPrice        uint64                `json:"sl"`
	SLPriceTrigger dao.OrderPriceTrigger `json:"spt"`
	MaxSlippage    uint16                `json:"msl"`
	PositionId     uint64                `json:"pId"`
}

func (order *OpenOrderReq) ToBinary() []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(OPEN_ORDER_REQ_MSG_LENGTH)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_OPEN_ORDER)
	binary.Write(buffer, binary.BigEndian, order.AccountId)
	binary.Write(buffer, binary.BigEndian, order.InstrumentId)
	binary.Write(buffer, binary.BigEndian, order.Price)
	binary.Write(buffer, binary.BigEndian, order.Amount)
	binary.Write(buffer, binary.BigEndian, order.Type)
	binary.Write(buffer, binary.BigEndian, order.Direction)
	binary.Write(buffer, binary.BigEndian, order.PriceTrigger)
	binary.Write(buffer, binary.BigEndian, order.Duration)
	binary.Write(buffer, binary.BigEndian, order.TPPrice)
	binary.Write(buffer, binary.BigEndian, order.TPPriceTrigger)
	binary.Write(buffer, binary.BigEndian, order.SLPrice)
	binary.Write(buffer, binary.BigEndian, order.SLPriceTrigger)
	binary.Write(buffer, binary.BigEndian, order.Time)
	binary.Write(buffer, binary.BigEndian, order.CorrelationId)
	binary.Write(buffer, binary.BigEndian, order.MaxSlippage)
	binary.Write(buffer, binary.BigEndian, order.PositionId)
	return buffer.Bytes()
}

func (order *OpenOrderReq) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid OpenOrderReq message format. Err: %v", e)
		}
	}()
	err = validateBinary(data, order, OPEN_ORDER_REQ_MSG_LENGTH)
	if err == nil {
		//remove 1 byte - messageType
		buffer := bytes.NewBuffer(data[1:])
		binary.Read(buffer, binary.BigEndian, &order.AccountId)
		binary.Read(buffer, binary.BigEndian, &order.InstrumentId)
		binary.Read(buffer, binary.BigEndian, &order.Price)
		binary.Read(buffer, binary.BigEndian, &order.Amount)
		binary.Read(buffer, binary.BigEndian, &order.Type)
		binary.Read(buffer, binary.BigEndian, &order.Direction)
		binary.Read(buffer, binary.BigEndian, &order.PriceTrigger)
		binary.Read(buffer, binary.BigEndian, &order.Duration)
		binary.Read(buffer, binary.BigEndian, &order.TPPrice)
		binary.Read(buffer, binary.BigEndian, &order.TPPriceTrigger)
		binary.Read(buffer, binary.BigEndian, &order.SLPrice)
		binary.Read(buffer, binary.BigEndian, &order.SLPriceTrigger)
		binary.Read(buffer, binary.BigEndian, &order.Time)
		binary.Read(buffer, binary.BigEndian, &order.CorrelationId)
		binary.Read(buffer, binary.BigEndian, &order.MaxSlippage)
		binary.Read(buffer, binary.BigEndian, &order.PositionId)
	}
	return err
}

func ToBinaryOpenOrderResp(instrumentId uint16, order *dao.OrderEntity) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(OPEN_ORDER_RESP_MSG_LENGTH)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_OPEN_ORDER)
	binary.Write(buffer, binary.BigEndian, order.AccountId)
	binary.Write(buffer, binary.BigEndian, instrumentId)
	binary.Write(buffer, binary.BigEndian, order.CorrelationId)
	binary.Write(buffer, binary.BigEndian, order.Id)
	binary.Write(buffer, binary.BigEndian, order.EntryOrderId)
	binary.Write(buffer, binary.BigEndian, order.Price)
	binary.Write(buffer, binary.BigEndian, order.Amount)
	binary.Write(buffer, binary.BigEndian, order.AmountRest)
	binary.Write(buffer, binary.BigEndian, order.Kind)
	binary.Write(buffer, binary.BigEndian, order.Type)
	binary.Write(buffer, binary.BigEndian, order.Direction)
	binary.Write(buffer, binary.BigEndian, order.PriceTrigger)
	binary.Write(buffer, binary.BigEndian, order.Duration)
	binary.Write(buffer, binary.BigEndian, order.CreatedTime)
	binary.Write(buffer, binary.BigEndian, order.ExpTime)
	binary.Write(buffer, binary.BigEndian, order.PositionId)
	binary.Write(buffer, binary.BigEndian, order.State)
	return buffer.Bytes()
}

func FromBinaryOpenOrderResp(data []byte) (instrumentId uint16, order *dao.OrderEntity, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid PutOrderResp message format. Err: %v", e)
		}
	}()
	err = validateBinary(data, order, OPEN_ORDER_RESP_MSG_LENGTH)
	if err == nil {
		//remove 1 byte - messageType
		buffer := bytes.NewBuffer(data[1:])
		instrumentId = uint16(0)
		order = &dao.OrderEntity{}
		binary.Read(buffer, binary.BigEndian, &order.AccountId)
		binary.Read(buffer, binary.BigEndian, &instrumentId)
		binary.Read(buffer, binary.BigEndian, &order.CorrelationId)
		binary.Read(buffer, binary.BigEndian, &order.Id)
		binary.Read(buffer, binary.BigEndian, &order.EntryOrderId)
		binary.Read(buffer, binary.BigEndian, &order.Price)
		binary.Read(buffer, binary.BigEndian, &order.Amount)
		binary.Read(buffer, binary.BigEndian, &order.AmountRest)
		binary.Read(buffer, binary.BigEndian, &order.Kind)
		binary.Read(buffer, binary.BigEndian, &order.Type)
		binary.Read(buffer, binary.BigEndian, &order.Direction)
		binary.Read(buffer, binary.BigEndian, &order.PriceTrigger)
		binary.Read(buffer, binary.BigEndian, &order.Duration)
		binary.Read(buffer, binary.BigEndian, &order.CreatedTime)
		binary.Read(buffer, binary.BigEndian, &order.ExpTime)
		binary.Read(buffer, binary.BigEndian, &order.PositionId)
		binary.Read(buffer, binary.BigEndian, &order.State)
	}
	return instrumentId, order, err
}

type CancelOrderReq struct {
	AccountId     uint32 `json:"ac"`
	OrderId       uint64 `json:"oi"`
	InstrumentId  uint16 `json:"i"`
	CorrelationId uint64 `json:"ci"`
	Price         uint64 `json:"p"` //used for check
}

func (order *CancelOrderReq) ToBinary() []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(CANCEL_ORDER_REQ_MSG_LENGTH)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_CANCEL_ORDER)
	binary.Write(buffer, binary.BigEndian, order.AccountId)
	binary.Write(buffer, binary.BigEndian, order.OrderId)
	binary.Write(buffer, binary.BigEndian, order.InstrumentId)
	binary.Write(buffer, binary.BigEndian, order.CorrelationId)
	binary.Write(buffer, binary.BigEndian, order.Price)
	return buffer.Bytes()
}

func (order *CancelOrderReq) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid CancelOrderReq message format. Err: %v", e)
		}
	}()
	err = validateBinary(data, order, CANCEL_ORDER_REQ_MSG_LENGTH)
	if err == nil {
		buffer := bytes.NewBuffer(data[1:])
		binary.Read(buffer, binary.BigEndian, &order.AccountId)
		binary.Read(buffer, binary.BigEndian, &order.OrderId)
		binary.Read(buffer, binary.BigEndian, &order.InstrumentId)
		binary.Read(buffer, binary.BigEndian, &order.CorrelationId)
		binary.Read(buffer, binary.BigEndian, &order.Price)
	}
	return err
}

type CancelOrderResp struct {
	AccountId    uint32 `json:"ac"`
	OrderId      uint64 `json:"oi"`
	InstrumentId uint16 `json:"i"`
}

func (order *CancelOrderResp) ToBinary() []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(CANCEL_ORDER_RESP_MSG_LENGTH)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_CANCEL_ORDER)
	binary.Write(buffer, binary.BigEndian, order.AccountId)
	binary.Write(buffer, binary.BigEndian, order.OrderId)
	binary.Write(buffer, binary.BigEndian, order.InstrumentId)
	return buffer.Bytes()
}

func (order *CancelOrderResp) FromBinary(data []byte) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid CancelOrderResp message format. Err: %v", e)
		}
	}()
	err = validateBinary(data, order, CANCEL_ORDER_RESP_MSG_LENGTH)
	if err == nil {
		buffer := bytes.NewBuffer(data[1:])
		binary.Read(buffer, binary.BigEndian, &order.AccountId)
		binary.Read(buffer, binary.BigEndian, &order.OrderId)
		binary.Read(buffer, binary.BigEndian, &order.InstrumentId)
	}
	return err
}

func validateBinary(data []byte, message interface{}, expectedLength int) (err error) {
	actualLength := len(data)
	if actualLength != expectedLength {
		return errors.New(fmt.Sprintf("invalid %T message length: expected: %d, actual %d", message, expectedLength, actualLength))
	}
	return nil
}

/**
Update order message
*/
func ToBinaryOrderChangesEntity(instrumentId uint16, order *dao.OrderChangesEntity) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(22)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_UPDATE_ORDER)
	binary.Write(buffer, binary.BigEndian, instrumentId)
	binary.Write(buffer, binary.BigEndian, order.Id)
	binary.Write(buffer, binary.BigEndian, order.AccountId)
	binary.Write(buffer, binary.BigEndian, order.AmountRest)
	binary.Write(buffer, binary.BigEndian, order.State)
	return buffer.Bytes()
}
func FromBinaryOrderChangesEntity(data []byte) (instrumentId uint16, order *dao.OrderChangesEntity, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid ENGINE_MSG_TYPE_UPDATE_ORDER message format. Err: %v", e)
		}
	}()
	order = &dao.OrderChangesEntity{}
	instrumentId = uint16(0)
	//remove msg type id
	buffer := bytes.NewBuffer(data[1:])
	binary.Read(buffer, binary.BigEndian, &instrumentId)
	binary.Read(buffer, binary.BigEndian, &order.Id)
	binary.Read(buffer, binary.BigEndian, &order.AccountId)
	binary.Read(buffer, binary.BigEndian, &order.AmountRest)
	binary.Read(buffer, binary.BigEndian, &order.State)

	return instrumentId, order, err
}
