package msg

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/flashrider/rider/dao"
	"testing"
)

func TestPutOrderReqSer(t *testing.T) {
	order := &OpenOrderReq{}
	order.AccountId = 1
	order.Price = 500
	order.Amount = 1000
	order.CorrelationId = 23434234
	order.Direction = dao.ORDER_DIRECTION_SELL
	order.Type = dao.ORDER_TYPE_LIMIT
	order.PriceTrigger = dao.ORDER_PRICE_TRIGGER_ASK_L
	order.Duration = dao.ORDER_DURATION_GTC
	order.TPPrice = 1
	order.SLPrice = 2
	order.InstrumentId = 1

	binary := order.ToBinary()
	fmt.Printf("bytes: %#v\n", binary)

	assert.Equal(t, OPEN_ORDER_REQ_MSG_LENGTH, len(binary))

	or := &OpenOrderReq{}
	or.FromBinary(binary)
	fmt.Println(or.AccountId, or.InstrumentId, or.Price)

	assert.Equal(t, uint32(1), or.AccountId, "AccountId")
	assert.Equal(t, uint16(1), or.InstrumentId, "InstrumentId")
	assert.Equal(t, uint64(500), or.Price, "Price")
}

func TestPutOrderRespSer(t *testing.T) {
	instrumentId := uint16(1)
	order := &dao.OrderEntity{}
	order.AccountId = 2
	order.CorrelationId = 3
	order.Id = 4

	binary := ToBinaryOpenOrderResp(instrumentId, order)

	assert.Equal(t, OPEN_ORDER_RESP_MSG_LENGTH, len(binary))

	instrumentId, deserialized, err := FromBinaryOpenOrderResp(binary)

	fmt.Println("err", err)

	assert.Equal(t, uint16(1), instrumentId, "InstrumentId")
	assert.Equal(t, uint32(2), deserialized.AccountId, "Order.AccountId")
	assert.Equal(t, uint64(3), deserialized.CorrelationId, "Order.CorrelationId")
	assert.Equal(t, uint64(4), deserialized.Id, "Order.Id")
}

func TestCancelOrderReqSer(t *testing.T) {
	order := &CancelOrderReq{}
	order.AccountId = 1
	order.OrderId = 2
	order.InstrumentId = 3

	binary := order.ToBinary()

	assert.Equal(t, CANCEL_ORDER_REQ_MSG_LENGTH, len(binary))

	deserialized := &CancelOrderReq{}
	deserialized.FromBinary(binary)

	assert.Equal(t, uint32(1), deserialized.AccountId, "AccountId")
	assert.Equal(t, uint64(2), deserialized.OrderId, "OrderId")
	assert.Equal(t, uint16(3), deserialized.InstrumentId, "InstrumentId")
}

func TestCancelOrderRespSer(t *testing.T) {
	order := &CancelOrderResp{}
	order.AccountId = 1
	order.OrderId = 2
	order.InstrumentId = 3

	binary := order.ToBinary()

	deserialized := &CancelOrderResp{}
	deserialized.FromBinary(binary)

	assert.Equal(t, CANCEL_ORDER_RESP_MSG_LENGTH, len(binary))

	assert.Equal(t, uint32(1), deserialized.AccountId, "AccountId")
	assert.Equal(t, uint64(2), deserialized.OrderId, "OrderId")
	assert.Equal(t, uint16(3), deserialized.InstrumentId, "InstrumentId")
}

func TestPutOrderReqValidateBinary(t *testing.T) {
	deserialized := &OpenOrderReq{}

	validateBinaryLength(t, ENGINE_MSG_TYPE_OPEN_ORDER, deserialized)
}

func TestCancelOrderReqValidateBinary(t *testing.T) {
	deserialized := &CancelOrderReq{}

	validateBinaryLength(t, ENGINE_MSG_TYPE_CANCEL_ORDER, deserialized)
}

func validateBinaryLength(t *testing.T, messageType EngineMessageType, message CommunicationMessage) {
	var invalidLengthBinary = []byte{byte(messageType)}

	err := message.FromBinary(invalidLengthBinary)

	fmt.Println(err)

	assert.Error(t, err, "Message length error absent")
}
