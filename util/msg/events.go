package msg

//Order book changes events.
type OrderBookPriceIndexEventType uint8

const (
	ORDER_BOOK_PRICE_EVENT_NEW    OrderBookPriceIndexEventType = 1
	ORDER_BOOK_PRICE_EVENT_DELETE OrderBookPriceIndexEventType = 2
	ORDER_BOOK_PRICE_EVENT_CHANGE OrderBookPriceIndexEventType = 3
)

// Engine messages
type EngineMessageType uint8

const (
	// operation. 0 reserved for errors. see app_error.go
	ENGINE_MSG_TYPE_ERR_RESP                EngineMessageType = 0
	ENGINE_MSG_TYPE_OPEN_ORDER              EngineMessageType = 1
	ENGINE_MSG_TYPE_CANCEL_ORDER            EngineMessageType = 2 //user cancel order
	ENGINE_MSG_TYPE_UPDATE_ORDER            EngineMessageType = 3 //order matched partial or full
	ENGINE_MSG_TYPE_GET_ORDER_BOOK_SNAPSHOT EngineMessageType = 4
	ENGINE_MSG_TYPE_UPDATE_FUND             EngineMessageType = 5 //fund changed at engine
	ENGINE_MSG_TYPE_OPEN_POSITION           EngineMessageType = 6 //open position
	ENGINE_MSG_TYPE_UPDATE_POSITION         EngineMessageType = 7 //update position/close position
	//if order doen't partially filled (touched) sys should remove order. This msg type for private use
	ENGINE_MSG_ORDER_EXPIRED EngineMessageType = 8
)

const (
	TOPIC_ENGINE_MAIN_MSG_REQ        = "main-req"  //putOrder, cancelOrder, get order book snapshot ...
	TOPIC_ENGINE_MAIN_MSG_RESP       = "main-resp" //putOrder, cancelOrder response
	TOPIC_ENGINE_MSG_ORDER_BOOK_FEED = "order"     //order book changes feed
	TOPIC_ENGINE_MSG_TRADES_FEED     = "trade"     //new trades feed
	//TO PUBLIC API TOPIC_ENGINE_MSG_INSTR_STATUS_FEED  = "i_status"          //instruments status
	//TO PUBLIC API TOPIC_ENGINE_MSG_MARKET_STATIS_FEED = "m_statistic"       //market statistics, last trade, ask order count. Every 2sec OHLC
	TOPIC_ENGINE_MSG_GET_SNAPSHOT_RESP = "get-snapshot-resp" //order book snapshot response
)
