package msg

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/flashrider/rider/dao"
)

func ToBinaryFundCacheModelResp(fund *dao.FundCacheModel) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(37)
	binary.Write(buffer, binary.BigEndian, ENGINE_MSG_TYPE_UPDATE_FUND)
	binary.Write(buffer, binary.BigEndian, fund.Id)
	binary.Write(buffer, binary.BigEndian, fund.AccountId)
	binary.Write(buffer, binary.BigEndian, fund.Balance)
	binary.Write(buffer, binary.BigEndian, fund.Reserved)
	binary.Write(buffer, binary.BigEndian, fund.State)
	binary.Write(buffer, binary.BigEndian, fund.TotalVolumeAmount)
	return buffer.Bytes()
}

func FromBinaryFundCacheModelResp(data []byte) (fund *dao.FundCacheModel, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid ENGINE_MSG_TYPE_UPDATE_FUND message format. Err: %v", e)
		}
	}()
	//remove msg type id
	fund = &dao.FundCacheModel{}
	buffer := bytes.NewBuffer(data[1:])
	binary.Read(buffer, binary.BigEndian, &fund.Id)
	binary.Read(buffer, binary.BigEndian, &fund.AccountId)
	binary.Read(buffer, binary.BigEndian, &fund.Balance)
	binary.Read(buffer, binary.BigEndian, &fund.Reserved)
	binary.Read(buffer, binary.BigEndian, &fund.State)
	binary.Read(buffer, binary.BigEndian, &fund.TotalVolumeAmount)

	return fund, err
}
