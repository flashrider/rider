package msg

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/flashrider/rider/dao"
)

/*
	Trades
*/
func ToBinaryTradeEntityResp(instrumentId uint16, entity *dao.TradeEntity) []byte {
	buffer := &bytes.Buffer{}
	buffer.Grow(50)
	binary.Write(buffer, binary.BigEndian, instrumentId)
	binary.Write(buffer, binary.BigEndian, entity.Id)
	binary.Write(buffer, binary.BigEndian, entity.Direction)
	binary.Write(buffer, binary.BigEndian, entity.Price)
	binary.Write(buffer, binary.BigEndian, entity.Quantity)
	binary.Write(buffer, binary.BigEndian, entity.AskOrderId)
	binary.Write(buffer, binary.BigEndian, entity.BidOrderId)
	binary.Write(buffer, binary.BigEndian, entity.SettleDate)
	return buffer.Bytes()
}

func FromBinaryTradeEntityResp(data []byte) (instrumentId uint16, fund *dao.TradeEntity, err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("invalid ENGINE_MSG_TYPE_UPDATE_FUND message format. Err: %v", e)
		}
	}()
	instrumentId = uint16(0)
	//remove msg type id
	fund = &dao.TradeEntity{}
	buffer := bytes.NewBuffer(data)
	binary.Read(buffer, binary.BigEndian, &instrumentId)
	binary.Read(buffer, binary.BigEndian, &fund.Id)
	binary.Read(buffer, binary.BigEndian, &fund.Direction)
	binary.Read(buffer, binary.BigEndian, &fund.Price)
	binary.Read(buffer, binary.BigEndian, &fund.Quantity)
	binary.Read(buffer, binary.BigEndian, &fund.AskOrderId)
	binary.Read(buffer, binary.BigEndian, &fund.BidOrderId)
	binary.Read(buffer, binary.BigEndian, &fund.SettleDate)

	return instrumentId, fund, err
}
