package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"time"
)

/**
Symbol cache
Every 30sec refresh cache
ArtyomAminov
*/
const (
	INSTRUMENT_FLUSH_CACHE_TIME_SEC = 60
)

type InstrumentCache struct {
	buffer        map[uint16]*dao.InstrumentEntity
	log           *flog.Logger
	instrumentDao *dao.InstrumentDao
}

func NewInstrumentCache(logManager *flog.LogManager, instrumentDao *dao.InstrumentDao) *InstrumentCache {
	buffer := make(map[uint16]*dao.InstrumentEntity, 200)
	log := logManager.NewLogger("instr_cache", flog.LEVEL_INFO)
	cache := &InstrumentCache{buffer, log, instrumentDao}
	cache.load()
	//create goroutine
	go cache.flushCache()
	return cache
}

func (cache *InstrumentCache) Get(instrumentId uint16) *dao.InstrumentEntity {
	return cache.buffer[instrumentId]
}

//load when init cache
func (cache *InstrumentCache) load() {
	list, err := cache.instrumentDao.GetAll()
	if err == nil {
		cache.log.InfoS("count: ", len(list))
		for _, entity := range list {
			cache.buffer[entity.Id] = entity
		}
	} else {
		panic("get instrumentDao.GetAll: " + err.Error())
	}
}

func (cache *InstrumentCache) flushCache() {
	for true {
		cache.log.Info("start read data")
		list, err := cache.instrumentDao.GetAll()
		if err == nil {
			cache.log.InfoS("count: ", len(list))
			for _, entity := range list {
				inMem := cache.buffer[entity.Id]
				if inMem == nil {
					cache.log.InfoF("new instrument name: %s id: %d ", entity.Symbol, entity.Id)
					cache.buffer[entity.Id] = entity
				} else {
					//do not copy full entity fields!
					if inMem.Version != entity.Version {
						cache.log.InfoF("update instrument name: %s id: %d version: %d ",
							entity.Symbol, entity.Id, entity.Version)
						inMem.Version = entity.Version
						inMem.SessionState = entity.SessionState
						inMem.MinAmount = entity.MinAmount
						inMem.UpdateTime = entity.UpdateTime
						inMem.Decimal = entity.Decimal
						inMem.SessionOpenTime = entity.SessionOpenTime
						inMem.SessionEndTime = entity.SessionEndTime
						inMem.FeeAccountId = entity.FeeAccountId
					}
				}
			}
		} else {
			cache.log.Err("get instrumentDao.GetAll: " + err.Error())
		}
		time.Sleep(time.Second * INSTRUMENT_FLUSH_CACHE_TIME_SEC)
	}
}

func (cache *InstrumentCache) GetCacheSize() uint16 {
	return uint16(len(cache.buffer))
}

func (cache *InstrumentCache) GetCache() map[uint16]*dao.InstrumentEntity {
	return cache.buffer
}

//return baseCurrencyId, quoteCurrencyId true/false - found
//return 0, 0, false if not found by id
func (cache *InstrumentCache) GetInstrumentCurrency(instrumentId uint16) (uint16, uint16, bool) {
	val, ok := cache.buffer[instrumentId]
	if !ok {
		return 0, 0, false
	}
	return val.BaseCurrCodeId, val.QuoteCurrCodeId, true
}

func (cache *InstrumentCache) GetInstrumentById(instrumentId uint16) *dao.InstrumentEntity {
	return cache.buffer[instrumentId]
}
