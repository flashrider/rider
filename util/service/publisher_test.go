package service

import (
	"testing"
	"time"
)

func TestPublisher(t *testing.T) {
	publisher := NewMsgPublisher("test", "127.0.0.1:4150")
	publisher.PublishMsgAsync([]byte("test msg!!"))
	time.Sleep(2 * time.Second)
	publisher.Shutdown()
}
