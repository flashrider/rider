package service

import (
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/dao"
	"time"
)

/**
Currency cache
Every 30sec refresh cache
ArtyomAminov
*/
const (
	CURRENCY_FLUSH_CACHE_TIME_SEC = 60
)

type CurrencyCache struct {
	currencyBuff map[uint16]*dao.CurrencyEntity
	log          *flog.Logger
	currencyDao  *dao.CurrencyDao
}

func NewCurrencyCache(logManager *flog.LogManager, currencyDao *dao.CurrencyDao) *CurrencyCache {
	cache := make(map[uint16]*dao.CurrencyEntity, 100)
	log := logManager.NewLogger("curr_cache", flog.LEVEL_INFO)
	cahceObj := &CurrencyCache{cache, log, currencyDao}
	//create goroutine
	go cahceObj.flushCache()
	return cahceObj
}

//get currency
func (cache *CurrencyCache) GetCurrency(currId uint16) *dao.CurrencyEntity {
	return cache.currencyBuff[currId]
}

func (cache *CurrencyCache) flushCache() {
	for true {
		cache.log.Info("start read data")
		list, err := cache.currencyDao.GetAll()
		if err == nil {
			cache.log.InfoS("count: ", len(list))
			for _, entity := range list {
				cache.currencyBuff[entity.Id] = entity
			}
		} else {
			cache.log.Err("get instrumentDao.GetAll: " + err.Error())
		}
		time.Sleep(time.Second * CURRENCY_FLUSH_CACHE_TIME_SEC)
	}
}

func (cache *CurrencyCache) GetCacheSize() uint16 {
	return uint16(len(cache.currencyBuff))
}

func (cache *CurrencyCache) GetCache() map[uint16]*dao.CurrencyEntity {
	return cache.currencyBuff
}
