package service

import (
	"fmt"
	"testing"
	"time"
)

func TestListener(t *testing.T) {
	queue := make(chan []byte, 128)
	listener := NewMsgListener("test", queue, "127.0.0.1:4150")
	go func() {
		for msg := range queue {
			fmt.Println(string(msg))
		}
	}()
	time.Sleep(5 * time.Minute)
	listener.Shutdown()
}
