package service

import (
	"fmt"
	"github.com/artjoma/flog"
	"gitlab.com/flashrider/rider/util"
	"time"
)

type DiagnosticService struct {
	log *flog.Logger
}

func NewDiagnosticService(logManager *flog.LogManager) *DiagnosticService {
	ds := &DiagnosticService{}
	ds.log = logManager.NewLogger("diag", flog.LEVEL_INFO)
	go ds.printDiagnostic()
	return ds
}

func (ds *DiagnosticService) printDiagnostic() {
	var heap uint64 = 0
	var processMem uint64 = 0
	var cpuUsage float64 = 0

	for {
		heap, processMem = util.GetUsedMem()
		cpuUsage = util.CpuUsagePercent()
		ds.log.Info(fmt.Sprintf("%7d Kib, %7d KiB, %3.1f %%", heap, processMem, cpuUsage))
		time.Sleep(10 * time.Second)
	}
}
