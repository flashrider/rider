package service

import (
	"github.com/nsqio/go-nsq"
	"log"
	"os"
	"strconv"
	"time"
)

//Rider API client
type MsgListener struct {
	topicName string
	msgQueue  chan []byte
	consumer  *nsq.Consumer
}

//brokerAddress - ip:port
func NewMsgListener(topicName string, msgQueue chan []byte, brokerAddress string) *MsgListener {
	listener := &MsgListener{topicName: topicName, msgQueue: msgQueue}
	config := nsq.NewConfig()

	consumer, _ := nsq.NewConsumer(topicName, "main_" + strconv.Itoa(int(time.Now().UnixNano())), config)
	consumer.SetLogger(log.New(os.Stdout, "", log.Flags()), nsq.LogLevelWarning)

	consumer.AddHandler(listener)
	//addr := "127.0.0.1:4150"
	err := consumer.ConnectToNSQD(brokerAddress)
	if err != nil {
		panic(err)
	}
	listener.consumer = consumer

	return listener
}

func (client *MsgListener) HandleMessage(message *nsq.Message) error {
	client.msgQueue <- message.Body
	return nil
}

func (client *MsgListener) LogFailedMessage(message *nsq.Message) {

}

func (client *MsgListener) Shutdown() {
	client.consumer.Stop()
	close(client.msgQueue)
}
