package service

import (
	"github.com/nsqio/go-nsq"
	"log"
	"os"
)

//Rider API client
type MsgPublisher struct {
	topicName string
	publisher *nsq.Producer
}

//brokerAddress: 127.0.0.1:4150
func NewMsgPublisher(topicName, brokerAddress string) *MsgPublisher {
	config := nsq.NewConfig()

	publisher, _ := nsq.NewProducer(brokerAddress, config)
	publisher.SetLogger(log.New(os.Stdout, "", log.Flags()), nsq.LogLevelWarning)
	err := publisher.Ping()
	if err != nil {
		panic("publisher should connect on ping")
	}

	return &MsgPublisher{topicName, publisher}
}

//publish message to topic
func (client *MsgPublisher) PublishMsgAsync(msg []byte) error {
	return client.publisher.PublishAsync(client.topicName, msg, nil)
}

func (client *MsgPublisher) PublishMsg(msg []byte) error {
	return client.publisher.Publish(client.topicName, msg)
}

func (client *MsgPublisher) Shutdown() {
	client.publisher.Stop()
}
