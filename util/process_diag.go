package util

//#include <time.h>
import "C"
import (
	"runtime"
	"time"
)

//Return heap size in kb and total mem reserved by process in kb
func GetUsedMem() (uint64, uint64) {
	memStat := &runtime.MemStats{}
	runtime.ReadMemStats(memStat)
	return memStat.Alloc / 1024, memStat.Sys / 1024
}

var startTime time.Time = time.Now()
var startTicks = C.clock()

//Return CPU usage percents
func CpuUsagePercent() float64 {
	clockSeconds := float64(C.clock()-startTicks) / float64(C.CLOCKS_PER_SEC)
	realSeconds := time.Since(startTime).Seconds()
	return clockSeconds / realSeconds * 100
}
