package util

import (
	"fmt"
	"time"
)

//std out print info message
func LogI(msg string) {
	fmt.Println(time.Now().UTC().Format("I0201 15:04:05 ") + msg)
}

//std out print info message using sprint.
//formats using the default formats for its operands and returns the resulting string
func LogIs(msg ...interface{}) {
	fmt.Println(time.Now().UTC().Format("I0201 15:04:05 ") + fmt.Sprint(msg...))
}

//std out print error message
func LogE(msg string) {
	fmt.Println(time.Now().UTC().Format("E0201 15:04:05 ") + msg)
}
