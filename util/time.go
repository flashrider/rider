package util

import "time"

// return current time in microseconds
func NowMk() uint64 {
	return uint64(time.Now().UnixNano() / 1000)
}

// return current time in seconds
func NowUnix() uint64 {
	return uint64(time.Now().Unix())
}
